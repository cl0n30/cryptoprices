# Changelog

### version 1.4.1
* add OLED theme
* add option to display price in percentage alerts
* fix graph display errors

### version 1.4.0
* copy portfolio and transaction coin amounts and values with long press
* show market cap rank on detail screen
* add options to change number and date formats
* add password lock and private mode
* add auto backup
* add custom large move alert threshold

### version 1.3.3
* choose app start screen
* download thumbnails file
* add more currencies
* fix opening detail screen with wrong portfolio
* fix wrong default portfolio after import
* don't add all coins to favorites when importing
* reduce API calls when loading favorites
* correct price display position in wallet graph

### version 1.3.2
* support multiple portfolios
* add coin id migration assistant
* add overwrite alert for import
* fix number format of percentages
* display all small numbers correctly
* fix coin list caching and scrolling problems
* fix global market share diagram
* fix errors with not enough market data

### Version 1.3.1
* add error and detailed explanation for API rate limitation
* add donation page to About
* add pull-to-refresh when getting an error message
* show global market cap and volume
* add global market info screen
* add option to use either your own coin prices or the real coin prices for the currency value in the transaction list
* add transaction fee field
* fix coin list being empty after an error when refreshing
* use correct transaction price when unchecking transaction date price
* fix app freezing when displaying all negative portfolio values
* make it possible to use 0 as coin price in transaction
* fix comma being an invalid decimal separator
* fix transaction fields not resetting after cancelling

### Version 1.3.0
* Add portfolios
* Restart alerts after updating the app

### Version 1.2.2
* Don't create a new large move alert when one already exists (e.g. when importing)
* Update the favorites list instantly
* Display a snackbar after removing a favorite to undo the removal
* Show price, change percentage and timestamps when touching the graph
* Show the max and min price for the selected time period

### Version 1.2.1
* Added option to change the primary color of the app
* Added support for system dark mode for Android 10 and up
* Added option to import and export favorites and alerts
* Added "collapse / expand all" button to alert list
* Added search history
* Fixed issue where price alert list entries where still displayed after being deleted
* Fixed some UI issues

### Version 1.1.0
* Added price alerts
* Added large move alerts
* Added coin thumbnails
* Added currency flag icons
* Update search list every day
* Display smaller prices correctly

### Version 1.0.0
* Initial release
