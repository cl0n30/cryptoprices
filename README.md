<div align="center">

<p><img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/assets/images/ic_launcher_crypto_orange_3_round.png" width="200"></p>

# Crypto Prices

### FLOSS Android app to keep track of the prices of most cryptocurrencies

[![License](https://badgen.net/gitlab/license/cl0n30/cryptoprices)](https://gitlab.com/cl0n30/cryptoprices/-/blob/master/LICENSE)
[![Release](https://badgen.net/gitlab/release/cl0n30/cryptoprices)](https://gitlab.com/cl0n30/cryptoprices/-/releases)
[![Google Play](https://badgen.net/badge/icon/googleplay?icon=googleplay&label)](https://play.google.com/store/apps/details?id=de.cloneapps.crypto_prices)
[![F-Droid](https://img.shields.io/f-droid/v/de.cloneapps.crypto_prices.svg)](https://f-droid.org/packages/de.cloneapps.crypto_prices/)
[![Open Issues](https://badgen.net/gitlab/open-issues/cl0n30/cryptoprices)](https://gitlab.com/cl0n30/cryptoprices/-/issues)
[![Liberapay](https://img.shields.io/liberapay/receives/CryptoPrices.svg?logo=liberapay)](https://liberapay.com/CryptoPrices)
[![Goal](https://img.shields.io/liberapay/goal/CryptoPrices.svg?logo=liberapay)](https://liberapay.com/CryptoPrices)

</div>

----

Crypto Prices is an Android app that lets you keep track of the prices of most available cryptocurrencies.

You can add coins as favorites to get updates on your portfolio even faster.

Watch the value of all your real or imaginary crypto portfolios.
(This is **NOT** a crypto wallet. All added coins are imaginary.)

Create your own alerts for all coins and get notified of large price changes of your favorites.

This app currently supports 56 currencies such as USD, EUR and GBP and is available in English and German.

<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/screenshots/coinlist.png" height=600>
<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/screenshots/favorites.png" height=600>
<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/screenshots/detail.png" height=600>
<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/screenshots/portfolio.png" height=600>
<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/screenshots/price_alert_list.png" height=600>

### Powered by
[<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/CoinGeckoLogo.png" height=80>](https://www.coingecko.com/en)

## Download
[<img src="https://gitlab.com/cl0n30/cryptoprices/-/raw/master/google-play-badge.png" height=80>](https://play.google.com/store/apps/details?id=de.cloneapps.crypto_prices)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="80">](https://f-droid.org/packages/de.cloneapps.crypto_prices/)

## Report a bug / Request a feature
To report bugs or request features please use the [issue tracker](https://gitlab.com/cl0n30/cryptoprices/-/issues).
When reporting a bug, if possible, please provide detailed steps on how to reproduce the bug and state the version number of the app you are using. The version number can be found on the About page of the app.

## Support the developer
If you want to support me in continuing to develop this app, you can do so by pledging on Liberapay or by donating cryptocurrencies to the following addresses:

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/CryptoPrices) | [![PayPal](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/donate/?hosted_button_id=TC7SVTTLQ7A6U)


### Bitcoin
bc1qsd3x37d7dea2ghcrmvmgdthl7723gf8ja0j7va

![bitcoin](https://gitlab.com/cl0n30/cryptoprices/-/raw/master/Bitcoin_QR_Code.png)

### Ethereum (and ERC20 tokens)
0x24F34C5C88c61C9578Ce607D30e7d5b9A94b7d38

![ethereum](https://gitlab.com/cl0n30/cryptoprices/-/raw/master/Ethereum_QR_Code.png)
