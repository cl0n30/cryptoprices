import 'dart:convert';
import 'dart:io';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

///Provides image URLs from the associated json files in the assets
class ImageUrls {
  final _box = Hive.box(Constants.COINTHUMBNAILURLSBOXNAME);

  ///Loads the thumbnail json and saves thumbnails to database
  ///Currently: 13669 thumbnails
  void decodeThumbnails() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String filePath = appDocDir.path + "/${Constants.thumbnailsFileName}.json";
    final file = File(filePath);
    String thumbnailsJson = "";
    Map<String, String> thumbnails = Map();
    try {
      final lastUpdated = SettingsDAO().getThumbnailsLastUpdated();
      if (lastUpdated == null || DateTime.now().difference(lastUpdated).inDays >= Constants.thumbnailsUpdateTimerDays) {
        await _fetchThumbnails(file);
        thumbnailsJson = await file.readAsString();
        thumbnails = await compute(parseThumbnailUrls, thumbnailsJson);
        await _box.clear();
        _box.putAll(thumbnails);
        file.delete();
        SettingsDAO().setThumbnailsLastUpdated(DateTime.now());
      }
    } catch (e) { //no internet or other error
      if (_box.isEmpty) { //only update when no thumbnails exist (don't overwrite potentially newer entries)
        thumbnailsJson = await rootBundle.loadString("assets/images/thumbnails.json");
        thumbnails = await compute(parseThumbnailUrls, thumbnailsJson);
        _box.putAll(thumbnails);
      }
    }
  }

  ///Download the thumbnail file and save it into the given file
  Future<void> _fetchThumbnails(File writeFile) async {
    final request = await HttpClient().getUrl(Uri.parse(Constants.thumbnailFileUrl));
    final response = await request.close();

    await response.pipe(writeFile.openWrite());
  }

  ///Returns the thumbnail URL of the coin with the given id
  ///Size: 25x25px
  String getThumbnailUrl(String coinId) {
    if (_box.get(coinId) == null) {
      return "";
    }
    return _box.get(coinId)!;
  }

  ///Returns the small image URL of the coin with the given id
  ///Size: 50x50px
  String getSmallImageUrl(String coinId) {
    if (_box.get(coinId) == null) {
      return "";
    }
    return _box.get(coinId)!.replaceFirst("/thumb/", "/small/");
  }

  ///Returns the large image URL of the coin with the given id
  ///Size: 250x250px
  String getLargeImageUrl(String coinId) {
    if (_box.get(coinId) == null) {
      return "";
    }
    return _box.get(coinId)!.replaceFirst("/thumb/", "/large/");
  }
}

///Parses the thumbnail URLs from the given json string and returns them as map (coinId: url)
Map<String, String> parseThumbnailUrls(String jsonData) {
  List thumbnailsJson = jsonDecode(jsonData);
  Map<String, String> thumbnails = Map();

  for (int i = 0; i < thumbnailsJson.length; i++) {
    thumbnails[thumbnailsJson[i]["id"]] = thumbnailsJson[i]["image"];
  }

  return thumbnails;
}