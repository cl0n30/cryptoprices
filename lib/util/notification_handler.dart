import 'dart:math';
import 'dart:ui';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/large_move_alert.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

///Handler for the FlutterLocalNotificationsPlugin
class NotificationHandler {
  late FlutterLocalNotificationsPlugin plugin;

  String _numberFormatLocale = "";
  SettingsDAO _settingsDAO = SettingsDAO();

  //use Singleton
  static final NotificationHandler _instance = NotificationHandler._internal();
  factory NotificationHandler() => _instance;

  NotificationHandler._internal() {
    plugin = FlutterLocalNotificationsPlugin();
  }

  ///Initializes the FlutterLocalNotificationsPlugin.
  ///Must be called before any notifications are sent.
  ///onSelectNotification is called when a notification is pressed.
  void initialize(Future<dynamic> Function(String?) onSelectNotification) async {
    AndroidInitializationSettings androidInitializationSettings = AndroidInitializationSettings(
        "ic_notification"
    );
    InitializationSettings initializationSettings = InitializationSettings(
        android: androidInitializationSettings
    );
    await plugin.initialize(
        initializationSettings,
        onSelectNotification: onSelectNotification
    );

    _numberFormatLocale = await _settingsDAO.getNumberFormatLocalePrefs();
  }

  ///Shows a price alert notification using the PriceAlert to determine the message content.
  ///PricePercentage contains both price and percentage as a map
  Future<void> showPriceAlertNotification(PriceAlert alert, {Map<String, double> pricePercentage = const {}}) async {
    await _initializeLocalization();

    AndroidNotificationDetails androidDetails = AndroidNotificationDetails(
        Constants.PRICEALERTNOTIFICATIONCHANNELID, S.current.priceAlertsTabName,
        importance: Importance.max,
        priority: Priority.high,
        showWhen: true,
        styleInformation: BigTextStyleInformation('')
    );

    NotificationDetails details = NotificationDetails(
      android: androidDetails
    );

    String priceMovementText;
    switch (alert.priceMovement) {
      case PriceMovement.above: {
        priceMovementText = S.current.notificationPriceAlertAbove;
        break;
      }

      case PriceMovement.equal: {
        priceMovementText = S.current.notificationPriceAlertEqual;
        break;
      }

      case PriceMovement.below: {
        priceMovementText = S.current.notificationPriceAlertBelow;
        break;
      }
    }

    String payload = "${alert.coinId}_${alert.coinName}";

    if (alert.targetValueType == TargetValueType.percentage) {
      String alertText = "";
      if (await _settingsDAO.getShowPriceInPercentageAlertsPrefs()) {
        alertText = S.current.notificationPriceAlertMessagePercentWithPrice(
          priceMovementText, 
          Util.formatPercentage(alert.targetValue, locale: _numberFormatLocale),
          Util.formatPrice(pricePercentage[Constants.APIGETCOINPRICEKEY]!, locale: _numberFormatLocale),
          Constants.currencySymbols[alert.currency]!
        );
      } else {
        alertText = S.current.notificationPriceAlertMessagePercent(
          priceMovementText, 
          Util.formatPercentage(alert.targetValue, locale: _numberFormatLocale)
        );
      }
      await plugin.show(
          Random.secure().nextInt(Constants.MAXNOTIFICATIONID),
          S.current.notificationPriceAlertTitle(alert.coinName),
          alertText,
          details,
          payload: payload
      );
    } else {
      await plugin.show(
          Random.secure().nextInt(Constants.MAXNOTIFICATIONID),
          S.current.notificationPriceAlertTitle(alert.coinName),
          S.current.notificationPriceAlertMessagePrice(
            priceMovementText,
            Util.formatPrice(alert.targetValue, locale: _numberFormatLocale),
            Constants.currencySymbols[alert.currency]!
          ),
          details,
          payload: payload
      );
    }
  }

  ///Shows a large price move alert notification using the PriceAlert
  ///and the current change percentage and price to determine the message content.
  Future<void> showLargeMoveAlertNotification(LargeMoveAlert alert, double changePercentage, double currentPrice) async {
    await _initializeLocalization();

    AndroidNotificationDetails androidDetails = AndroidNotificationDetails(
        Constants.LARGEMOVEALERTNOTIFICATIONCHANNELID, S.current.notificationLargeMoveAlertChannelName,
        importance: Importance.max,
        priority: Priority.high,
        showWhen: true,
        styleInformation: BigTextStyleInformation('')
    );

    NotificationDetails details = NotificationDetails(
        android: androidDetails
    );

    String payload = "${alert.coinId}_${alert.coinName}";

    if (alert.coinName.isEmpty) {
      alert.coinName = S.current.notificationLargeMoveAlertCoinNamePlaceholder;
    }

    await plugin.show(
        Random.secure().nextInt(Constants.MAXNOTIFICATIONID),
        S.current.notificationLargeMoveAlertTitle(alert.coinName),
        S.current.notificationLargeMoveAlertMessage(
          alert.coinName,
          Util.formatPercentage(changePercentage, locale: _numberFormatLocale),
          currentPrice,
          Constants.currencySymbols[alert.currency]!
        ),
        details,
        payload: payload
    );
  }

  ///Initializes the localization delegate outside of the app (during background work)
  Future<void> _initializeLocalization() async {
    String languageCode = await _settingsDAO.getLocalePrefs();
    if (languageCode.isEmpty) {
      languageCode = "en";
    }
    await S.delegate.load(Locale(languageCode)); //load localization outside of app
  }
}