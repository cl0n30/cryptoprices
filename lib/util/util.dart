import 'dart:io';
import 'dart:math';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:decimal/decimal.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../generated/l10n.dart';

///Contains useful utility functions e.g. formaters and converters
class Util {
  ///Converts a number to double if it is an int
  static double convertIfInt(num value) {
    if (value is int) {
      return value.toDouble();
    }

    return value as double;
  }

  ///Rounds a double to 2 decimals if it is bigger than 1000
  ///and to 5 decimals after last zero else
  static double roundToDecimals(double x) {
    final nonZeroDecimals = 5;
    if (x > 1000) {
      return double.parse(x.toStringAsFixed(2));
    }
    final pString = Decimal.parse(x.toString()).toString();
    final decimals = pString.split('.').last.characters.toList();
    for (int i = 0; i < decimals.length; i++) {
      if (decimals[i] != '0') {
        final number = x.toStringAsFixed(i + nonZeroDecimals);
        return double.parse(number);
      }
    }
    return double.parse(x.toStringAsFixed(2));
  }

  ///Formats a percentage double to have two decimals according to the locale
  static String formatPercentage(double x, {String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getNumberFormatLocaleHive();
    }
    return NumberFormat(Constants.NUMBERFORMATTWODECIMALS, locale).format(x);
  }

  ///Formats a price double to have two, nine or varying number of decimals
  ///depending on how big it is.
  ///Can format to two decimals above a custom value
  ///> 1000 (or custom value)= 2 decimals;
  ///other = 5 decimals after the last zero;
  static String formatPrice(double x, {double twoDecimalsAbove = 1000, locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getNumberFormatLocaleHive();
    }

    final nonZeroDecimals = 5;
    if (x > twoDecimalsAbove) {
      return NumberFormat(Constants.NUMBERFORMATTWODECIMALS, locale).format(x);
    }
    final pString = Decimal.parse(x.toString()).toString();
    final decimals = pString.split('.').last.characters.toList();
    for (int i = 0; i < decimals.length; i++) {
      if (decimals[i] != '0') {
        final maxToStringAsFoxed = 20; //toStringAsFixed doc: 0 <= fractionDigits <= 20
        if (i + nonZeroDecimals > maxToStringAsFoxed) {
          return 0.toString();
        }
        final number = x.toStringAsFixed(i + nonZeroDecimals);
        return NumberFormat(_buildDecimalPattern(i + nonZeroDecimals), locale).format(double.parse(number));
      }
    }

    return NumberFormat(Constants.NUMBERFORMATTWODECIMALS, locale).format(x); //all decimals zero
  }

  ///Formats an int according to the locale
  static String formatInt(int x, {String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getNumberFormatLocaleHive();
    }
    return NumberFormat(Constants.NUMBERFORMATNODECIMALS, locale).format(x);
  }

  ///Returns a number format pattern with the given amount of decimals
  static String _buildDecimalPattern(int numberDecimals) {
    String pattern = Constants.NUMBERFORMATTWODECIMALS;
    for (int i = 2; i < numberDecimals; i++) {
      pattern += '#';
    }
    return pattern;
  }

  ///Returns the localized language name for the given locale
  static String localeToLanguageName(String locale, BuildContext context) {
    switch (locale) {
      case "en" : {
        return S.of(context).settingsEnglish;
      }
      case "de" : {
        return S.of(context).settingsGerman;
      }
      default : {
        return "";
      }
    }
  }

  ///Returns the localized name of the given start screen
  static String startScreenToName(StartScreen startScreen, BuildContext context) {
    switch (startScreen) {
      case StartScreen.coinList: {
        return S.of(context).coinsListTabName;
      }
      case StartScreen.favorites: {
        return S.of(context).favoritesTabName;
      }
      case StartScreen.portfolio: {
        return S.of(context).portfolioTabName;
      }
      case StartScreen.alerts: {
        return S.of(context).priceAlertsTabName;
      }
      case StartScreen.settings: {
        return S.of(context).settingsTabName;
      }
    }
  }

  ///Creates a sliding animation from the right side of the screen for a widget
  static Route createSlidingRoute(Widget widget) {
    return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => widget,
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(1.0, 0.0);
          var end = Offset.zero;
          var curve = Curves.ease;

          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

          return SlideTransition(
              position: animation.drive(tween),
              child: child
          );
        }
    );
  }

  ///Generates a secure int id
  static int generateId() {
    return Random.secure().nextInt(1<<32);
  }

  ///Returns a file name with an enumeration if a file with this name already exists in the given directory
  ///e.g. backup -> backup (1)
  static Future<String> getDuplicateFileName(String oldFileName, String folderPath) async {
    final numberDivider = "_";
    final backupDirectory = Directory(folderPath);
    final directoryFiles = await backupDirectory.list().where((event) => event is File).toList();
    int duplicates = 0;
    directoryFiles.forEach((element) {
      final name = element.path.split(Platform.pathSeparator).last;
      if (name.startsWith(oldFileName)) {
        duplicates++;
      }
    });
    if (duplicates > 0) {
      return oldFileName + numberDivider + duplicates.toString();
    }

    return oldFileName;
  }
}

extension listFilter on List {
  ///Return every nth entry of the input list.
  ///Preserve the last list entry from being cut out by default
  List<E> everyNthEntry<E>(int n, {bool preserveLast = true}) {
    List<E> outList = [];
    int maxLength = 0;
    if (preserveLast) {
      maxLength = this.length - 1;
    } else {
      maxLength = this.length;
    }
    for (int i = 0; i < maxLength; i++) {
      if (i % n == 0) {
        outList.add(this[i]);
      }
    }
    if (preserveLast) {
      outList.add(this[this.length - 1]);
    }
    return outList;
  }
}

extension dateComparison on DateTime {
  ///Checks if two dates have the same date (day, month and year are the same)
  bool isSameDate(DateTime other) {
    if (this.year == other.year && this.month == other.month && this.day == other.day)
      return true;
    else
      return false;
  }
}

extension formatDateTime on DateTime {
  ///Converts a DateTime to only the date (dd/mm/yyyy) of the original
  DateTime toDate() {
    return DateTime(this.year, this.month, this.day);
  }

  ///Gets a time string in hh:mm format from DateTime in locale format
  String getTimeOnly({String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getDateFormatLocaleHive();
    }
    return DateFormat.Hm(locale).format(this).toString();
  }

  ///Gets a date string in dd/mm/yyyy format from DateTime in locale format
  String getDateOnly({String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getDateFormatLocaleHive();
    }
    return DateFormat.yMd(locale).format(this).toString();
  }

  ///Gets a date string in mm/yy format from DateTime in locale format.
  String yM({String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getDateFormatLocaleHive();
    }
    return DateFormat.yM(locale).format(this).toString();
  }

  ///Gets a date string in dd/mm format from DateTime in locale format.
  String mD({String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getDateFormatLocaleHive();
    }
    return DateFormat.Md(locale).format(this).toString();
  }

  ///Gets a date time string in yyyy:mm:dd hh:mm format from DateTime in locale format
  String localeFormat({String locale = ""}) {
    if (locale.isEmpty) {
      locale = SettingsDAO().getDateFormatLocaleHive();
    }
    return "${DateFormat.yMd(locale).format(this).toString()} ${DateFormat.Hm().format(this).toString()}";
  }
}

extension Parsing on String {
  ///Parses a double with either a period '.' or a comma ',' as decimal separator
  double parseDouble() {
    if (this.contains(",")) {
      String newText = this.replaceAll(",", ".");
      return double.parse(newText);
    } else {
      return double.parse(this);
    }
  }
}