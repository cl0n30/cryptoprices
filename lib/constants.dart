import 'package:crypto_prices/changeNotifiers/color_change.dart';
import 'package:crypto_prices/changeNotifiers/language_change.dart';
import 'package:flutter/material.dart';

import 'changeNotifiers/theme_change.dart';

///Collection of all constant values used in the app
class Constants {
  static const COINSLISTPAGEINDEX = 0;
  static const FAVORITELISTPAGEINDEX = 1;
  static const PORTFOLIOPAGEINDEX = 2;
  static const PRICEALERTLISTPAGEINDEX = 3;
  static const SETTINGSPAGEINDEX = 4;

  static const DEFAULTSTARTSCREEN = StartScreen.coinList;
  static const STARTSCREENKEY = "startScreen";
  static const TABICONS = [
    Icon(Icons.view_list),
    Icon(Icons.star),
    Icon(Icons.pie_chart),
    Icon(Icons.notifications),
    Icon(Icons.settings)
  ];

  static const DATETIMEDEFAULT = "00000101";

  static const COINSBOXNAME = "coins";
  static const FAVORITESBOXNAME = "favorites";
  static const MARKETDATABOXNAME = "marketData";
  static const COINDETAILBOXNAME = "coinDetails";
  static const AVAILABLECOINSBOXNAME = "availableCoins";
  static const SETTINGSBOXNAME = "settings";
  static const CURRENCYCHANGEDBOXNAME = "currencyChanged";
  static const PRICEALERTSBOXNAME = "priceAlerts";
  static const PRICEALERTLISTENTRIESBOXNAME = "priceAlertListEntries";
  static const WALLETBOXNAME = "portfolios"; //variable name changed
  static const PORTFOLIOBOXNAME = "walletCollections";
  static const TRANSACTIONSBOXNAME = "transactions";
  static const EXPLORERSTRANSACTIONBOXNAME = "explorersTransaction";
  static const GLOBALMARKETINFOBOXNAME = "globalMarketInfo";
  static const COINTHUMBNAILURLSBOXNAME = "coinThumbnailUrls";

  static const COINSLASTUPDATEDKEY = "coinsLastUpdated";
  static const FAVORITESLASTUPDATEDKEY = "favoritesLastUpdated";
  static const MARKETDATALASTUPDATEDKEY = "marketDataLastUpdated";
  static const AVAILABLECOINSLASTUPDATEDKEY = "availableCoinsLastUpdated";
  static const WALLETSLASTUPDETEDKEY = "walletsLastUpdated";
  static const GLOBALMARKETINFOLASTUPDATEDKEY = "globalMarketInfoLastUpdated";
  static const DARKMODEKEY = "darkMode";
  static const SYSTEMDARKMODEKEY = "systemDarkMode";
  static const themeKey = "theme";
  static const CURRENCYKEY = "currency";
  static const CURRENCYDEFAULT = "usd";
  static const LOCALEKEY = "locale";
  static const LOCALEDEFAULT = "";
  static const COINSCURRENCYCHANGEDKEY = "coinsCurrencyChanged";
  static const FAVORITESCURRENCYCHANGEDKEY = "favoritesCurrencyChanged";
  static const MARKETDATACURRENCYCHANGEDKEY = "marketDataCurrencyChanged";
  static const COINDETAILCURRENCYCHANGEDKEY = "coinDetailCurrencyChangedKey";
  static const WALLETSCURRENCYCHANGEDKEY = "walletsCurrencyChanged";

  static const APIGETCOINPRICEKEY = "price";
  static const APIGETCOINCHANGEPERCENTAGEKEY = "changePercentage";

  static const MARKETDATAKEYDAY  = "1";
  static const MARKETDATAKEYWEEK = "7";
  static const MARKETDATAKEYMONTH = "30";
  static const MARKETDATAKEYYEAR = "365";
  static const MARKETDATAKEYMAX = "max";

  static const UPDATETIMERMINUTES = 5;
  static const AVAILABLECOINSUPDATETIMERDAYS = 1;

  static const MAXSEARCHHISTORYLENGTH = 10;

  static const PRICEALERTIDLENGTH = 10;

  static const COLLAPSEALLKEY = "collapseAll";
  static const COLLAPSEALLDEFAULT = false;

  static const PRICEALERTTASKNAME = "priceAlert";
  static const PRICEALERTDATAKEYID = "alertId";
  static const LARGEMOVEALERTTASKNAME = "largeMoveAlert";
  static const LARGEMOVEALERTDATAKEYID = "alertId";
  static const LARGEMOVEALERTLIMITKEY = "largeMoveAlertLimit";
  static const LARGEMOVEALERTTHRESHOLDDEFAULT = 10.0;
  static const LARGEMOVEALERTENABLEDKEY = "largeMoveAlertEnabled";
  static const LARGEMOVEALERTENABLEDDEFAULT = true;
  static const showPriceInPercentageAlertKey = "showPriceInPercentage";
  static const showPriceInPercentageAlertDefault = false;

  static const PRICEALERTNOTIFICATIONCHANNELID = "priceAlerts";
  static const LARGEMOVEALERTNOTIFICATIONCHANNELID = "largeMoveAlerts";
  static const MAXNOTIFICATIONID = 999999999;

  static const GLOBALMARKETINFOKEY = "globalMarketInfo";

  static const PRIMARYCOLORKEY = "primaryColor";
  static var defaultPrimaryColor = Colors.red[900];
  static var accentColorLight = Colors.blueAccent[700];
  static var accentColorDark = Colors.blue[200];
  static var favoriteIconColor = Colors.yellow[800];
  static var priceChangeRedLight = Colors.red[800];
  static var priceChangeRedDark = Colors.red;
  static var priceChangeGreenLight = Colors.green[800];
  static var priceChangeGreenDark = Colors.greenAccent[700];
  static var errorColor = Color(0xFFB00020);
  static var errorColorDark = Color(0xFFCF6679);
  static var darkBackground = Color(0xff212121);

  static var primaryColors = [
    Colors.red,
    Colors.red[900]!,
    Colors.pink,
    Colors.purple,
    Colors.deepPurple,
    Colors.indigo,
    Colors.blue[900]!,
    Colors.lightBlue[600]!,
    Colors.cyan[600]!,
    Colors.teal,
    Colors.green,
    Colors.green[800]!,
    Colors.lime[900]!,
    Colors.deepOrange,
    Colors.deepOrange[900]!,
    Colors.brown,
    Colors.brown[800]!,
    Colors.grey[850]!,
    Colors.blueGrey,
    Colors.black,
  ];

  static const NUMBERFORMATNODECIMALS = ",##0";
  static const NUMBERFORMATTWODECIMALS = ",##0.00";

  static final signedNumberExp = RegExp(r"^-?\d+(([,.]\d+)|(\d*))$");
  static final positiveNumberExp = RegExp(r"^\d+(([,.]\d+)|(\d*))$");

  static const dateFormatLocaleKey = "dateFormatLocale";
  ///Formatting options for settings list
  static const dateFormatLocales = [
    "en_US",
    "en_GB",
    "de_DE"
  ];
  static const numberFormatLocaleKey = "numberFormatLocale";
  ///Formatting options for settings list
  static const numberFormatLocales = [
    "en_US",
    "de_DE"
  ];

  static const List<String> supportedCurrencies = [
    "usd", "aed", "ars", "aud", "bdt", "bhd",
    "bmd", "brl", "cad", "chf", "clp", "cny",
    "czk", "dkk", "eur", "gbp", "hkd", "huf",
    "idr", "ils", "inr", "jpy", "krw", "kwd",
    "lkr", "mmk", "mxn", "myr", "ngn", "nok",
    "nzd", "php", "pkr", "pln", "rub", "sar",
    "sek", "sgd", "thb", "try", "twd", "uah",
    "vef", "vnd", "zar", "btc", "eth", "ltc",
    "bch", "bnb", "eos", "xrp", "xlm", "link",
    "dot", "yfi",
  ];

  static const Map<String, String> currencySymbols = {
    "usd":"\$",
    "aed":"DH",
    "ars":"\$",
    "aud":"AU\$",
    "bdt":"৳",
    "bhd":"BD",
    "bmd":"\$",
    "brl":"R\$",
    "cad":"CA\$",
    "chf":"Fr.",
    "clp":"CLP\$",
    "cny":"CN¥",
    "czk":"Kč",
    "dkk":"Kr.",
    "eur":"€",
    "gbp":"£",
    "hkd":"HK\$",
    "huf":"Ft",
    "idr":"Rp",
    "ils":"₪",
    "inr":"₹",
    "jpy":"¥",
    "krw":"₩",
    "kwd":"KD",
    "lkr":"Rs",
    "mmk":"K",
    "mxn":"MX\$",
    "myr":"RM",
    "ngn":"₦",
    "nok":"kr",
    "nzd":"NZ\$",
    "php":"₱",
    "pkr":"₨",
    "pln":"zł",
    "rub":"₽",
    "sar":"SR",
    "sek":"kr",
    "sgd":"S\$",
    "thb":"฿",
    "try":"₺",
    "twd":"NT\$",
    "uah":"₴",
    "vef":"Bs.F",
    "vnd":"₫",
    "zar":"R",
    "btc":"BTC",
    "eth":"ETH",
    "ltc":"LTC",
    "bch":"BCH",
    "bnb":"BNB",
    "eos":"EOS",
    "xrp":"XRP",
    "xlm":"XLM",
    "link":"LINK",
    "dot":"DOT",
    "yfi":"YFI",
  };

  static const Map<String, String> currencyText = {
    "usd":"\$ - USD",
    "aed":"DH - AED",
    "ars":"\$ - ARS",
    "aud":"AU\$ - AUD",
    "bdt":"৳ - BDT",
    "bhd":"BD - BHD",
    "bmd":"\$ - BMD",
    "brl":"R\$ - BRL",
    "cad":"CA\$ - CAD",
    "chf":"Fr. - CHF",
    "clp":"CLP\$ - CLP",
    "cny":"CN¥ - CNY",
    "czk":"Kč - CZK",
    "dkk":"Kr. - DKK",
    "eur":"€ - EUR",
    "gbp":"£ - GBP",
    "hkd":"HK\$ - HKD",
    "huf":"Ft - HUF",
    "idr":"Rp - IDR",
    "ils":"₪ - ILS",
    "inr":"₹ - INR",
    "jpy":"¥ - JPY",
    "krw":"₩ - KRW",
    "kwd":"KD - KWD",
    "lkr":"Rs - LRK",
    "mmk":"K - MMK",
    "mxn":"MX\$ - MXN",
    "myr":"RM - MYR",
    "ngn":"₦ - NGN",
    "nok":"kr - NOK",
    "nzd":"NZ\$ - NZD",
    "php":"₱ - PHP",
    "pkr":"₨ - PKR",
    "pln":"zł - PLN",
    "rub":"₽ - RUB",
    "sar":"SR - SAR",
    "sek":"kr - SEK",
    "sgd":"S\$ - SGD",
    "thb":"฿ - THB",
    "try":"₺ - TRY",
    "twd":"NT\$ - TWD",
    "uah":"₴ - UAH",
    "vef":"Bs.F - VEF",
    "vnd":"₫ - VND",
    "zar":"R - ZAR",
    "btc":"BTC - BTC",
    "eth":"ETH - ETH",
    "ltc":"LTC - LTC",
    "bch":"BCH - BCH",
    "bnb":"BNB - BNB",
    "eos":"EOS - EOS",
    "xrp":"XRP - XRP",
    "xlm":"XLM - XLM",
    "link":"LINK - LINK",
    "dot":"DOT - DOT",
    "yfi":"YFI - YFI",
  };

  static const gitLabUrl = "https://gitlab.com/cl0n30/cryptoprices";
  static const coinGeckoUrl = "https://www.coingecko.com/";
  static const coinGeckoPricingUrl = "https://www.coingecko.com/en/api/pricing";
  static var emailLaunchUri = Uri(scheme: "mailto", path: "clone.appde@gmail.com");
  static const playStoreUrl = "https://play.google.com/store/apps/details?id=de.cloneapps.crypto_prices";
  static const liberapayUrl = "https://liberapay.com/CryptoPrices";
  static const btcDonationAddress = "bc1qsd3x37d7dea2ghcrmvmgdthl7723gf8ja0j7va";
  static const ethDonationAddress = "0x24F34C5C88c61C9578Ce607D30e7d5b9A94b7d38";

  static const EXPORTFILENAME = "crypto_prices_export";
  static const BACKUPFILENAME = "crypto_prices_backup";
  static const EXPORTTIMEKEY = "exportTime";
  static const EXPORTJSONFAVORITEKEY = "favorites";
  static const EXPORTJSONALERTSKEY = "alerts";
  static const EXPORTTRANSACTIONSKEY = "transactions";
  static const EXPORTWALLETSKEY = "wallets";
  static const OLDEXPORTWALLETSKEY = "portfolios"; //only DB-version 3 (1.3.1) and below
  static const EXPORTPORTFOLIOSKEY = "portfolios";
  static const autoBackupPathKey = "autoBackupPath";
  static const autoBackupPathDefault = "";
  static const autoBackupDateKey = "autoBackupDate";
  static const autoBackupFileNameKey = "autoBackupFileName";
  static const autoBackupOverwriteFileKey = "autoBackupOverwriteFile";
  static const autoBackupOverwriteFileDefault = true;

  static const DATABASEVERSIONKEY = "databaseVersion";
  static const DATABASEVERSION = 4;

  ///Version before multiple portfolios were added
  static const DATABASEVERSIONMULTIPLEPORTFOLIOS = 3;

  static const APPVERSIONKEY = "appVersion";
  static const APPVERSIONDEFAULT = "";

  static const ADDWALLETTOFAVORITESKEY = "addPortfolioToFavorites";
  static const ADDWALLETTOFAVORITESDEFAULT = true;
  static const PORTFOLIOSELECTEDPERIODKEY = "portfolioSelectedPeriod";
  static const USEOWNCOINPRICEKEY = "useOwnCoinPrice";
  static const USEOWNCOINPRICEDEFAULT = true;
  static const DISPLAYINPORTFOLIOCURRENCYKEY = "displayInPortfolioCurrency";
  static const DISPLAYINPORTFOLIOCURRENCYDEFAULT = false;
  static const DEFAULTPORTFOLIOIDKEY = "defaultPortfolioId";
  static const NODEFAULTPORTFOLIOID = 0;

  static const DELETERESULT = -1;
  static const successResult = 1;

  static const thumbnailsFileName = "thumbnails";
  static const thumbnailFileUrl = "https://drive.google.com/uc?export=download&id=1jNO0O7nE6hDvHN3HNJs8H1i9hYIof3By";
  static const thumbnailsLastUpdatedKey = "thumbnailsLastUpdated";
  static const thumbnailsUpdateTimerDays = 7;

  static const privateModeKey = "privateMode";
  static const privateModeDefault = false;

  static const passwordKey = "password";
  static const passwordDefault = "";
  static const biometricsAllowedKey = "biometricsAllowed";
  static const biometricsAllowedDefault = true;
  static const biometricsAvailableKey = "biometricsAvailable";
  static const biometricsAvailableDefault = false;
  static const isAuthenticatedKey = "isAuthenticated";
  static const isAuthenticatedDefault = true;
  static const wasAuthenticatedKey = "wasAuthenticated";
  static const wasAuthenticatedDefault = true;
  static const openAuthOnAppStartKey = "openAuthOnAppStart";
  static const openAuthOnAppStartDefault = true;
  static const stayAuthenticatedMinutesKey = "stayAuthenticatedMinutes";
  static const stayAuthenticatedMinutesDefault = 5;
  static const stayAuthenticatedMinutes = [-1, 5, 10, 15, 30];
  static const stayAuthenticatedLongDifference = 100;
  static const appClosedTimeKey = "appClosedTime";

  static const sendToBackMethodChannel = "android_app_retain";
  static const sendToBackgroundMethod = "sendToBackground";
}

///Available time periods for the coin data
enum TimePeriod { hour, day, week, month, year, max }

///Possible start screens
enum StartScreen { coinList, favorites, portfolio, alerts, settings }

//Initialize the ThemeChange notifier to use it at app start
ThemeChange currentTheme = ThemeChange();

//Initialize the LanguageChange notifier to use it at app start
LanguageChange languageChange = LanguageChange();

//Initialize the ColorChange notifier to use it at app start
ColorChange colorChange = ColorChange();
