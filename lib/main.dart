
import 'dart:io';

import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/changeNotifiers/theme_change.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/explorer_transaction_url_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/isar.g.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/models/screen_arguments.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/import_export.dart';
import 'package:crypto_prices/util/manual_migration_handler.dart';
import 'package:crypto_prices/util/notification_handler.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/detailScreen/detail_screen_base.dart';
import 'package:crypto_prices/widgets/initial_page.dart';
import 'package:crypto_prices/widgets/priceAlertsScreen/price_alert_edit_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/authentication_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/migration_assistant.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:isar/isar.dart';
import 'package:local_auth/local_auth.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants.dart';
import 'generated/l10n.dart';
import 'models/available_coin.dart';
import 'models/coin_detail.dart';
import 'models/date_price.dart';
import 'models/favorite_coin.dart';
import 'models/market_data.dart';
import 'util/workmanager_handler.dart';
import 'widgets/detailScreen/coin_detail_screen.dart';
import 'widgets/home_page.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();

  Hive.registerAdapter(CoinDetailAdapter()); //register subclasses first
  Hive.registerAdapter(CoinAdapter());
  Hive.registerAdapter(MarketDataAdapter());
  Hive.registerAdapter(DatePriceAdapter());
  Hive.registerAdapter(AvailableCoinAdapter());
  Hive.registerAdapter(FavoriteCoinAdapter());
  Hive.registerAdapter(PortfolioAdapter());
  Hive.registerAdapter(WalletAdapter());
  Hive.registerAdapter(TransactionAdapter());
  Hive.registerAdapter(ExplorerTransactionUrlAdapter());
  Hive.registerAdapter(GlobalMarketInfoAdapter());

  //do before opening boxes to prevent incompatibility  errors
  int currentDatabaseVersion = await SettingsDAO().getDatabaseVersionPrefs();
  if (currentDatabaseVersion != Constants.DATABASEVERSION) {
    await migrateDatabase(currentDatabaseVersion);
    SettingsDAO().setDatabaseVersion(Constants.DATABASEVERSION);
  }

  await Hive.openBox(Constants.COINSBOXNAME);
  await Hive.openBox(Constants.FAVORITESBOXNAME);
  await Hive.openBox(Constants.MARKETDATABOXNAME);
  await Hive.openBox(Constants.COINDETAILBOXNAME);
  await Hive.openBox(Constants.AVAILABLECOINSBOXNAME);
  await Hive.openBox(Constants.SETTINGSBOXNAME);
  await Hive.openBox(Constants.CURRENCYCHANGEDBOXNAME);
  await Hive.openBox<Portfolio>(Constants.PORTFOLIOBOXNAME);
  await Hive.openBox<Wallet>(Constants.WALLETBOXNAME);
  await Hive.openBox<Transaction>(Constants.TRANSACTIONSBOXNAME);
  await Hive.openBox<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
  await Hive.openBox(Constants.GLOBALMARKETINFOBOXNAME);
  await Hive.openBox(Constants.COINTHUMBNAILURLSBOXNAME);

  isar = await openIsar();

  SettingsDAO settingsDAO = SettingsDAO();

  WorkmanagerHandler().initializeWorkmanager();

  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  String currentVersion = packageInfo.version;

  //restart all alert background tasks after installing an app update
  if (settingsDAO.getCurrentAppVersionHive() != currentVersion) {
    WorkmanagerHandler().restartAllAlerts();
    settingsDAO.setCurrentAppVersion(currentVersion);
  }

  String initialRoute = "";
  InitialPageArguments initialPageArguments = InitialPageArguments();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (!prefs.containsKey(Constants.numberFormatLocaleKey)) {
    settingsDAO.setNumberFormatLocale(Platform.localeName); //Platform localeName is "und" in background task on real device
  }
  NotificationHandler notificationHandler = NotificationHandler();
  notificationHandler.initialize(onSelectNotification);

  NotificationAppLaunchDetails? notificationAppLaunchDetails =
    await notificationHandler.plugin.getNotificationAppLaunchDetails();

  if (notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
    String? payload = notificationAppLaunchDetails!.payload;
    if (payload != null) {
      initialRoute += CoinDetailScreen.routeName;
      initialPageArguments.coinId = payload.split("_").first;
      initialPageArguments.coinName = payload.split("_").last;
    }
  }

  ImageUrls().decodeThumbnails(); //parse image urls in background

  AvailableCoinsDao availableCoinsDao = AvailableCoinsDao();
  final oldSupportedCoins = AvailableCoinsDao().getAllAvailableCoins();

  DateTime? availableCoinsLastUpdated = settingsDAO.getAvailableCoinsLastUpdated();
  if (oldSupportedCoins.length == 0
      || availableCoinsLastUpdated == null
      || DateTime.now().difference(availableCoinsLastUpdated).inDays >= Constants.AVAILABLECOINSUPDATETIMERDAYS) {
    CoinGeckoAPI().getAvailableCoins().then((value) {
      availableCoinsDao.updateAllAvailableCoins(value);
      settingsDAO.setAvailableCoinsLastUpdated(DateTime.now());
    });
  }

  if (Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME).isEmpty
      || settingsDAO.getCurrentAppVersionHive() != currentVersion) {
    ExplorerTransactionUrlDao().decodeDefaultTransactionUrls();
  }

  if (ManualMigrationHandler().isManualMigrationNeeded()) {
    initialRoute += MigrationAssistant.routeName;
  }

  final biometricsAvailable = await LocalAuthentication().canCheckBiometrics;
  settingsDAO.setBiometricsAvailable(biometricsAvailable);
  if (!Hive.box(Constants.SETTINGSBOXNAME).containsKey(Constants.biometricsAllowedKey)) {
    //on the first app start. if available, allow
    settingsDAO.setBiometricsAllowed(biometricsAvailable);
  }

  final wasAuthenticated = settingsDAO.getWasAuthenticated(); //was authenticated when closing the app
  final authTimeDifference = (wasAuthenticated)
      ? DateTime.now().difference(settingsDAO.getAppClosedTime()).inMinutes
      : Constants.stayAuthenticatedLongDifference;
  if (settingsDAO.getPassword().isNotEmpty
      && settingsDAO.getOpenAuthOnAppStart()
      && authTimeDifference >= settingsDAO.getStayAuthenticatedMinutes()
  ) {
    //open password screen
    settingsDAO.setIsAuthenticated(false);
    initialRoute += AuthenticationWidget.routeName;
  } else if (settingsDAO.getPassword().isNotEmpty
      && !settingsDAO.getOpenAuthOnAppStart()
      && authTimeDifference >= settingsDAO.getStayAuthenticatedMinutes()
  ) {
    //don't open password screen on startup
    settingsDAO.setIsAuthenticated(false);
    settingsDAO.setPrivateMode(true);
  } else {
    //no password or within set time window
    settingsDAO.setIsAuthenticated(true);
  }

  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  androidDeviceInfo = await deviceInfo.androidInfo;

  final permission = await Permission.storage.status;
  if (!permission.isGranted) {
    settingsDAO.setAutoBackupPath("");
  }

  runApp(MyApp(initialRoute: initialRoute, initialPageArguments: initialPageArguments));
}

final navKey = new GlobalKey<NavigatorState>();

Isar? isar;

AndroidDeviceInfo? androidDeviceInfo;

Future<dynamic> onSelectNotification(String? payload) async {
  if (payload != null) {
    String coinId = payload.split("_").first;
    String coinName = payload.split("_").last;
    navKey.currentState!.push(MaterialPageRoute(
        builder: (_) => DetailScreenBase(coinId: coinId))
    );
  }
}

///Migrates the database from the old version to the new
///Version 2: save MarketData objects in the MarketDataBox instead of CoinDetail objects
///Version 3: add transaction actualPrice. Set actual price to coinPrice in old transactions
///Version 4: add multiple portfolios. Wallets now have int ids
Future<void> migrateDatabase(int oldVersion) async {
  if (oldVersion == 1) {
    Hive.deleteBoxFromDisk(Constants.MARKETDATABOXNAME);
    oldVersion++;
  }

  if (oldVersion == 2) {
    final box = await Hive.openBox<Transaction>(Constants.TRANSACTIONSBOXNAME);
    List<Transaction> transactions = box.values.toList();
    transactions.forEach((element) {
      element.actualCoinPrice = element.coinPrice;
      element.save();
    });
    oldVersion++;
  }

  if (oldVersion == 3) {
    await Hive.openBox<Transaction>(Constants.TRANSACTIONSBOXNAME);
    await Hive.openBox<Portfolio>(Constants.PORTFOLIOBOXNAME);
    await Hive.openBox(Constants.SETTINGSBOXNAME);
    await Hive.openBox<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
    final walletBox = await Hive.openBox<Wallet>(Constants.WALLETBOXNAME);

    if (walletBox.isNotEmpty) { //only add new portfolio if wallet data exists
      final newPortfolio = Portfolio(
          Util.generateId(),
          "Portfolio 1",
          currency: SettingsDAO().getCurrencyHive()
      );

      WalletDao walletDao = WalletDao();
      List<Wallet> wallets = walletDao.getAllWallets();
      wallets.forEach((wallet) {
        wallet.id = Util.generateId();
        wallet.connectedPortfolioId = newPortfolio.id;
      });

      PortfolioDao().insertPortfolio(newPortfolio);

      await walletBox.clear();
      walletDao.insertMultipleWallets(wallets);

      final transactions = TransactionDao().getAllTransactions();
      newPortfolio.insertMultipleTransaction(transactions);
    }

    //bittorrent coinId changed
    ExplorerTransactionUrlDao explorerTransactionUrlDao = ExplorerTransactionUrlDao();
    if (explorerTransactionUrlDao.getCoinExplorerTransactionUrl("bittorrent-2") != null) {
      final url = explorerTransactionUrlDao.getCoinExplorerTransactionUrl("bittorrent-2")!;
      url.coinId = "bittorrent";
      explorerTransactionUrlDao.deleteCoinExplorerTransactionUrl("bittorrent-2");
      explorerTransactionUrlDao.insertCoinExplorerTransactionUrl(url);
    }

    oldVersion++;
  }
}

class MyApp extends StatefulWidget {
  final String initialRoute;

  final InitialPageArguments initialPageArguments;

  MyApp({
    Key? key,
    required this.initialRoute,
    required this.initialPageArguments
  });

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  late ThemeData _lightTheme;

  late ThemeData _standardDarkTheme;

  late ThemeData _amoledDarkTheme;

  late ThemeData _appTheme;

  Color? _primaryColor = Constants.defaultPrimaryColor;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);

    //rebuild app when theme or language changes
    currentTheme.addListener(() {
      setState(() {
        _appTheme = _getAppTheme();
      });
    });
    languageChange.addListener(() {
      setState(() {});
    });
    colorChange.addListener(() {
      setState(() {
        _primaryColor = colorChange.currentPrimaryColor();
        _standardDarkTheme = _standardDarkTheme.copyWith(
          primaryColor: _primaryColor,
          colorScheme: _standardDarkTheme.colorScheme.copyWith(surface: _primaryColor)
        );
        _lightTheme = _lightTheme.copyWith(
          primaryColor: _primaryColor,
          colorScheme: _lightTheme.colorScheme.copyWith(primary: _primaryColor)
        );
        _appTheme = _getAppTheme();
      });
    });

    _primaryColor = colorChange.currentPrimaryColor();

    _lightTheme = ThemeData(
      primaryColor: _primaryColor, //for color of title bar in app history
      textButtonTheme: TextButtonThemeData(style: TextButton.styleFrom(primary: Constants.accentColorLight!)),
      snackBarTheme: SnackBarThemeData(actionTextColor: Constants.accentColorDark),
      colorScheme: ColorScheme.light().copyWith(
        primary: _primaryColor,
        secondary: Constants.accentColorLight,
        onSecondary: Colors.white
      ),
    );

    _standardDarkTheme = ThemeData(
      primaryColor: _primaryColor, //for color of title bar in app history
      textButtonTheme: TextButtonThemeData(style: TextButton.styleFrom(primary: Constants.accentColorDark!)),
      snackBarTheme: SnackBarThemeData(actionTextColor: Constants.accentColorLight),
      colorScheme: ColorScheme.dark().copyWith(
        surface: _primaryColor,
        primary: Constants.accentColorDark,
        secondary: Constants.accentColorDark
      ),
    );

    _amoledDarkTheme = ThemeData(
      primaryColor: Colors.black, //for color of title bar in app history
      textButtonTheme: TextButtonThemeData(style: TextButton.styleFrom(primary: Constants.accentColorDark!)),
      cardColor: Colors.black,
      popupMenuTheme: PopupMenuTheme.of(context).copyWith(color: Constants.darkBackground),
      dialogBackgroundColor: Constants.darkBackground,
      snackBarTheme: SnackBarThemeData(
        backgroundColor: Constants.darkBackground,
        actionTextColor: Constants.accentColorDark,
        contentTextStyle: TextStyle(color: Colors.white)
      ),
      scaffoldBackgroundColor: Colors.black,
      bottomNavigationBarTheme: BottomNavigationBarTheme.of(context).copyWith(backgroundColor: Colors.black),
      colorScheme: ColorScheme.dark().copyWith(
        surface: Colors.black,
        background: Colors.black,
        primary: Constants.accentColorDark,
        secondary: Constants.accentColorDark
      ),
    );

    _appTheme = _getAppTheme();
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.paused: {
        final settingsDao = SettingsDAO();
        final isAuthenticated = settingsDao.getIsAuthenticated();
        settingsDao.setAppClosedTime(DateTime.now());
        settingsDao.setWasAuthenticated(settingsDao.getIsAuthenticated());
        await _backupDataInternal();
        if (isAuthenticated) {
          await _autoBackup();
        }
        break;
      }

      default: {
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (context) => S.of(context).appName,
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: S.delegate.supportedLocales,
      locale: languageChange.currentLocale(), //use system locale (if supported) if null
      theme: _appTheme,
      navigatorKey: navKey,
      home: InitialPage(initialRoute: widget.initialRoute, routeArguments: widget.initialPageArguments),
    );
  }

  ///Creates a backup file of all data in the internal app directory
  Future<void> _backupDataInternal() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String internalBackupFilePath = appDocDir.path + "/${Constants.BACKUPFILENAME}.json";
    await ImportExport().exportData(internalBackupFilePath);
    print("backup complete");
  }

  ///Creates a backup file of all data in the directory set by the user
  Future<void> _autoBackup() async {
    SettingsDAO settingsDAO = SettingsDAO();
    final path = settingsDAO.getAutoBackupPath();
    final permission = await Permission.storage.status;
    if (permission.isGranted && path.isNotEmpty) {
      settingsDAO.setAutoBackupDate(DateTime.now());
      String fileName = settingsDAO.getAutoBackupFileName();
      if (!settingsDAO.getAutoBackupOverwriteFile()) {
        fileName = await Util.getDuplicateFileName(fileName, path);
      }
      String backupFilePath = path + "/$fileName.json";
      await ImportExport().exportData(backupFilePath);
    }
  }

  ///Gets the current theme and sets appTheme accordingly
  ThemeData _getAppTheme() {
    final theme = currentTheme.currentTheme();
    switch (theme) {
      case Themes.SYSTEM: {
        if (SchedulerBinding.instance!.window.platformBrightness == Brightness.dark) {
          return _standardDarkTheme;
        } else {
          return _lightTheme;
        }
      }
      case Themes.SYSTEM_AMOLED: {
        if (SchedulerBinding.instance!.window.platformBrightness == Brightness.dark) {
          return _amoledDarkTheme;
        } else {
          return _lightTheme;
        }
      }
      case Themes.LIGHT: {
        return _lightTheme;
      }
      case Themes.DARK: {
        return _standardDarkTheme;
      }
      case Themes.AMOLED_DARK: {
        return _amoledDarkTheme;
      }
    }
  }
}
