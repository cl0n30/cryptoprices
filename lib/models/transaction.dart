import 'package:hive/hive.dart';

part 'transaction.g.dart';

@HiveType(typeId: 7)
class Transaction extends HiveObject{
  ///The ID of the transaction
  @HiveField(0)
  int id;

  ///The ID of the coin that this transaction belong to
  @HiveField(1)
  String coinId;

  ///The ID of the wallet to which this transaction belongs to
  @HiveField(17, defaultValue: 0)
  int connectedWalletId;

  ///The name of the coin that this transaction belong to
  @HiveField(2, defaultValue: "")
  String coinName;

  ///The symbol of the coin that this transaction belong to
  @HiveField(3, defaultValue: "")
  String coinSymbol;

  ///The volume of coins in this transaction
  @HiveField(4, defaultValue: 0)
  double coinVolume;

  ///The volume of coins in the selected currency
  @HiveField(11, defaultValue: 0)
  double currencyVolume;

  ///The current price of the coin
  @HiveField(5, defaultValue: 0)
  double coinPrice;

  ///The coin price returned from the api.
  ///Used instead of coinPrice for value calculation if coinPrice is 0
  @HiveField(14, defaultValue: 0)
  double actualCoinPrice;

  ///The currency that the coin price is in
  @HiveField(12, defaultValue: "")
  String currency;

  ///The date of the transaction
  @HiveField(6)
  DateTime? transactionDate;

  ///The name of the transaction
  @HiveField(7, defaultValue: "")
  String transactionName;

  ///A description of the transaction
  @HiveField(8, defaultValue: "")
  String description;

  ///The transaction fee in coins
  @HiveField(15, defaultValue: 0)
  double feeCoin;

  ///The transaction fee in currency
  @HiveField(16, defaultValue: 0)
  double feeCurrency;

  ///The source address of the transaction
  @HiveField(9, defaultValue: "")
  String sourceAddress;

  ///The destination address of the transaction
  @HiveField(10, defaultValue: "")
  String destinationAddress;

  ///The transaction id on the blockchain
  @HiveField(13, defaultValue: "")
  String transactionId;

  Transaction(
      this.id,
      this.coinId,
      this.connectedWalletId,

  {   this.coinName = "",
      this.coinSymbol = "",

      this.coinVolume = 0,
      this.currencyVolume = 0,

      this.coinPrice = 0,
      this.actualCoinPrice = 0,
      this.currency = "",

      this.transactionDate,

      this.transactionName = "",
      this.description = "",
      this.feeCoin = 0,
      this.feeCurrency = 0,
      this.sourceAddress = "",
      this.destinationAddress = "",
      this.transactionId = ""
  });

  ///Returns the coin value when using the price entered by the user
  double get coinValue {
    return coinVolume * coinPrice;
  }

  ///Returns the coin value when using the actual coin price at the time of the transaction
  double get actualValue {
    return coinVolume * actualCoinPrice;
  }

  ///Converts a Portfolio object to json
  Map<String, dynamic> toJson() => {
    "id": id,
    "coinId": coinId,
    "connectedWalletId": connectedWalletId,
    "coinName": coinName,
    "coinSymbol": coinSymbol,
    "coinVolume": coinVolume,
    "currencyVolume": currencyVolume,
    "coinPrice": coinPrice,
    "actualCoinPrice": actualCoinPrice,
    "currency": currency,
    "transactionDate": transactionDate?.toIso8601String(),
    "transactionName": transactionName,
    "description": description,
    "feeCoin": feeCoin,
    "feeCurrency": feeCurrency,
    "sourceAddress": sourceAddress,
    "destinationAddress": destinationAddress,
    "transactionId": transactionId
  };

  ///Converts json to a Transaction object
  Transaction.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        coinId = json["coinId"],
        connectedWalletId = json["connectedWalletId"] ?? 0,
        coinName = json["coinName"] ?? "",
        coinSymbol = json["coinSymbol"] ?? "",
        coinVolume = json["coinVolume"] ?? 0,
        currencyVolume = json["currencyVolume"] ?? 0,
        coinPrice = json["coinPrice"] ?? 0,
        actualCoinPrice = json["actualCoinPrice"] ?? (json["coinPrice"] ?? 0),
        currency = json["currency"] ?? "",
        transactionDate = DateTime.parse(json["transactionDate"] ?? DateTime.now().toIso8601String()),
        transactionName = json["transactionName"] ?? "",
        description = json["description"] ?? "",
        feeCoin = json["feeCoin"] ?? 0,
        feeCurrency = json["feeCurrency"] ?? 0,
        sourceAddress = json["sourceAddress"] ?? "",
        destinationAddress = json["destinationAddress"] ?? "",
        transactionId = json["transactionId"] ?? "";
}