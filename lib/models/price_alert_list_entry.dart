import 'package:isar/isar.dart';

///PriceAlertListEntry class that contains the position of the price alerts card for a coin
///in the price alerts list
@Collection()
class PriceAlertListEntry {
  ///The ID of this entry
  int id;

  ///The ID of the coin that this entry belongs to
  @Index()
  String coinId;

  ///The name of the coin that this entry belongs to
  String coinName;

  ///The name of the coin that this entry belongs to
  String coinSymbol;

  ///The position of this entry in the price alert list
  int listPosition;

  ///Is this entry collapsed or not
  @Index()
  bool isCollapsed;

  PriceAlertListEntry({
    this.id = 0,
    this.coinId = "",
    this.coinName = "",
    this.coinSymbol = "",
    this.listPosition = 0,
    this.isCollapsed = false
  });
}