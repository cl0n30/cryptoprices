import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'portfolio.g.dart';

///The Portfolio class is a collection of coin wallets.
///The currency of all price values in the portfolio can be set independently from the app currency.
@HiveType(typeId: 10)
class Portfolio extends HiveObject {
  ///The ID of the portfolio
  @HiveField(0)
  int id;

  ///The name of the portfolio
  @HiveField(1)
  String name;

  ///The currency in which the portfolio should be displayed
  @HiveField(2, defaultValue: "")
  String currency;

  ///Should the portfolio use the current app currency instead of its own?
  @HiveField(4, defaultValue: true)
  bool useAppCurrency;

  ///The wallets in this portfolio
  @HiveField(3)
  HiveList<Wallet>? wallets;

  Portfolio(
      this.id,
      this.name,

  {
      this.currency = "",
      this.useAppCurrency = true,
      this.wallets
  });

  ///Returns the list of wallets or an empty list if null
  HiveList<Wallet> get allWallets {
    if (wallets == null) {
      wallets = HiveList<Wallet> (Hive.box<Wallet>(Constants.WALLETBOXNAME));
      return wallets!;
    } else {
      return wallets!;
    }
  }

  ///Adds a wallet to this portfolio if it doesn't already exist
  void addWallet(Wallet wallet) {
    if (!allWallets.contains(wallet)) {
      allWallets.add(wallet);
      save();
    }
  }

  ///Returns the wallet of the given coin from this portfolio
  Wallet? getPortfolioWalletByCoinId(String coinId) {
    if (allWallets.length == 0) {
      return null;
    }

    for (int i = 0; i < allWallets.length; i++) {
      if (allWallets[i].coinId == coinId) {
        return wallets![i];
      }
    }
    return null;
  }

  ///Deletes the wallet of the given coin from this portfolio
  void deletePortfolioWalletByCoinId(String coinId) {
    if (allWallets.length == 0) {
      return;
    }

    for (int i = 0; i < allWallets.length; i++) {
      if (allWallets[i].coinId == coinId) {
        WalletDao().deleteWallet(allWallets[i].id);
      }
    }
  }

  ///Deletes all wallets from this portfolio
  void deleteAllPortfolioWallets() {
    WalletDao().deleteAllPortfolioWallets(this.id);
  }

  ///Inserts one transaction into this portfolio
  void insertTransaction(Transaction transaction) {
    TransactionDao().insertTransaction(transaction, this);
  }

  ///Inserts multiple transactions into this portfolio
  void insertMultipleTransaction(List<Transaction> transactions) {
    TransactionDao().insertMultipleTransaction(transactions, this);
  }

  ///Converts a Portfolio object to json
  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "currency": currency,
    "useAppCurrency": useAppCurrency,
    "wallets": (allWallets.map((e) => e.id).toList())
  };

  ///Converts json to a Portfolio object.
  ///Portfolio wallets have to be added separately
  Portfolio.fromJson(Map<String, dynamic> json)
    : id = json["id"],
      name = json["name"],
      currency = json["currency"] ?? "",
      useAppCurrency = json["useAppCurrency"] ?? true;

}