// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'explorer_transaction_url.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ExplorerTransactionUrlAdapter
    extends TypeAdapter<ExplorerTransactionUrl> {
  @override
  final int typeId = 8;

  @override
  ExplorerTransactionUrl read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ExplorerTransactionUrl(
      fields[0] as String,
      url: fields[1] == null ? '' : fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ExplorerTransactionUrl obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.coinId)
      ..writeByte(1)
      ..write(obj.url);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ExplorerTransactionUrlAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
