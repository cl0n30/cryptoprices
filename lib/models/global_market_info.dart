import 'package:hive_flutter/hive_flutter.dart';

part 'global_market_info.g.dart';

///Global information on the entire crypto market
@HiveType(typeId: 9)
class GlobalMarketInfo extends HiveObject {
  ///The total market cap of all coins
  @HiveField(0)
  double totalMarketCap;

  ///The change of the total market cap in the last 24H in percent
  @HiveField(1, defaultValue: 0)
  double marketCapChangePercentage24H;

  ///The total trading volume in the last 24H
  @HiveField(2, defaultValue: 0)
  double total24HVolume;

  ///The number of currently active cryptocurrencies
  @HiveField(3, defaultValue: 0)
  int activeCryptoCurrencies;

  ///The 10 coins with the highest market share and their share percentages
  @HiveField(4, defaultValue: const {})
  Map<String, double> marketCapCoinPercentages = const {};

  GlobalMarketInfo(
      this.totalMarketCap,
      {
        this.marketCapChangePercentage24H = 0,
        this.total24HVolume = 0,
        this.activeCryptoCurrencies = 0,
        this.marketCapCoinPercentages = const {}
      }
  );

  ///Converts json to a GlobalMarketInfo object
  GlobalMarketInfo.fromJson(Map<String, dynamic> json, String currency)
      : totalMarketCap = json['total_market_cap'][currency] ?? 0,
        marketCapChangePercentage24H = json['market_cap_change_percentage_24h_usd'] ?? 0,
        total24HVolume = json['total_volume'][currency] ?? 0,
        activeCryptoCurrencies = json['active_cryptocurrencies'] ?? 0
  {
    marketCapCoinPercentages = _jsonMapToDoubleMap(json['market_cap_percentage'] ?? {});
  }


  ///Converts a map from a json object to a String/double map
  Map<String, double> _jsonMapToDoubleMap(Map<String, dynamic> json) {
    Map<String, double> map = {};
    json.forEach((key, value) {
      map[key] = value;
    });
    return map;
  }
}