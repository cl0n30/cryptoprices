import 'package:isar/isar.dart';

///Large price move alert class with alert information
@Collection()
class LargeMoveAlert {
  ///The alert ID
  int id;

  ///The ID of the coin that this alert belong to
  @Index() //for faster querying
  String coinId;

  ///The name of the coin that this alert belong to
  String coinName;

  ///The symbol of the coin that this alert belong to
  String coinSymbol;

  ///The currency that is used for API requests
  ///and that should be displayed in the notification
  String currency;

  ///The date of the last alert notification that has been sent
  DateTime? lastNotificationSent;

  LargeMoveAlert({
    this.id = 0,
    this.coinId = "",
    this.coinName = "",
    this.coinSymbol = "",
    this.currency = "",
    this.lastNotificationSent
  });
}