import 'package:hive/hive.dart';

part 'date_price.g.dart';

///Price on a specific date. Used in MarketData
@HiveType(typeId: 3)
class DatePrice extends HiveObject {
  ///The Date
  @HiveField(0)
  DateTime date;

  ///The price on the date
  @HiveField(1, defaultValue: 0)
  double price;

  DatePrice(this.date, {this.price = 0});
}