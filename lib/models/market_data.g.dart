// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'market_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MarketDataAdapter extends TypeAdapter<MarketData> {
  @override
  final int typeId = 2;

  @override
  MarketData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MarketData(
      fields[1] == null ? '' : fields[1] as String,
      fields[2] as String,
      (fields[0] as List).cast<DatePrice>(),
      fields[3] as String,
      fields[5] == null ? '' : fields[5] as String,
      lastUpdated: fields[4] as DateTime?,
    );
  }

  @override
  void write(BinaryWriter writer, MarketData obj) {
    writer
      ..writeByte(6)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.coinId)
      ..writeByte(0)
      ..write(obj.prices)
      ..writeByte(3)
      ..write(obj.timePeriodDays)
      ..writeByte(5)
      ..write(obj.currency)
      ..writeByte(4)
      ..write(obj.lastUpdated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MarketDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
