// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coin_detail.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CoinDetailAdapter extends TypeAdapter<CoinDetail> {
  @override
  final int typeId = 1;

  @override
  CoinDetail read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CoinDetail(
      fields[0] as String,
      symbol: fields[1] == null ? '' : fields[1] as String,
      name: fields[2] == null ? '' : fields[2] as String,
      imageURL: fields[4] == null ? '' : fields[4] as String,
      price: fields[7] == null ? 0 : fields[7] as double,
      marketCap: fields[14] == null ? 0 : fields[14] as double,
      totalVolume: fields[16] == null ? 0 : fields[16] as double,
      priceChangePercentage24h: fields[21] == null ? 0 : fields[21] as double,
      lastUpdated: fields[27] as DateTime?,
      homepage: fields[3] == null ? '' : fields[3] as String,
      genesisDate: fields[6] as DateTime?,
      atHigh: fields[8] == null ? 0 : fields[8] as double,
      atHighChangePercentage: fields[9] == null ? 0 : fields[9] as double,
      atHighDate: fields[10] as DateTime?,
      atLow: fields[11] == null ? 0 : fields[11] as double,
      atLowChangePercentage: fields[12] == null ? 0 : fields[12] as double,
      atLowDate: fields[13] as DateTime?,
      high24h: fields[17] == null ? 0 : fields[17] as double,
      low24h: fields[18] == null ? 0 : fields[18] as double,
      priceChange24hCurrency: fields[19] == null ? 0 : fields[19] as double,
      priceChangePercentage60m: fields[20] == null ? 0 : fields[20] as double,
      priceChangePercentage7d: fields[22] == null ? 0 : fields[22] as double,
      priceChangePercentage14d: fields[23] == null ? 0 : fields[23] as double,
      priceChangePercentage30d: fields[24] == null ? 0 : fields[24] as double,
      priceChangePercentage1y: fields[25] == null ? 0 : fields[25] as double,
      circulatingSupply: fields[26] == null ? 0 : fields[26] as double,
      oldMarketChart: fields[28] == null
          ? {}
          : (fields[28] as Map).cast<String, MarketData>(),
      marketChart: (fields[29] as HiveList?)?.castHiveList(),
    )..marketCapRank = fields[30] == null ? 0 : fields[30] as int;
  }

  @override
  void write(BinaryWriter writer, CoinDetail obj) {
    writer
      ..writeByte(29)
      ..writeByte(3)
      ..write(obj.homepage)
      ..writeByte(6)
      ..write(obj.genesisDate)
      ..writeByte(8)
      ..write(obj.atHigh)
      ..writeByte(9)
      ..write(obj.atHighChangePercentage)
      ..writeByte(10)
      ..write(obj.atHighDate)
      ..writeByte(11)
      ..write(obj.atLow)
      ..writeByte(12)
      ..write(obj.atLowChangePercentage)
      ..writeByte(13)
      ..write(obj.atLowDate)
      ..writeByte(17)
      ..write(obj.high24h)
      ..writeByte(18)
      ..write(obj.low24h)
      ..writeByte(19)
      ..write(obj.priceChange24hCurrency)
      ..writeByte(20)
      ..write(obj.priceChangePercentage60m)
      ..writeByte(22)
      ..write(obj.priceChangePercentage7d)
      ..writeByte(23)
      ..write(obj.priceChangePercentage14d)
      ..writeByte(24)
      ..write(obj.priceChangePercentage30d)
      ..writeByte(25)
      ..write(obj.priceChangePercentage1y)
      ..writeByte(26)
      ..write(obj.circulatingSupply)
      ..writeByte(28)
      ..write(obj.oldMarketChart)
      ..writeByte(29)
      ..write(obj.marketChart)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.symbol)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.imageURL)
      ..writeByte(7)
      ..write(obj.price)
      ..writeByte(14)
      ..write(obj.marketCap)
      ..writeByte(30)
      ..write(obj.marketCapRank)
      ..writeByte(16)
      ..write(obj.totalVolume)
      ..writeByte(21)
      ..write(obj.priceChangePercentage24h)
      ..writeByte(27)
      ..write(obj.lastUpdated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CoinDetailAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
