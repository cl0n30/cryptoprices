///Arguments for the named route when navigating to the CoinDetailScreen
class CoinDetailScreenArguments {
  ///The ID of the coin
  final String coinId;
  ///The name of the coin
  final String coinName;

  CoinDetailScreenArguments(this.coinId, this.coinName);
}

///Arguments for the named route when navigating to the PriceAlertEditWidget
class PriceAlertEditWidgetArguments {
  ///The ID of the alert
  final int alertId;

  PriceAlertEditWidgetArguments(this.alertId);
}

///Arguments for the InitialPage
class InitialPageArguments {
  ///The ID of the coin
  String coinId;
  ///The name of the coin
  String coinName;
  ///The ID of the alert
  int alertId;

  InitialPageArguments({
    this.coinId = "",
    this.coinName = "",
    this.alertId = 0
  });
}