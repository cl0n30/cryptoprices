
import 'package:hive/hive.dart';

part 'coin.g.dart';

///Coin class with basic information
@HiveType(typeId: 0)
class Coin extends HiveObject {
  ///The ID of the coin
  @HiveField(0)
  String id;

  ///The symbol of the coin
  @HiveField(1, defaultValue: "")
  String symbol;

  ///The name of the coin
  @HiveField(2, defaultValue: "")
  String name;

  ///The URL of the coin icon image
  @HiveField(4, defaultValue: "")
  String imageURL;

  ///The current price of the coin
  @HiveField(7, defaultValue: 0)
  double price;

  ///The current market cap of the coin
  @HiveField(14, defaultValue: 0)
  double marketCap;

  ///The current market cap rank of the coin
  @HiveField(30, defaultValue: 0)
  int marketCapRank;

  ///The traded volume in the last 24H
  @HiveField(16, defaultValue: 0)
  double totalVolume;

  ///The price change in the last 24H in percent
  @HiveField(21, defaultValue: 0)
  double priceChangePercentage24h;

  ///When was this coin last updated in the app database
  @HiveField(27)
  DateTime? lastUpdated;

  Coin(
      this.id,
  {   this.symbol = "",
      this.name = "",
      this.imageURL = "",

      this.price = 0,

      this.marketCap = 0,

      this.marketCapRank = 0,

      this.totalVolume = 0,

      this.priceChangePercentage24h = 0,

      this.lastUpdated
  });

  ///Converts json to a Coin object
  Coin.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        symbol = json['symbol'],
        name = json['name'],
        imageURL = json['image'] ?? "",

        price = (json['current_price'] ?? 0).toDouble(),

        marketCap = (json['market_cap'] ?? 0).toDouble(),

        marketCapRank = (json['market_cap_rank'] ?? 0),

        totalVolume = (json['total_volume'] ?? 0).toDouble(),

        priceChangePercentage24h = (json['price_change_percentage_24h'] ?? 0).toDouble();

  ///Creates a Coin object from detailed json data
  ///only for use in CoinDetail.fromJson
  Coin.fromDetailedJson(Map<String, dynamic> json, String setCurrency)
      : id = json['id'],
        symbol = json['symbol'],
        name = json['name'],
        imageURL = (json['image']['large'] ?? ""),

        price = (json['market_data']['current_price'][setCurrency]  ?? 0).toDouble(), //json can be int e.g. with USD as setCurrency

        marketCap = (json['market_data']['market_cap'][setCurrency]  ?? 0).toDouble(),

        marketCapRank = (json['market_cap_rank'] ?? 0),

        totalVolume = (json['market_data']['total_volume'][setCurrency] ?? 0).toDouble(),

        priceChangePercentage24h = (json['market_data']['price_change_percentage_24h_in_currency'][setCurrency] ?? 0).toDouble();
}
