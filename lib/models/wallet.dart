import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:hive/hive.dart';

part 'wallet.g.dart';

///The Wallet class that contains all transactions for a specific coin and its current price.
@HiveType(typeId: 6)
class Wallet extends HiveObject {
  ///The ID of the wallet
  @HiveField(11, defaultValue: 0)
  int id;

  ///The ID of the coin this portfolio belongs to
  @HiveField(1)
  String coinId;

  ///The ID of the portfolio to which this wallet belongs to
  @HiveField(10, defaultValue: 0)
  int connectedPortfolioId;

  ///The name of the coin this wallet belongs to
  @HiveField(2, defaultValue: "")
  String coinName;

  ///The symbol of the coin this wallet belongs to
  @HiveField(3, defaultValue: "")
  String coinSymbol;

  ///The current price of the coin this wallet belongs to
  ///in the currency of the connected portfolio
  @HiveField(4, defaultValue: 0)
  double currentCoinPrice;

  ///When was this wallet last updated in the database
  @HiveField(7)
  DateTime? lastUpdated;

  ///The transactions that belong to this wallet
  @HiveField(8)
  HiveList<Transaction>? transactions;

  ///The position of this wallet in the wallet list
  @HiveField(9, defaultValue: 0)
  int listPosition;

  Wallet(
      this.id,
      this.coinId,
      this.connectedPortfolioId,

  {   this.coinName = "",
      this.coinSymbol = "",
      this.currentCoinPrice = 0,

      this.lastUpdated,

      this.transactions,

      this.listPosition = 0
  });

  ///The total amount of coins in this portfolio
  double get totalCoinAmount {
    if (allTransactions.length == 0)
      return 0;

    double coinAmount = 0;
    allTransactions.forEach((element) {
      coinAmount += element.coinVolume;
    });
    return coinAmount;
  }

  ///The current value of the coins in this portfolio
  double get totalValue {
    return double.parse((totalCoinAmount * currentCoinPrice).toStringAsFixed(9));
  }

  ///The earliest transaction date
  DateTime? get firstTransactionDate {
    if (allTransactions.length == 0)
      return null;

    DateTime firstDate = DateTime.now();
    allTransactions.forEach((element) {
      if (element.transactionDate!.isBefore(firstDate))
        firstDate = element.transactionDate!;
    });
    return firstDate;
  }

  ///Returns the list of transactions or an empty list if null
  HiveList<Transaction> get allTransactions {
    if (transactions == null) {
      transactions = HiveList<Transaction> (Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME));
      return transactions!;
    } else {
      return transactions!;
    }
  }

  ///Sets the transactions to a new HiveList with the given newTransaction as objects
  set allTransactions(List<Transaction> newTransactions) {
    transactions = HiveList<Transaction> (Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME), objects: newTransactions);
  }

  ///Returns the coin amount in the wallet at the given date
  double totalAmountAtDate(DateTime date) {
    if (allTransactions.length == 0)
      return 0;

    double coinAmountOnDate = 0;
    final transactionsOnDate = allTransactions.where((element) => element.transactionDate!.isBefore(date));
    transactionsOnDate.forEach((element) {
      coinAmountOnDate += element.coinVolume;
    });
    return coinAmountOnDate;
  }

  ///Returns the value of the wallet at the given date with the given coin price
  double totalValueAtDate(DateTime date, double priceAtDate) {
    if (allTransactions.length == 0)
      return 0;

    return double.parse((totalAmountAtDate(date) * priceAtDate).toStringAsFixed(9));
  }

  ///Converts a Wallet object to json
  Map<String, dynamic> toJson() => {
    "id": id,
    "coinId": coinId,
    "connectedPortfolioId": connectedPortfolioId,
    "coinName": coinName,
    "coinSymbol": coinSymbol,
    "currentCoinPrice": currentCoinPrice,
    "lastUpdated": lastUpdated?.toIso8601String(),
    "transactions": (transactions?.map((e) => e.id).toList()) ?? [],
    "listPosition": listPosition
  };

  ///Converts json to a Wallet object.
  ///Wallet transactions have to be added separately
  Wallet.fromJson(Map<String, dynamic> json)
      : id = json["id"] ?? 0,
        coinId = json["coinId"],
        connectedPortfolioId = json["connectedPortfolioId"] ?? 0,
        coinName = json["coinName"]  ?? "",
        coinSymbol = json["coinSymbol"] ?? "",
        currentCoinPrice = json["currentCoinPrice"] ?? 0,
        lastUpdated = DateTime.parse(json["lastUpdated"] ?? DateTime.now().toIso8601String()),
        listPosition = json["listPosition"] ?? 0;
}