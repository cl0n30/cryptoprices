import 'package:hive_flutter/hive_flutter.dart';

part 'explorer_transaction_url.g.dart';

///The base url of a transaction in the explorer of a coin
@HiveType(typeId: 8)
class ExplorerTransactionUrl extends HiveObject {
  ///The id of the coin that the URL is for
  @HiveField(0)
  String coinId;

  ///The URL of a transaction in the block explorer of the coin
  @HiveField(1, defaultValue: "")
  String url;

  ExplorerTransactionUrl(
    this.coinId,
  {
    this.url = ""
  });

  ///Converts json to a ExplorerTransactionUrl object
  ExplorerTransactionUrl.fromJson(Map<String, dynamic> json)
      : coinId = json['coinId'],
        url = json['url'];
}