import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:flutter/material.dart';

///Change Notifier that changes the color of the app and notifies listeners
class ColorChange extends ChangeNotifier {
  Color _primaryColor = Constants.defaultPrimaryColor!;
  SettingsDAO _settingsDAO = SettingsDAO();

  final _lightColors = [
    Colors.lightGreen.value,
    Colors.lime.value,
    Colors.yellow.value,
    Colors.amber.value,
    Colors.orange.value,
    Colors.grey.value,
  ];

  ColorChange() {
    _primaryColor = _settingsDAO.getPrimaryColorHive();
  }

  ///Returns the current primary color
  Color currentPrimaryColor() {
    return _primaryColor;
  }

  ///Returns the current text color based on the brightness of the primary color
  Color currentTextColor() {
    if (_lightColors.contains(_primaryColor.value)) {
      return Colors.black;
    } else {
      return Colors.white;
    }
  }

  ///Changes the primary color and notifies listeners
  void changePrimaryColor(Color newColor) {
    _primaryColor = newColor;
    _settingsDAO.setPrimaryColor(newColor);
    notifyListeners();
  }
}