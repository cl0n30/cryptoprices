import 'package:crypto_prices/database/settings_dao.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

///Change Notifier that changes the theme of the app and notifies listeners
class ThemeChange extends ChangeNotifier {
  Themes _currentTheme = Themes.DARK;
  SettingsDAO _settingsDAO = SettingsDAO();

  ThemeChange() {
    _currentTheme = _settingsDAO.getCurrentThemeHive();
  }

  ///Returns the current theme
  Themes currentTheme() {
    return _currentTheme;
  }

  ///Checks if the current theme is the dark theme
  bool isThemeDark() {
    switch (_currentTheme) {
      case Themes.SYSTEM: {
        if (SchedulerBinding.instance!.window.platformBrightness == Brightness.dark) {
          return true;
        } else {
          return false;
        }
      }
      case Themes.SYSTEM_AMOLED: {
        if (SchedulerBinding.instance!.window.platformBrightness == Brightness.dark) {
          return true;
        } else {
          return false;
        }
      }
      case Themes.LIGHT: {
        return false;
      }
      case Themes.DARK: {
        return true;
      }
      case Themes.AMOLED_DARK: {
        return true;
      }
    }
  }

  ///Changes the theme mode of the app (light, dark, system, AMOLED)
  void changeTheme(Themes theme) {
    _currentTheme = theme;
    _settingsDAO.setCurrentTheme(theme);
    notifyListeners();
  }
}

///All available themes
enum Themes {
  SYSTEM,
  SYSTEM_AMOLED,
  LIGHT,
  DARK,
  AMOLED_DARK
}