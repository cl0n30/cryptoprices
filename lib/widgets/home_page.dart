import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/price_alert_list_entry_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/detailScreen/detail_screen_base.dart';
import 'package:crypto_prices/widgets/portfolioScreen/edit_portfolio_widget.dart';
import 'package:crypto_prices/widgets/portfolioScreen/portfolio_selection_dropdown.dart';
import 'package:crypto_prices/widgets/portfolioScreen/portfolio_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/authentication_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/search_available_coins_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import '../api/api_interaction.dart';
import '../api/coingecko_api.dart';
import '../constants.dart';
import '../generated/l10n.dart';
import '../models/available_coin.dart';
import 'coinListScreen/coin_list.dart';
import 'favoritesScreen/favorite_list.dart';
import 'portfolioScreen/transaction_edit_widget.dart';
import 'priceAlertsScreen/price_alert_edit_widget.dart';
import 'priceAlertsScreen/price_alert_list.dart';
import 'settingsScreens/settings_widget.dart';

class HomePage extends StatefulWidget {
  static const routeName = "/";

  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _page = 0;

  late PageController _pageController;

  ///Order in the coin list
  OrderBy _coinsOrder = OrderBy.market_cap;

  ///Direction in the coin list
  OrderDirection _coinsOrderDirection = OrderDirection.desc;

  ///Notifier if an item has been selected in the Sort-Popup menu
  ValueNotifier<OrderBy> _coinsSortSelectedItem = ValueNotifier<OrderBy>(OrderBy.market_cap);

  ///Order in the portfolio list
  WalletOrderBy _walletOrder = WalletOrderBy.totalValueDesc;

  ///Notifier if an item has been selected in the Sort-Popup menu
  ValueNotifier<WalletOrderBy> _walletSortSelectedItem = ValueNotifier<WalletOrderBy>(WalletOrderBy.totalValueDesc);

  ///Are all entries in the alert list collapsed?
  bool _allPriceAlertListEntriesCollapsed = false;

  ///Id of the currently selected portfolio
  int _selectedPortfolioId = 0;

  bool _privateModeEnabled = false;

  bool _isAuthenticated = false;

  ApiInteraction api = CoinGeckoAPI();

  PriceAlertListEntryDao _priceAlertListEntryDao = PriceAlertListEntryDao();

  SettingsDAO _settingsDAO = SettingsDAO();

  PortfolioDao _portfolioDao = PortfolioDao();

  @override
  void initState() {
    super.initState();

    _page = _settingsDAO.getStartScreen().index;
    _pageController = PageController(initialPage: _page);

    _allPriceAlertListEntriesCollapsed = _priceAlertListEntryDao.areAllCollapsed();

    _selectedPortfolioId = _settingsDAO.getDefaultPortfolioIdHive();

    _privateModeEnabled = _settingsDAO.getPrivateMode();

    _isAuthenticated = _settingsDAO.getIsAuthenticated();

    //watch if one entry is collapsed manually
    _priceAlertListEntryDao.getAllNonCollapsedEntriesWatcher().listen((newResult) {
      if (newResult.length == 0) {
        setState(() {
          _allPriceAlertListEntriesCollapsed = true;
        });
      } else {
        setState(() {
          _allPriceAlertListEntriesCollapsed = false;
        });
      }
    });

    _coinsSortSelectedItem.addListener(() {
      setState(() {
        _coinsOrder = _coinsSortSelectedItem.value;
      });
    });

    _walletSortSelectedItem.addListener(() {
      setState(() {
        _walletOrder = _walletSortSelectedItem.value;
      });
    });

    _portfolioDao.getAllPortfoliosWatcher().addListener(() {
      if (this.mounted) {
        //select new portfolio if none existed before
        if (_selectedPortfolioId == Constants.NODEFAULTPORTFOLIOID && _settingsDAO.getDefaultPortfolioIdHive() != Constants.NODEFAULTPORTFOLIOID) {
          setState(() {
            _selectedPortfolioId = _settingsDAO.getDefaultPortfolioIdHive();
          });
        }
      }
    });

    _settingsDAO.getPrivateModeWatcher().listen((event) {
      if (this.mounted) {
        setState(() {
          _privateModeEnabled = event.value;
        });
      }
    });

    _settingsDAO.getIsAuthenticatedWatcher().listen((event) {
      if (this.mounted) {
        setState(() {
          _isAuthenticated = event.value;
        });
      }
    });
  }

  ///Returns the appbar with symbols and title for each page
  AppBar _buildAppBar(int page) {
    List<String> appBarTitles = [
      S.of(context).coinsListTabName,
      S.of(context).favoritesTabName,
      S.of(context).portfolioTabName,
      S.of(context).priceAlertsTabName,
      S.of(context).settingsTabName,
    ];
    switch (page) {
      case Constants.COINSLISTPAGEINDEX :
        { //CoinList
          return AppBar(
            title: Text(appBarTitles[page]),
            actions: [
              IconButton(
                icon: (_coinsOrderDirection == OrderDirection.desc
                    ? Icon(Icons.arrow_downward)
                    : Icon(Icons.arrow_upward)),
                onPressed: () {
                  setState(() {
                    if (_coinsOrderDirection == OrderDirection.desc) {
                      _coinsOrderDirection = OrderDirection.asc;
                    } else {
                      _coinsOrderDirection = OrderDirection.desc;
                    }
                  });
                },
                tooltip: S.of(context).sortingDirectionTooltip,
              ),
              _buildCoinsSortPopupMenuButton(_coinsSortSelectedItem),
              _buildSearchIconButton()
            ],
          );
        }
      case Constants.PORTFOLIOPAGEINDEX :{
        if (_selectedPortfolioId != Constants.NODEFAULTPORTFOLIOID) {
          List<Widget> actions = [];
          actions.add(
            IconButton(
              icon: (_privateModeEnabled)
                  ? Icon(Icons.visibility_off)
                  : Icon(Icons.visibility),
              onPressed: () async {
                if (_isAuthenticated) {
                  setState(() {
                    _privateModeEnabled = !_privateModeEnabled;
                    _settingsDAO.setPrivateMode(_privateModeEnabled);
                  });
                } else {
                  _showSnackbar(S.of(context).authenticationRequired);
                }
              },
              tooltip: S.of(context).privateMode,
            )
          );
          if (_isAuthenticated) {
            actions.add(
              _buildWalletSortPopupMenuButton(_walletSortSelectedItem)
            );
            actions.add(
              IconButton(
                icon: Icon(Icons.build),
                tooltip: S.of(context).portfolioEditEditPortfolio,
                onPressed: () async {
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EditPortfolioWidget(selectedPortfolioId: _selectedPortfolioId)
                    )
                  );
                  if (result == Constants.DELETERESULT) { //deleted portfolio before closing
                    setState(() {
                      _selectedPortfolioId = _settingsDAO.getDefaultPortfolioIdHive();
                    });
                  } else {
                    setState(() {
                    });
                  }
                },
              )
            );
          }
          return AppBar(
            title: PortfolioSelectionDropdown(
              onItemTapped: _handlePortfolioSelection,
              selectedPortfolioId: _selectedPortfolioId
            ),
            actions: actions,
          );
        } else {
          return AppBar(
            title: Text(S.of(context).portfolioTabName),
          );
        }
      }
      case Constants.FAVORITELISTPAGEINDEX :
        {
          return AppBar(
            title: Text(appBarTitles[page]),
            actions: [
              _buildSearchIconButton()
            ],
          );
        }
      case Constants.PRICEALERTLISTPAGEINDEX : {
        return AppBar(
          title: Text(appBarTitles[page]),
          actions: [
            if(_allPriceAlertListEntriesCollapsed)
              IconButton(
                icon: Icon(Icons.unfold_more),
                tooltip: S.of(context).expandAll,
                onPressed: _onCollapseAllPressed
              )
            else
              IconButton(
                icon: Icon(Icons.unfold_less),
                tooltip: S.of(context).collapseAll,
                onPressed: _onCollapseAllPressed
              ),
            _buildSearchIconButton()
          ],
        );
      }
      default :
        return AppBar(
          title: Text(appBarTitles[page])
        );
    }
  }

  ///Return a popup menu to select coins sorting order with radio buttons
  PopupMenuButton<OrderBy> _buildCoinsSortPopupMenuButton(ValueNotifier _selectedItem) {
    List<String> orderByNames = [
      S.of(context).sortMenuOptionMarketCap,
      S.of(context).sortMenuOptionVolume,
      S.of(context).sortMenuOptionPriceChange,
      S.of(context).sortMenuOptionName,
      S.of(context).sortMenuOptionSymbol
    ];
    return PopupMenuButton<OrderBy>(
        icon: Icon(Icons.sort),
        tooltip: S.of(context).sortByToolTip,
        itemBuilder: (BuildContext context) {
          return List<PopupMenuEntry<OrderBy>>.generate(
              OrderBy.values.length, (index) {
                return PopupMenuItem(
                  value: OrderBy.values[index],
                  child: AnimatedBuilder(
                    child: Text(orderByNames[index]),
                    animation: _selectedItem,
                    builder: (BuildContext context, Widget? child) {
                      return RadioListTile<OrderBy>(
                          activeColor: (currentTheme.isThemeDark()
                              ? Constants.accentColorDark
                              : Constants.accentColorLight),
                          value: OrderBy.values[index],
                          groupValue: _selectedItem.value,
                          title: child,
                          onChanged: (OrderBy? value) {
                            _selectedItem.value = value!;
                            Navigator.pop(context);
                          }
                      );
                    },
                  ),
                );
              }
          );
        }
    );
  }

  ///Changes the selected portfolio when one is selected from the menu
  void _handlePortfolioSelection(int newPortfolioId) {
    setState(() {
      _selectedPortfolioId = newPortfolioId;
    });
  }

  ///Return a popup menu to select portfolios sorting order with radio buttons
  PopupMenuButton<WalletOrderBy> _buildWalletSortPopupMenuButton(ValueNotifier _selectedItem) {
    List<String> orderByNames = [
      S.of(context).sortMenuOptionTotalValueDesc,
      S.of(context).sortMenuOptionTotalValueAsc,
      S.of(context).sortMenuOptionNameAsc,
      S.of(context).sortMenuOptionNameDesc
    ];
    return PopupMenuButton<WalletOrderBy>(
      icon: Icon(Icons.sort),
      tooltip: S.of(context).sortByToolTip,
      itemBuilder: (BuildContext context) {
        return List<PopupMenuEntry<WalletOrderBy>>.generate(
            WalletOrderBy.values.length, (index) {
          return PopupMenuItem(
            value: WalletOrderBy.values[index],
            child: AnimatedBuilder(
              child: Text(orderByNames[index]),
              animation: _selectedItem,
              builder: (BuildContext context, Widget? child) {
                return RadioListTile<WalletOrderBy>(
                    activeColor: (currentTheme.isThemeDark()
                        ? Constants.accentColorDark
                        : Constants.accentColorLight),
                    value: WalletOrderBy.values[index],
                    groupValue: _selectedItem.value,
                    title: child,
                    onChanged: (WalletOrderBy? value) {
                      _selectedItem.value = value!;
                      Navigator.pop(context);
                    }
                );
              },
            ),
          );
        }
        );
      }
    );
  }

  ///Returns an IconButton that opens the search view
  Widget _buildSearchIconButton() {
    return IconButton(
      icon: Icon(Icons.search),
      tooltip: S.of(context).searchToolTip,
      onPressed: () async {
        AvailableCoin? result = await showSearch<AvailableCoin?>(
          context: context,
          delegate: SearchAvailableCoinsDelegate(context)
        );
        if (result != null) { //coin has been selected from search
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                DetailScreenBase(coinId: result.id)
            )
          );
        }
      }
    );
  }

  ///Change page
  void _onItemTapped(int index) {
    setState(() {
      _pageController.jumpToPage(index);
    });
  }

  ///Set new page as current page
  void _onPageChange(int newPage) {
    setState(() {
      _page = newPage;
    });
  }

  ///Collapse or expand all alert list entries
  void _onCollapseAllPressed() {
    setState(() {
      _settingsDAO.setCollapseAll(!_allPriceAlertListEntriesCollapsed);
    });
  }

  @override
  Widget build(BuildContext context) {
    //save current locale if none selected
    if (_settingsDAO.getLocaleHive().isEmpty) {
      _settingsDAO.setLocale(
        Localizations.localeOf(context).languageCode
      );
    }

    return Scaffold(
      appBar: _buildAppBar(_page),
      body: PageView(
        controller: _pageController,
        scrollDirection: Axis.horizontal,
        onPageChanged: _onPageChange,
        children: [
          Center(
              child: CoinList(order: _coinsOrder, orderDirection: _coinsOrderDirection)
          ),
          Center(
              child: FavoriteList()
          ),
          Center(
              child: (_isAuthenticated)
                  ? PortfolioWidget(order: _walletOrder, selectedPortfolioId: _selectedPortfolioId)
                  : _buildPortfolioLockScreen(),
          ),
          Center(
              child: PriceAlertList()
          ),
          Center(
              child: SettingsWidget()
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Constants.TABICONS[0],
              label: S.of(context).coinsListTabName
          ),
          BottomNavigationBarItem(
              icon: Constants.TABICONS[1],
              label: S.of(context).favoritesTabName
          ),
          BottomNavigationBarItem(
              icon: Constants.TABICONS[2],
              label: S.of(context).portfolioTabName
          ),
          BottomNavigationBarItem(
              icon: Constants.TABICONS[3],
              label: S.of(context).priceAlertsTabName
          ),
          BottomNavigationBarItem(
              icon: Constants.TABICONS[4],
              label: S.of(context).settingsTabName
          ),
        ],
        currentIndex: _page,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        onTap: _onItemTapped,
      ),
      floatingActionButton: _buildFloatingActionButton(_page),
    );
  }

  ///Builds the floating action buttons for the widgets that use them
  Widget _buildFloatingActionButton(int page) {
    switch (page) {
      case Constants.PRICEALERTLISTPAGEINDEX : {
        return FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PriceAlertEditWidget(alertId: 0)
              )
            );
          }
        );
      }

      case Constants.PORTFOLIOPAGEINDEX : {
        if (!_isAuthenticated) {
          return Row();
        }
        if (_selectedPortfolioId == Constants.NODEFAULTPORTFOLIOID) {
          return FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => EditPortfolioWidget(selectedPortfolioId: 0)
                )
              );
              if (result != null) {
                _handlePortfolioSelection(result);
              }
            },
          );
        } else {
          return SpeedDial(
            icon: Icons.add,
            activeIcon: Icons.close,
            spacing: 10,
            spaceBetweenChildren: 10,
            children: [
              SpeedDialChild(
                child: Icon(Icons.pie_chart),
                label: S.of(context).newPortfolio,
                onTap: () async {
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EditPortfolioWidget(selectedPortfolioId: 0)
                    )
                  );
                  if (result != null) {
                    _handlePortfolioSelection(result);
                  }
                },
              ),
              SpeedDialChild(
                child: Icon(Icons.data_saver_on),
                label: S.of(context).newTransaction,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TransactionEditWidget(transactionId: 0, selectedPortfolioId: _selectedPortfolioId)
                    )
                  );
                }
              )
            ],
          );
        }
      }
      default :  {
        return Row();
      }
    }
  }

  ///Builds a lock screen with a button to unlock and a message
  Widget _buildPortfolioLockScreen() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          S.of(context).portfolioLocked,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18),
        ),
        SizedBox(height: 20),
        ElevatedButton(
          child: Text(
            S.current.authenticationUnlockButton,
            style: TextStyle(fontSize: 18),
          ),
          onPressed: () async {
            final result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  AuthenticationWidget()));
            if (result == Constants.successResult) {
              setState(() {
                _isAuthenticated = _settingsDAO.getIsAuthenticated();
              });
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(S.of(context).authenticationRequired),
                  duration: Duration(seconds: 2),
                ),
              );
            }
          }
        ),
      ],
    );
  }

  ///Shows a snackbar with the message
  void _showSnackbar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}