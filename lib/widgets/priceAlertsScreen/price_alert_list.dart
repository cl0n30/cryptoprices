import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/price_alert_list_entry_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:crypto_prices/widgets/priceAlertsScreen/price_alert_list_entry_widget.dart';
import 'package:flutter/material.dart';

class PriceAlertList extends StatefulWidget {

  @override
  _PriceAlertListState createState() => _PriceAlertListState();
}

class _PriceAlertListState extends State<PriceAlertList> {

  PriceAlertListEntryDao _alertListEntryDao = PriceAlertListEntryDao();

  List<PriceAlertListEntry> _coinsWithAlerts = [];

  @override
  void initState() {
    super.initState();
    _coinsWithAlerts = _alertListEntryDao.getAllEntries();
    _alertListEntryDao.getAllEntriesChangeWatcher().listen((newResult) { //listen if entry was deleted
      //setState only when alert was deleted or added and not when any database entry changes
      if(this.mounted && newResult.length != _coinsWithAlerts.length) {
        setState(() {
          _coinsWithAlerts = newResult..sort(CoinComparators.priceAlertListPosition);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_coinsWithAlerts.isEmpty)
      return Center(
        child: Text(S.of(context).noPriceAlerts),
      );
    else
      return ReorderableListView.builder(
        padding: EdgeInsets.all(8.0),
        itemCount: _coinsWithAlerts.length,
        itemBuilder: (context, i) {
          //return _buildRow(coin);
          return PriceAlertListEntryWidget(
              key: Key(_coinsWithAlerts[i].id.toString()),
              coinId: _coinsWithAlerts[i].coinId
          );
        },
        onReorder: (int oldIndex, int newIndex) {
          setState(() {
            if (oldIndex < newIndex) {
              newIndex -= 1;
              _alertListEntryDao.moveUpAllBetween(oldIndex, newIndex);
            } else {
              _alertListEntryDao.moveDownAllBetween(newIndex, oldIndex);
            }

            PriceAlertListEntry item = _coinsWithAlerts.removeAt(oldIndex);
            _coinsWithAlerts.insert(newIndex, item);

            var entry = _alertListEntryDao.getEntry(item.coinId)!;
            entry.listPosition = newIndex;
            _alertListEntryDao.insertEntry(entry);
          });
        },
      );
  }
}