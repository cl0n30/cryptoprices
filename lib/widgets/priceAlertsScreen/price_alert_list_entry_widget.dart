import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/price_alert_dao.dart';
import 'package:crypto_prices/database/price_alert_list_entry_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/util/workmanager_handler.dart';
import 'package:crypto_prices/widgets/detailScreen/detail_screen_base.dart';
import 'package:flutter/material.dart';

import 'price_alert_edit_widget.dart';

///Widget that shows all price alerts for a specific coin sorted by dateAdded
class PriceAlertListEntryWidget extends StatefulWidget {
  final String coinId;

  ///Only displays alert count and not the associated coin
  final bool dontDisplayCoin;

  PriceAlertListEntryWidget({
    Key? key,
    required this.coinId,
    this.dontDisplayCoin = false
  }) : super(key: key);

  @override
  _PriceAlertListEntryWidgetState createState() => _PriceAlertListEntryWidgetState();
}

class _PriceAlertListEntryWidgetState extends State<PriceAlertListEntryWidget> {
  String _coinName = "";
  String _coinSymbol = "";
  late PriceAlertListEntry _entry;

  final _boldFont = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
  final _smallerFont = TextStyle(fontSize: 14.0);
  late Color _iconRed;
  late Color _iconGreen;

  List<PriceAlert> _priceAlerts = [];
  PriceAlertListEntryDao _priceAlertListEntryDao = PriceAlertListEntryDao();
  PriceAlertDao _priceAlertDao = PriceAlertDao();
  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();
    _entry = _priceAlertListEntryDao.getEntry(widget.coinId)!;

    _priceAlerts = _priceAlertDao.getAllCoinPriceAlerts(widget.coinId);
    if (_priceAlerts.isNotEmpty) {
      _coinName = _entry.coinName;
      _coinSymbol = _entry.coinSymbol;
    }

    if (currentTheme.isThemeDark()) {
      _iconRed = Constants.priceChangeRedDark;
      _iconGreen = Constants.priceChangeGreenDark!;
    } else {
      _iconRed = Constants.priceChangeRedLight!;
      _iconGreen = Constants.priceChangeGreenLight!;
    }

    _priceAlertDao.getAllPriceAlertsChangeWatcher().listen((newResult) {
      if (this.mounted) {
        setState(() {
          //update list to always show newly added alerts
          _priceAlerts = _priceAlertDao.getAllCoinPriceAlerts(widget.coinId);
          if (_priceAlerts.isNotEmpty) {
            _coinName = _entry.coinName;
            _coinSymbol = _entry.coinSymbol;
          }
        });
      }
    });

    //listen if collapseAll was pressed and collapse or expand the widget accordingly
    _settingsDAO.getCollapseAllWatcher().listen((event) {
      if (this.mounted) {
        setState(() {
          _entry.isCollapsed = event.value;
        });
        _priceAlertListEntryDao.insertEntry(_entry);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        child: Column(
          children: [
            _buildTitleRow(),
            AnimatedCrossFade(
                firstChild: Row(),
                secondChild: _buildAlertList(),
                crossFadeState: _entry.isCollapsed
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: Duration(milliseconds: 200)
            )
          ],
        ),
        onTap: () {
          _toggleCollapse();
        },
      )
    );
  }

  ///Builds the row that shows the coin name, symbol, icon and alert count or only the alert count
  Widget _buildTitleRow() {
    if (widget.dontDisplayCoin) {
      return Row(
        children: [
          Container(
            margin: EdgeInsets.all(8.0),
            child: Icon(Icons.notifications)
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "${_priceAlerts.length} ${S.of(context).alertCount(_priceAlerts.length)}",
                  style: _boldFont
                ),
                if (!_entry.isCollapsed)
                  IconButton(
                      icon: Icon(Icons.delete_outline),
                      tooltip: S.of(context).alertEntryDeleteAllAlertsTooltip,
                      onPressed: () {
                        _onDeleteButtonPressed();
                      }
                  ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Icon((_entry.isCollapsed)
              ? Icons.arrow_drop_down
              : Icons.arrow_drop_up
            ),
          )
        ],
      );
    } else {
      return Row(
        children: [
          Container(
            margin: EdgeInsets.all(8.0),
            child: CachedNetworkImage(
              imageUrl: ImageUrls().getLargeImageUrl(widget.coinId),
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error_outline),
              height: 40.0,
              width: 40.0,
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    margin: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _coinName,
                          style: _boldFont
                        ),
                        SizedBox(height: 10),
                        Text(
                          _coinSymbol.toUpperCase(),
                          style: _smallerFont
                        )
                      ],
                    ),
                  ),
                ),
                _buildButtonRow()
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Icon((_entry.isCollapsed)
              ? Icons.arrow_drop_down
              : Icons.arrow_drop_up
            ),
          )
        ],
      );
    }
  }

  ///Returns a button row with more options when the entry is not collapsed
  ///and a text with the number of alerts otherwise
  Widget _buildButtonRow() {
    if (_entry.isCollapsed) {
      return Row(
        children: [
          Text(
            "${_priceAlerts.length} ${S.of(context).alertCount(_priceAlerts.length)}",
            style: _boldFont
          )
        ],
      );
    } else {
      return Row(
        children: [
          IconButton(
            icon: Icon(Icons.show_chart),
            tooltip: S.of(context).alertEntryOpenDetailTooltip,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailScreenBase(coinId: widget.coinId)
                )
              );
            }
          ),
          IconButton(
            icon: Icon(Icons.delete_outline),
            tooltip: S.of(context).alertEntryDeleteAllAlertsTooltip,
            onPressed: () {
              _onDeleteButtonPressed();
            }
          ),
        ],
      );
    }
  }

  ///Builds the list of all alerts for this coin
  Widget _buildAlertList() {
    return Card(
      color: (currentTheme.isThemeDark())
        ? Colors.black12
        : Colors.white,
        child: MediaQuery( //TODO hotfix for https://github.com/flutter/flutter/issues/83224 in flutter 2.2
          data: MediaQuery.of(context).removePadding(removeTop: true),
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _priceAlerts.length,
            itemBuilder: (context, i) {
              return _buildListTile(_priceAlerts[i]);
            },
          )
        )
    );
  }

  ///Builds a list tile for an alert. OnTap open the alert edit widget for that alert
  Widget _buildListTile(PriceAlert alert) {
    Icon leading;
    switch (alert.priceMovement) {
      case PriceMovement.above: {
        leading = Icon(Icons.trending_up, color: _iconGreen,);
        break;
      }

      case PriceMovement.equal: {
        leading = Icon(Icons.trending_flat);
        break;
      }

      case PriceMovement.below: {
        leading = Icon(Icons.trending_down, color: _iconRed,);
        break;
      }
    }
    Widget title;
    if (alert.targetValueType == TargetValueType.price) {
      if (alert.alertFrequency == AlertFrequency.once) {
        title = Text("${Util.formatPrice(alert.targetValue)} ${Constants.currencySymbols[alert.currency]}");
      } else {
        title = Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("${Util.formatPrice(alert.targetValue)} ${Constants.currencySymbols[alert.currency]}"),
            Icon(Icons.repeat)
          ],
        );
      }
    } else {
      if (alert.alertFrequency == AlertFrequency.once) {
        title = Text("${Util.formatPercentage(alert.targetValue)}%");
      } else {
        title = Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("${Util.formatPercentage(alert.targetValue)}%"),
            Icon(Icons.repeat)
          ],
        );
      }
    }
    return Column(
      children: [
        ListTile(
          title: title,
          leading: leading,
          trailing: Icon(Icons.chevron_right),
          onTap: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PriceAlertEditWidget(alertId: alert.id)
                )
            );
          },
        ),
        Divider(height: 0, thickness: 1)
      ],
    );
  }

  ///Collapses or extends the alert list entry and saves the state in the database
  void _toggleCollapse() {
    setState(() {
      _entry.isCollapsed = !_entry.isCollapsed;
    });
    _priceAlertListEntryDao.insertEntry(_entry);
  }

  ///Opens an alert dialog to confirm deletion of all alerts of this entry
  void _onDeleteButtonPressed() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).alertDialogDeleteCoinPriceAlertTitle(_coinSymbol.toUpperCase())),
          content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () {
                WorkmanagerHandler().deleteAllCoinAlerts(widget.coinId);
                Navigator.pop(context); //pop dialog
              },
              child: Text(S.of(context).alertDialogDelete)
            ),
          ],
        );
      }
    );
  }
}