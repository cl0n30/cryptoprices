import 'package:crypto_prices/models/screen_arguments.dart';
import 'package:crypto_prices/widgets/detailScreen/coin_detail_screen.dart';
import 'package:crypto_prices/widgets/detailScreen/detail_screen_base.dart';
import 'package:crypto_prices/widgets/home_page.dart';
import 'package:crypto_prices/widgets/priceAlertsScreen/price_alert_edit_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/authentication_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/migration_assistant.dart';
import 'package:flutter/material.dart';

///Initial screen that opens other screens if necessary on app start
class InitialPage extends StatefulWidget {
  final String initialRoute;

  final InitialPageArguments routeArguments;

  const InitialPage({
    Key? key,
    required this.routeArguments,
    this.initialRoute = HomePage.routeName
  }) : super(key: key);

  @override
  _InitialPageState createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {

  @override
  void initState() {
    super.initState();
    final screensToOpen = _splitRoute(widget.initialRoute);

    Future.microtask(() { //futures are executed after build
      screensToOpen.forEach((routeName) {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  _getScreen(routeName)));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomePage(),
    );
  }

  ///Returns the screen with the given route name
  Widget _getScreen(String routeName) {
    switch ("/$routeName") {
      case HomePage.routeName: {
        return HomePage();
      }

      case CoinDetailScreen.routeName: {
        return DetailScreenBase(coinId: widget.routeArguments.coinId);
      }

      case PriceAlertEditWidget.routeName: {
        return PriceAlertEditWidget(alertId: widget.routeArguments.alertId);
      }

      case MigrationAssistant.routeName: {
        return MigrationAssistant();
      }

      case AuthenticationWidget.routeName: {
        return AuthenticationWidget(closeAppOnBackPressed: true);
      }

      default: {
        return Scaffold(body: Center(child: Text("Route not found")));
      }
    }
  }

  ///Splits a full route in its corresponding screens
  List<String> _splitRoute(String route) {
    final split = route.split("/");
    return split.skip(1).toList(); //first element empty because / is first symbol of route
  }
}
