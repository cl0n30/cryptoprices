import 'dart:convert';

import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/crypto_prices_icons.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/donation_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/rate_limitation_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants.dart';

///Displays information about the app
class AboutWidget extends StatefulWidget {

  @override
  _AboutWidgetState createState() => _AboutWidgetState();
}

class _AboutWidgetState extends State<AboutWidget> {
  final TextStyle _bigFont = TextStyle(fontSize: 22, );
  final TextStyle _mediumFont = TextStyle(fontSize: 20, );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).settingsAbout),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  children: [
                    Container(
                        padding: EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Image(
                              image: AssetImage("assets/images/ic_launcher_crypto_orange_3_round.png"),
                              width: 50,
                              height: 50,
                            ),
                            SizedBox(width: 20),
                            Text(
                              S.of(context).appName,
                              style: _bigFont,
                            )
                          ],
                        )
                    ),
                    ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        ListTile(
                          leading: Icon(Icons.info_outline),
                          title: Text(S.of(context).aboutVersion),
                          subtitle: Text(S.of(context).appVersion),
                        ),
                        ListTile(
                          leading: Icon(Icons.history),
                          title: Text(S.of(context).aboutChangelog),
                          onTap: () {
                            _showChangelog();
                          },
                        ),
                        ListTile(
                          leading: Icon(CustomIcons.gitlab),
                          title: Text(S.of(context).aboutGitLab),
                          subtitle: Text(S.of(context).aboutGitLabText),
                          onTap: () {
                            _launchUrl(Constants.gitLabUrl);
                          },
                        ),
                        ListTile(
                          leading: Icon(Icons.description),
                          title: Text(S.of(context).aboutLicenses),
                          subtitle: Text(S.of(context).aboutLicensesText),
                          isThreeLine: true,
                          onTap: () {
                            showLicensePage(
                              context: context,
                              applicationVersion: S.of(context).appVersion,
                              applicationIcon: Image(
                                image: AssetImage("assets/images/ic_launcher_crypto_orange_3_round.png"),
                                width: 50,
                                height: 50,
                              ),
                            );
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: _buildCard(
                title: S.of(context).aboutGroupCoinGecko,
                children: [
                  ListTile(
                    leading: Image(
                      image: AssetImage("assets/images/coinGecko_logo.png"),
                      width: 30,
                      height: 30,
                    ),
                    title: Text(
                      S.of(context).aboutCoinGeckoName,
                      style: _mediumFont,
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.language),
                    title: Text(S.of(context).aboutCoinGeckoWebsite),
                    onTap: () {
                      _launchUrl(Constants.coinGeckoUrl);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.network_locked),
                    title: Text(S.of(context).rateLimitInfoTitle),
                    onTap: () {
                      Navigator.of(context).push(Util.createSlidingRoute(RateLimitationInfo()));
                    },
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: _buildCard(
                title: S.of(context).aboutGroupAuthor,
                children: [
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text(S.of(context).aboutAuthorName),
                    subtitle: Text(S.of(context).aboutAuthorCountry),
                  ),
                  ListTile(
                    leading: Icon(Icons.mail_outline),
                    title: Text(S.of(context).aboutAuthorMail),
                    onTap: () {
                      _launchUrl(Constants.emailLaunchUri.toString());
                    },
                  )
                ]
              )
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: _buildCard(
                title: S.of(context).aboutGroupSupport,
                children: [
                  ListTile(
                    leading: Icon(Icons.paid),
                    title: Text(S.of(context).donateTitle),
                    onTap: () {
                      Navigator.of(context).push(Util.createSlidingRoute(DonationWidget()));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.star),
                    title: Text(S.of(context).aboutSupportRate),
                    subtitle: Text(S.of(context).aboutSupportRateText),
                    isThreeLine: true,
                    onTap: () {
                      _launchUrl(Constants.playStoreUrl);
                    },
                  )
                ]
              )
            )
          ],
        ),
      )
    );
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({required String title, required List<Widget> children}) {
    return Card(
      child: Column(
        children: [
          ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              ListTile(
                dense: true,
                title: Text(
                  title,
                  style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ]..addAll(children),
          )
        ],
      ),
    );
  }

  ///Displays the changelog for the chosen language
  void _showChangelog() async {
    String locale = SettingsDAO().getLocaleHive();
    String changelogJson = await DefaultAssetBundle.of(context).loadString("assets/changelogs/changelog_$locale.json");
    List changelog = jsonDecode(changelogJson);

    _showAlertDialog(
      Text(S.of(context).aboutChangelog),
      SingleChildScrollView(
        child: Container(
          width: double.maxFinite,
          height: MediaQuery.of(context).size.height / 2,
          child: ListView.builder(
            itemCount: changelog.length,
            itemBuilder: (context, i) {
              int j = changelog.length - (i + 1); //build list with newest entry first
              return _buildChangelogEntry(changelog[j]);
            }
          ),
        )
      )
    );
  }

  ///Builds one version entry in from the changelog
  Widget _buildChangelogEntry(Map<String, dynamic> json) {
    List changes = json["changes"];
    return Column(
      children: [
        Text(
          S.of(context).aboutVersion + " " + json["version"],
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Container(
          width: double.maxFinite,
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: changes.length,
            itemBuilder: (context, i) {
              return Text("- ${changes[i]}");
            }
          ),
        ), SizedBox(height: 20,)
      ],
    );
  }

  ///Launches an url in the browser
  void _launchUrl(String url) async =>
      await canLaunch(url)
          ? await launch(url)
          : _showAlertDialog(
          Text(S.of(context).urlLaunchAlertDialogTitle),
          Text(S.of(context).urlLaunchAlertDialogMessage)
      );

  ///Displays an alert dialog with the given title and message
  void _showAlertDialog(Widget title, Widget message) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: title,
            content: message,
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(S.of(context).alertDialogClose)
              )
            ],
          );
        }
    );
  }

}