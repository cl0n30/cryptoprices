import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/currency_changed_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/about_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/authentication_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/currency_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/backup_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/notifications_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/portfolio_settings.dart';
import 'package:crypto_prices/widgets/settingsScreens/security_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/start_screen_widget.dart';
import 'package:flutter/material.dart';

import '../../generated/l10n.dart';
import 'appearance_widget.dart';
import 'language_widget.dart';

///Displays the settings
class SettingsWidget extends StatefulWidget {

  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  SettingsDAO _settingsDAO = SettingsDAO();
  String _currency = SettingsDAO().getCurrencyHive();
  String _language = "";
  StartScreen _startScreen = SettingsDAO().getStartScreen();

  @override
  Widget build(BuildContext context) {
    _language = Util.localeToLanguageName(_settingsDAO.getLocaleHive(), context);
    return ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.attach_money),
          title: Text(S.of(context).settingsCurrency),
          subtitle: Text(_currency.toUpperCase()),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(CurrencyWidget(
              onCurrencyChange: _onCurrencyChange,
              setCurrency: _settingsDAO.getCurrencyHive(),
            )));
          }
        ),
        ListTile(
          leading: Icon(Icons.language),
          title: Text(S.of(context).settingsLanguage),
          subtitle: Text(_language),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(LanguageWidget(onLanguageChanged: _onLanguageChange)));
          }
        ),
        ListTile(
          leading: Icon(Icons.home),
          title: Text(S.of(context).settingsStartScreen),
          subtitle: Text(Util.startScreenToName(_startScreen, context)),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(StartScreenWidget(onStartScreenChanged: _onStartScreenChanged)));
          }
        ),
        ListTile(
          leading: Icon(Icons.pie_chart),
          title: Text(S.of(context).settingsPortfolio),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(PortfolioSettings()));
          }
        ),
        ListTile(
          leading: Icon(Icons.notifications),
          title: Text(S.of(context).settingsNotifications),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(NotificationsWidget()));
          }
        ),
        ListTile(
          leading: Icon(Icons.lock),
          title: Text(S.of(context).settingsSecurity),
          onTap: () async {
            final authenticated = await _isAuthenticated();
            if (authenticated) {
              Navigator.of(context).push(Util.createSlidingRoute(SecurityWidget()));
            } else {
              _showSnackbar(S.of(context).authenticationRequired);
            }
          }
        ),
        ListTile(
          leading: Icon(Icons.brush),
          title: Text(S.of(context).settingsAppearance),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(AppearanceWidget()));
          }
        ),
        ListTile(
          leading: Icon(Icons.backup),
          title: Text(S.of(context).settingsImportExport),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(ImportExportWidget()));
          }
        ),
        ListTile(
          leading: Icon(Icons.info),
          title: Text(S.of(context).settingsAbout),
          onTap: () {
            Navigator.of(context).push(Util.createSlidingRoute(AboutWidget()));
          },
        ),
      ],
    );
  }

  ///Changes the currency of the app if it is changed in the CurrencyWidget and notifies
  ///the widgets by setting their values in the CurrencyChanged box in the database
  void _onCurrencyChange(String newValue) {
    var oldCurrency = _currency;
    setState(() {
      _currency = newValue;
    });

    _settingsDAO.setCurrency(_currency);
    PortfolioDao().changePortfolioAppCurrency(_currency);
    if (_currency != oldCurrency) {
      CurrencyChangedDao().setAllCurrencyChanged(true);
    } else {
      CurrencyChangedDao().setAllCurrencyChanged(false);
    }
  }

  ///Change displayed language if it is changed in the LanguageWidget
  void _onLanguageChange(String locale) {
    setState(() {
      _language = Util.localeToLanguageName(locale, context);
    });
  }

  ///Change displayed start screen if it is changed in the StartScreenWidget
  void _onStartScreenChanged(StartScreen newStartScreen) {
    setState(() {
      _startScreen = newStartScreen;
    });
  }

  ///Opens the authentication screen, waits for the result and returns it.
  Future<bool> _isAuthenticated() async {
    if (_settingsDAO.getPassword().isEmpty || _settingsDAO.getIsAuthenticated()) {
      return true;
    } else {
      final result = await Navigator.of(context).push(Util.createSlidingRoute(AuthenticationWidget()));
      if (result == Constants.successResult) {
        return true;
      } else {
        return false;
      }
    }
  }

  ///Shows a snackbar with the message
  void _showSnackbar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}