import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

///Show a password field or a biometrics dialog for user authentication to unlock the portfolio
class AuthenticationWidget extends StatefulWidget {
  static const routeName = "/authentication";

  ///Should the app be closed when going back from this widget
  final bool closeAppOnBackPressed;

  const AuthenticationWidget({
    Key? key,
    this.closeAppOnBackPressed = false
  }) : super(key: key);

  @override
  _AuthenticationWidgetState createState() => _AuthenticationWidgetState();
}

class _AuthenticationWidgetState extends State<AuthenticationWidget> {
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _localAuthentication = LocalAuthentication();

  bool _showPassword = false;
  String _password = "";

  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();

    _password = _settingsDAO.getPassword();
    if (_settingsDAO.getBiometricsAvailable() && _settingsDAO.getBiometricsAllowed()) {
      _authenticateWithBiometrics(showErrorMessage: false);
    }
  }

  @override
  void dispose() {
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.fromLTRB(64, 0, 64, 0),
            child: Align(
              alignment: Alignment.center,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage("assets/images/ic_launcher_crypto_orange_3_round.png"),
                      width: 120,
                      height: 120,
                    ),
                    SizedBox(height: 50),
                    Text(
                      S.current.authenticationTitle,
                      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      controller: _passwordController,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                        border: OutlineInputBorder(),
                        hintText: S.current.editPasswordEnterHint,
                        suffixIcon: IconButton(
                          icon: Icon(_showPassword ? Icons.visibility_off : Icons.visibility),
                          tooltip: S.current.editPasswordShowPassword,
                          onPressed: () {
                            setState(() {
                              _showPassword = !_showPassword;
                            });
                          },
                        ),
                        errorStyle: TextStyle(
                          fontSize: 14,
                          color: (currentTheme.isThemeDark())
                              ? Constants.errorColorDark
                              : Constants.errorColor,
                        ),
                      ),
                      obscureText: !_showPassword,
                      autocorrect: false,
                      enableSuggestions: false,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return S.current.authenticationNoPasswordError;
                        }
                        if (value != _password) {
                          return S.current.authenticateWrongPasswordError;
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            child: Text(
                              S.current.authenticationUnlockButton,
                              style: TextStyle(fontSize: 18),
                            ),
                            onPressed: () {
                              _validateForm();
                            }
                          ),
                        ),
                      ],
                    ),
                    if (_settingsDAO.getBiometricsAllowed())
                      TextButton(
                        child: Text(S.current.authenticationUseBiometrics),
                        onPressed: () {
                          _authenticateWithBiometrics();
                        }
                      ),
                    SizedBox(height: 30),
                    TextButton(
                      child: Text(S.current.authenticationContinueWithoutPassword),
                      style: TextButton.styleFrom(primary: Colors.red),
                      onPressed: () {
                        _continueWithoutPassword();
                      }
                    )
                  ],
                ),
              ),
            )
          ),
        )
      ),
      onWillPop: () async {
        final _androidAppRetain = MethodChannel(Constants.sendToBackMethodChannel);
        if (widget.closeAppOnBackPressed) {
          _androidAppRetain.invokeMethod(Constants.sendToBackgroundMethod);
          return false;
        }
        return true;
      }
    );
  }

  ///Checks if the Form is valid
  void _validateForm() {
    if (_formKey.currentState!.validate()) {
      _authenticateSuccessful(true);
      Navigator.pop(context, Constants.successResult);
    }
  }

  ///Don't unlock the portfolio
  void _continueWithoutPassword() {
    _authenticateSuccessful(false);
    Navigator.pop(context);
  }

  ///Open the system dialog for biometric auth.
  ///If no biometrics are enrolled show error message, or not
  Future<void> _authenticateWithBiometrics({bool showErrorMessage = true}) async {
    bool authenticated = false;
    try {
      final options = AuthenticationOptions(
        biometricOnly: true,
        useErrorDialogs: true, // show error in dialog
        stickyAuth: true,
      );
      authenticated = await _localAuthentication.authenticate(
        localizedReason: S.current.authenticationTitle, // message for dialog
        options: options
      );
      if (authenticated) {
        _authenticateSuccessful(true);
        Navigator.pop(context, Constants.successResult);
      }
    } on PlatformException catch (e) {
      if (e.code == auth_error.notEnrolled && showErrorMessage) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(S.current.noBiometrics),
            duration: Duration(seconds: 2),
          )
        );
      }
    }
  }

  ///If the authentication was successful or not set everything accordingly
  void _authenticateSuccessful(bool successful) {
    _settingsDAO.setIsAuthenticated(successful);
    _settingsDAO.setPrivateMode(!successful);
  }
}
