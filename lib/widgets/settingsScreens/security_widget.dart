import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/authentication_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/edit_password_widget.dart';
import 'package:flutter/material.dart';

///Displays the security settings
class SecurityWidget extends StatefulWidget {
  const SecurityWidget({Key? key}) : super(key: key);

  @override
  _SecurityWidgetState createState() => _SecurityWidgetState();
}

class _SecurityWidgetState extends State<SecurityWidget> {
  SettingsDAO _settingsDAO = SettingsDAO();

  int _stayAuthenticatedMinutes = Constants.stayAuthenticatedMinutesDefault;

  @override
  void initState() {
    super.initState();

    _stayAuthenticatedMinutes = _settingsDAO.getStayAuthenticatedMinutes();
  }

  final _minutes = Constants.stayAuthenticatedMinutes;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(S.of(context).settingsSecurity)
      ),
      body: ListView(
        children: <Widget>[
          SwitchListTile(
            title: Text(S.of(context).privateMode),
            subtitle: Text(S.of(context).settingsSecurityPrivateModeSubtitle),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getPrivateMode(),
            onChanged: (bool newValue) {
              setState(() {
                _settingsDAO.setPrivateMode(newValue);
              });
            },
          ),
          SwitchListTile(
            title: Text(S.of(context).settingsSecurityPasswordLock),
            subtitle: Text(S.of(context).settingsSecurityPasswordLockSubtitle),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getPassword().isNotEmpty,
            onChanged: (bool newValue) async {
              if (newValue) {
                final result = await Navigator.of(context).push(Util.createSlidingRoute(EditPasswordWidget()));
                if (result == Constants.successResult) {
                  setState(() {
                  });
                }
              } else {
                _removePassword();
              }
            },
          ),
          ListTile(
            title: Text(S.of(context).settingsSecurityChangePassword),
            enabled: _settingsDAO.getPassword().isNotEmpty,
            onTap: () {
              Navigator.of(context).push(Util.createSlidingRoute(EditPasswordWidget())).then((_) {
                setState(() {
                });
              });
            },
          ),
          if (_settingsDAO.getBiometricsAvailable())
            SwitchListTile(
              title: Text(S.of(context).settingsSecurityBiometricUnlock),
              subtitle: Text(S.of(context).settingsSecurityBiometricUnlockSubtitle),
              isThreeLine: true,
              activeColor: Theme.of(context).colorScheme.secondary,
              value: _settingsDAO.getBiometricsAllowed(),
              onChanged: (bool enabled) {
                setState(() {
                  _settingsDAO.setBiometricsAllowed(enabled);
                });
              },
            ),
          SwitchListTile(
            title: Text(S.of(context).settingsSecurityAuthOnAppStart),
            subtitle: Text(S.of(context).settingsSecurityAuthOnAppStartSubtitle),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getOpenAuthOnAppStart(),
            onChanged: (bool enabled) {
              setState(() {
                _settingsDAO.setOpenAuthOnAppStart(enabled);
              });
            },
          ),
          ListTile(
            title: Text(S.of(context).settingsSecurityKeepUnlocked),
            subtitle: (_stayAuthenticatedMinutes.toInt() == -1)
                ? Text(S.of(context).never)
                : Text(S.of(context).minutes(_stayAuthenticatedMinutes)),
            onTap: () {
              _showThresholdDialog();
            },
          ),
          if (_settingsDAO.getPassword().isNotEmpty)
            ListTile(
              title: Text(S.of(context).settingsSecurityLockNow),
              leading: Icon(Icons.lock),
              trailing: Icon(Icons.chevron_right),
              onTap: () {
                _settingsDAO.setIsAuthenticated(false);
                _settingsDAO.setPrivateMode(true);
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => AuthenticationWidget(closeAppOnBackPressed: true)));
              },
            )
        ],
      ),
    );
  }

  ///Opens an alert dialog to confirm removal of the password lock
  Future<void> _removePassword() async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).editPasswordRemove),
          content: Text(S.of(context).alertDialogDeletePriceAlertMessage),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).alertDialogCancel)
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  _settingsDAO.setPassword(Constants.passwordDefault);
                });
                Navigator.pop(context); //pop dialog
              },
                child: Text(S.of(context).alertDialogRemove)
            ),
          ],
        );
      }
    );
  }

  ///Shows a dialog to select how log the app should stay unlocked after closing
  void _showThresholdDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(S.of(context).settingsSecurityKeepUnlockedDialogTitle),
          children: [
            SimpleDialogOption(
              child: RadioListTile(
                title: Text(S.of(context).never),
                activeColor: (currentTheme.isThemeDark()
                    ? Constants.accentColorDark
                    : Constants.accentColorLight),
                value: _minutes[0],
                groupValue: _stayAuthenticatedMinutes,
                onChanged: (int? value) {
                  setState(() {
                    _stayAuthenticatedMinutes = value!;
                    _settingsDAO.setStayAuthenticatedMinutes(value);
                    Navigator.pop(context);
                  });
                }
              ),
            ),
            _buildDialogOption(_minutes[1]),
            _buildDialogOption(_minutes[2]),
            _buildDialogOption(_minutes[3]),
            _buildDialogOption(_minutes[4]),
          ],
        );
      }
    );
  }

  ///Returns a dialog option for the given minutes value
  SimpleDialogOption _buildDialogOption(int minutes) {
    return SimpleDialogOption(
      child: RadioListTile(
        title: Text(S.of(context).minutes(minutes)),
        activeColor: (currentTheme.isThemeDark()
            ? Constants.accentColorDark
            : Constants.accentColorLight),
        value: minutes,
        groupValue: _stayAuthenticatedMinutes,
        onChanged: (int? value) {
          setState(() {
            _stayAuthenticatedMinutes = value!;
            _settingsDAO.setStayAuthenticatedMinutes(value);
            Navigator.pop(context);
          });
        }
      ),
    );
  }
}
