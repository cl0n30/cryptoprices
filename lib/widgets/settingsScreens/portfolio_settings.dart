import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/settingsScreens/explorer_urls_list.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

///Displays the portfolio settings
class PortfolioSettings extends StatefulWidget {
  PortfolioSettings({Key? key}) : super(key: key);

  @override
  _PortfolioSettingsState createState() => _PortfolioSettingsState();
}

class _PortfolioSettingsState extends State<PortfolioSettings> {
  SettingsDAO _settingsDAO = SettingsDAO();

  TimePeriod _selectedTimePeriod = TimePeriod.day;

  @override
  void initState() {
    super.initState();

    _selectedTimePeriod = _settingsDAO.getPortfolioSelectedPeriod();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(S.of(context).settingsPortfolio)
      ),
      body: ListView(
        children: [
          SwitchListTile(
            title: Text(S.of(context).settingsPortfolioAddFavoriteTitle),
            subtitle: Text(S.of(context).settingsPortfolioAddToFavoriteSubtitle),
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getAddWalletToFavoritesHive(),
            onChanged: (value) {
              setState(() {
                _settingsDAO.setAddWalletToFavorites(value);
              });
            }
          ),
          ListTile(
            title: Text(S.of(context).settingsPortfolioChangePercentagePeriodTitle),
            subtitle: Text(_selectedPeriodString(_selectedTimePeriod)),
            onTap: () {
              _showSelectPeriodDialog();
            },
          ),
          ListTile(
            dense: true,
            title: Text(
              S.of(context).settingsPortfolioTransactions,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          SwitchListTile(
            title: Text(S.of(context).settingsPortfolioTransactionsPortfolioCurrencyTitle),
            subtitle: Text(S.of(context).settingsPortfolioTransactionsPortfolioCurrencySubtitle),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getDisplayInPortfolioCurrency(),
            onChanged: (value) {
              setState(() {
                _settingsDAO.setDisplayInPortfolioCurrency(value);
              });
            }
          ),
          SwitchListTile(
            title: Text(S.of(context).useOwnCoinPriceTitle),
            subtitle: Text(S.of(context).useOwnCoinPriceSubtitle),
            isThreeLine: true,
            activeColor: Theme.of(context).colorScheme.secondary,
            value: _settingsDAO.getUseOwnCoinPriceHive(),
            onChanged: (value) {
              setState(() {
                _settingsDAO.setUseOwnCoinPrice(value);
              });
            }
          ),
          ListTile(
            trailing: Icon(Icons.chevron_right),
            title: Text(S.of(context).settingsPortfolioTransactionsExplorerUrlsTitle),
            subtitle: Text(S.of(context).settingsPortfolioTransactionsExplorerUrlsSubtitle),
            onTap: () {
              Navigator.of(context).push(Util.createSlidingRoute(ExplorerUrlsList()));
            },
          ),
        ],
      ),
    );
  }

  ///Returns the localized string for each time period
  String _selectedPeriodString(TimePeriod period) {
    switch (period) {
      case TimePeriod.hour: {
        return S.of(context).timePeriodHour;
      }

      case TimePeriod.day: {
        return S.of(context).timePeriodDay;
      }

      case TimePeriod.week: {
        return S.of(context).timePeriodWeek;
      }

      case TimePeriod.month: {
        return S.of(context).timePeriodMonth;
      }

      case TimePeriod.year: {
        return S.of(context).timePeriodYear;
      }

      case TimePeriod.max: {
        return S.of(context).timePeriodMax;
      }
    }
  }

  ///Shows a dialog to select the change percentage period
  void _showSelectPeriodDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(S.of(context).settingsPortfolioChangePercentagePeriodTitle),
          children: [
            SimpleDialogOption(
              child: RadioListTile(
                title: Text(_selectedPeriodString(TimePeriod.day)),
                activeColor: (currentTheme.isThemeDark()
                    ? Constants.accentColorDark
                    : Constants.accentColorLight),
                value: TimePeriod.day,
                groupValue: _selectedTimePeriod,
                onChanged: (TimePeriod? value) {
                  setState(() {
                    _selectedTimePeriod = value!;
                    _settingsDAO.setPortfolioSelectedPeriod(value);
                    Navigator.pop(context);
                  });
                }
              ),
            ),
            SimpleDialogOption(
              child: RadioListTile(
                title: Text(_selectedPeriodString(TimePeriod.week)),
                activeColor: (currentTheme.isThemeDark()
                    ? Constants.accentColorDark
                    : Constants.accentColorLight),
                value: TimePeriod.week,
                groupValue: _selectedTimePeriod,
                onChanged: (TimePeriod? value) {
                  setState(() {
                    _selectedTimePeriod = value!;
                    _settingsDAO.setPortfolioSelectedPeriod(value);
                    Navigator.pop(context);
                  });
                }
              ),
            ),
            SimpleDialogOption(
              child: RadioListTile(
                title: Text(_selectedPeriodString(TimePeriod.month)),
                activeColor: (currentTheme.isThemeDark()
                    ? Constants.accentColorDark
                    : Constants.accentColorLight),
                value: TimePeriod.month,
                groupValue: _selectedTimePeriod,
                onChanged: (TimePeriod? value) {
                  setState(() {
                    _selectedTimePeriod = value!;
                    _settingsDAO.setPortfolioSelectedPeriod(value);
                    Navigator.pop(context);
                  });
                }
              ),
            ),
          ],
        );
      }
    );
  }
}