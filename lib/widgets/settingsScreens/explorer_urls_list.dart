import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/explorer_transaction_url_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/widgets/settingsScreens/explorer_url_edit.dart';
import 'package:flutter/material.dart';

///Shows a list of the explorer transaction urls of all coins
class ExplorerUrlsList extends StatefulWidget {
  ExplorerUrlsList({Key? key}) : super(key: key);

  @override
  _ExplorerUrlsListState createState() => _ExplorerUrlsListState();
}

class _ExplorerUrlsListState extends State<ExplorerUrlsList> {
  ExplorerTransactionUrlDao _explorerTransactionDao = ExplorerTransactionUrlDao();
  List<ExplorerTransactionUrl> _urls = [];
  ImageUrls _imageUrls = ImageUrls();
  AvailableCoinsDao _availableCoinsDao = AvailableCoinsDao();

  @override
  void initState() {
    super.initState();

    _urls = _explorerTransactionDao.getAllCoinExplorerTransactionUrls();

    _explorerTransactionDao.getAllExplorerTransactionUrlsWatcher().addListener(() {
      if (this.mounted) {
        setState(() {
          _urls = _explorerTransactionDao.getAllCoinExplorerTransactionUrls();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).transactionUrlsListTitle),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ExplorerUrlEdit()));
            },
            icon: Icon(Icons.add),
            tooltip: S.of(context).addTransactionUrl,
          )
        ],
      ),
      body: ListView.builder(
        padding: EdgeInsets.all(8.0),
        itemCount: _urls.length,
        itemBuilder: (context, i) {
          final coin = _availableCoinsDao.getAvailableCoin(_urls[i].coinId);
          return ListTile(
            leading: CachedNetworkImage(
              imageUrl: _imageUrls.getSmallImageUrl(_urls[i].coinId),
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error_outline),
              width: 25,
              height: 25,
            ),
            title: Text(coin?.name ?? _urls[i].coinId),
            subtitle: Text(_urls[i].url),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ExplorerUrlEdit(coinId: _urls[i].coinId))
              );
            },
          );
        }
      )
    );
  }
}