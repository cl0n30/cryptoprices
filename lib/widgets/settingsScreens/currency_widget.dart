import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:flutter/material.dart';

///Displays all available currencies
class CurrencyWidget extends StatefulWidget {
  CurrencyWidget({Key? key, required this.onCurrencyChange, required this.setCurrency});

  ///Notifies the parent which currency has been selected
  final ValueChanged<String> onCurrencyChange;

  ///The currency that is set on widget launch
  final String setCurrency;

  @override
  _CurrencyWidgetState createState() => _CurrencyWidgetState();
}

class _CurrencyWidgetState extends State<CurrencyWidget> {
  String _currency = "";

  @override
  void initState() {
    super.initState();
    _currency = widget.setCurrency;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).selectCurrencyTitle),
      ),
      body: ListView.builder(
        itemCount: Constants.supportedCurrencies.length,
        itemBuilder: (context, i) {
          final id = Constants.supportedCurrencies[i];
          return ListTile(
            title: Text(
              Constants.currencyText[id]!
            ),
            leading: Image(
              image: AssetImage("assets/images/currencies/$id.png"),
              width: 25,
              height: 25,
            ),
            trailing: (_currency == id) ? Icon(Icons.check) : null,
            onTap: () => _changeCurrency(id),
          );
        }
      ),
    );
  }

  ///Changes the currency selected in the list and notifies the parent widget of the new value
  void _changeCurrency(String curr) {
    setState(() {
      _currency = curr;
      widget.onCurrencyChange(curr);
      Navigator.of(context).pop();
    });
  }
}