import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/explorer_transaction_url_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'search_available_coins_delegate.dart';

///Widget to edit the explorer url of a coin
class ExplorerUrlEdit extends StatefulWidget {
  final String coinId;

  ExplorerUrlEdit({
    Key? key,
    this.coinId = ""
  }) : super(key: key);

  @override
  _ExplorerUrlEditState createState() => _ExplorerUrlEditState();
}

class _ExplorerUrlEditState extends State<ExplorerUrlEdit> {
  TextStyle _normalFont = TextStyle(fontSize: 16.0);
  TextStyle _normalFontBold = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
  TextStyle _biggerFontBold = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);

  String _coinId = "";
  late AvailableCoin _coin;
  late ExplorerTransactionUrl _url;

  TextEditingController _urlTextEditController = TextEditingController();

  ExplorerTransactionUrlDao _urlDao = ExplorerTransactionUrlDao();
  AvailableCoinsDao _availableCoinsDao = AvailableCoinsDao();

  @override
  void initState() {
    super.initState();

    _coinId = widget.coinId;

    if (_coinId.isNotEmpty) {
      if (_availableCoinsDao.getAvailableCoin(_coinId) != null) {
        _coin = _availableCoinsDao.getAvailableCoin(_coinId)!;
      } else {
        _coin = AvailableCoin(_coinId, name: _coinId);
      }

      if (_urlDao.getCoinExplorerTransactionUrl(widget.coinId) != null) {
        _url = _urlDao.getCoinExplorerTransactionUrl(_coinId)!;
        _urlTextEditController.text = _url.url;
      } else {
        _url = ExplorerTransactionUrl(_coinId);
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).explorerUrlTitle),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              _buildCard(
                title: S.of(context).alertCoin,
                children: [
                  ListTile(
                    title: Text(
                      (_coinId.isEmpty
                          ? S.of(context).explorerUrlSelectACoin
                          : "${_coin.name} (${_coin.symbol.toUpperCase()})"),
                      style: _biggerFontBold,
                    ),
                    leading: (_coinId.isEmpty
                        ? Icon(Icons.list)
                        : CachedNetworkImage(
                      imageUrl: ImageUrls().getSmallImageUrl(_coinId),
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error_outline),
                      width: 25,
                      height: 25,
                    )),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () async {
                      AvailableCoin? result = await showSearch<AvailableCoin?>(
                          context: context,
                          delegate: SearchAvailableCoinsDelegate(context)
                      );
                      if (result != null) { //coin has been selected from search
                        setState(() {
                          _coinId = result.id;
                          _coin = result;
                          if (_urlDao.getCoinExplorerTransactionUrl(_coinId) != null) {
                            _url = _urlDao.getCoinExplorerTransactionUrl(_coinId)!;
                            _urlTextEditController.text = _url.url;
                          } else {
                            _url = ExplorerTransactionUrl(_coinId);
                            _urlTextEditController.text = "";
                          }
                        });
                      }
                    }
                  ),
                ]
              ),
              _buildCard(
                title: S.of(context).explorerUrlTransactionUrl,
                children: [
                  Container(
                    padding: EdgeInsets.all(16),
                    child: TextField(
                      controller: _urlTextEditController,
                      maxLines: 1,
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary)),
                          border: OutlineInputBorder(),
                          hintText: S.of(context).explorerUrlTransactionUrlHint,
                          errorStyle: TextStyle(
                            fontSize: 14,
                            color: (currentTheme.isThemeDark())
                                ? Constants.errorColorDark
                                : Constants.errorColor,
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _urlTextEditController.text = "";
                            },
                          )
                      ),
                    ),
                  )
                ]
              ),
              Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    Icon(Icons.info_outline),
                    SizedBox(width: 10),
                    Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              S.of(context).explorerUrlTransactionUrlInfo,
                              style: _normalFont,
                            ),
                            Text(
                                S.of(context).explorerUrlTransactionUrlInfoExample,
                                style: _normalFontBold
                            )
                          ],
                        )
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
        onPressed: () {
          if (_coinId.isEmpty) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).explorerUrlNoCoinSelected),
                duration: Duration(seconds: 2),
              )
            );
          } else {
            if (_urlTextEditController.text.isEmpty) {
              _urlDao.deleteCoinExplorerTransactionUrl(_coinId);
            } else {
              _url.url = _urlTextEditController.text;
              _urlDao.insertCoinExplorerTransactionUrl(_url);
            }
            Navigator.pop(context);
          }
        }
      ),
    );
  }

  @override
  void dispose() {
    _urlTextEditController.dispose();
    super.dispose();
  }

  ///Builds a Card widget that has a title and a child widget.
  ///Used to avoid repeating code when laying out title and content.
  Widget _buildCard({required String title, required List<Widget> children, EdgeInsets textPadding = const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 8.0)}) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: textPadding,
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ]..addAll(children),
      ),
    );
  }
}