import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/coin_id_migration_data.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/manual_migration_handler.dart';
import 'package:crypto_prices/widgets/settingsScreens/search_available_coins_delegate.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

///Widget that shows all possible new coin id suggestions for an old coin id
class MigrationDataEntryWidget extends StatefulWidget {
  final String oldCoinId;
  
  final String oldCoinName;
  
  final String oldCoinSymbol;

  final String newCoinId;

  final ValueChanged<Tuple2<String, String>> onCoinMigrate;
  
  const MigrationDataEntryWidget({
    Key? key,
    required this.oldCoinId,
    required this.oldCoinName,
    required this.oldCoinSymbol,
    required this.onCoinMigrate,
    this.newCoinId = ""
  }) : super(key: key);

  @override
  _MigrationDataEntryWidgetState createState() => _MigrationDataEntryWidgetState();
}

class _MigrationDataEntryWidgetState extends State<MigrationDataEntryWidget> {
  final _boldFont = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
  final _normalText = TextStyle(fontSize: 16);
  final _smallerFont = TextStyle(fontSize: 14.0);
  bool _collapsed = false;
  
  String _newCoinId = "";
  String _newCoinName = "";
  String _newCoinSymbol = "";

  CoinIdMigrationData _data = CoinIdMigrationData("");
  
  @override
  void initState() {
    super.initState();

    _newCoinId = widget.newCoinId;
    if (_newCoinId.isNotEmpty) {
      final newCoin = AvailableCoinsDao().getAvailableCoin(_newCoinId)!;
      _newCoinName = newCoin.name;
      _newCoinSymbol = newCoin.symbol;
    }
    
    _data = ManualMigrationHandler().getCoinIdDataToMigrate(widget.oldCoinId);
  }

  @override
  Widget build(BuildContext context) {
    if (_newCoinId.isEmpty) {
      return Card(
        child: InkWell(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            widget.oldCoinName,
                            style: _boldFont
                          ),
                          SizedBox(width: 10),
                          Text(
                            widget.oldCoinSymbol.toUpperCase(),
                            style: _smallerFont
                          )
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(16.0),
                    child: Icon((_collapsed)
                        ? Icons.arrow_drop_down
                        : Icons.arrow_drop_up
                    ),
                  )
                ],
              ),
              AnimatedCrossFade(
                  firstChild: Row(),
                  secondChild: _buildSuggestions(),
                  crossFadeState: _collapsed
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  duration: Duration(milliseconds: 200)
              )
            ],
          ),
          onTap: () {
            _toggleCollapse();
          },
        )
      );
    } else {
      return Card(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    child: Row(
                      children: [
                        Text(
                          _newCoinName,
                          style: _boldFont
                        ),
                        SizedBox(width: 10),
                        Text(
                          _newCoinSymbol.toUpperCase(),
                          style: _smallerFont
                        )
                      ],
                    ),
                    padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                  ),
                ),
                Container(
                  child: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      setState(() {
                        _newCoinId = "";
                        widget.onCoinMigrate(Tuple2(widget.oldCoinId, _newCoinId));
                      });
                    },
                  )
                ),
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.check, color: Colors.green)
                )
              ],
            )
          ],
        ),
      );
    }
  }

  ///Builds the list of all suggestions for this coin and a list tile to open the search
  Widget _buildSuggestions() {
    return Card(
      color: (currentTheme.isThemeDark())
          ? Colors.black12
          : Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: Text(S.of(context).migrationAssistantEntrySelectCoin, style: _normalText),
          ),
          Divider(height: 0, thickness: 1),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _getSuggestions().length + 1,
            itemBuilder: (context, i) {
              if (i == _getSuggestions().length) {
                return ListTile(
                  title: Text(S.of(context).migrationAssistantEntrySearchCoins),
                  leading: Icon(Icons.search),
                  trailing: Icon(Icons.chevron_right),
                  onTap: () async {
                    AvailableCoin? result = await showSearch<AvailableCoin?>(
                        context: context,
                        delegate: SearchAvailableCoinsDelegate(context)
                    );
                    if (result != null) { //coin has been selected from search
                      setState(() {
                        _newCoinId = result.id;
                        _newCoinName = result.name;
                        _newCoinSymbol = result.symbol;
                        widget.onCoinMigrate(Tuple2(widget.oldCoinId, _newCoinId));
                      });
                    }
                  }
                );
              } else {
                return _buildListTile(_getSuggestions()[i]);
              }
            },
          )
        ],
      )
    );
  }

  ///Builds a list tile for a suggestion. OnTap selects this suggestion for this coin id
  Widget _buildListTile(AvailableCoin suggestion) {
    String symbol = "";
    if (suggestion.symbol.isNotEmpty) {
      symbol = "(${suggestion.symbol.toUpperCase()})";
    }
    return Column(
      children: [
        ListTile(
          title: Text("${suggestion.name} $symbol"),
          leading: CachedNetworkImage(
            imageUrl: ImageUrls().getLargeImageUrl(suggestion.id),
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error_outline),
            height: 25.0,
            width: 25.0,
          ),
          onTap: () {
            setState(() {
              _newCoinId = suggestion.id;
              _newCoinName = suggestion.name;
              _newCoinSymbol = suggestion.symbol;
              widget.onCoinMigrate(Tuple2(widget.oldCoinId, _newCoinId));
            });
          },
        ),
        Divider(height: 0, thickness: 1)
      ],
    );
  }

  ///Collapses or expands the entry
  void _toggleCollapse() {
    setState(() {
      _collapsed = !_collapsed;
    });
  }

  ///Builds a list with possible new coins
  List<AvailableCoin> _getSuggestions() {
    final allCoins = AvailableCoinsDao().getAllAvailableCoins();
    if (widget.oldCoinSymbol.isNotEmpty) {
      return allCoins.where((element) => (element.symbol == widget.oldCoinSymbol)).toList();
    } else {
      return [];
    }
  }
}
