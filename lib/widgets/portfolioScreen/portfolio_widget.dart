import 'dart:io';

import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/market_data_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/charts/portfolio_pie_chart.dart';
import 'package:crypto_prices/widgets/detailScreen/wallet_detail_screen.dart';
import 'package:crypto_prices/widgets/private_price_text.dart';
import 'package:crypto_prices/widgets/portfolioScreen/edit_portfolio_widget.dart';
import 'package:crypto_prices/widgets/portfolioScreen/wallet_row_widget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../constants.dart';
import '../settingsScreens/rate_limitation_info.dart';

class PortfolioWidget extends StatefulWidget {
  ///The id of the currently selected portfolio
  final int selectedPortfolioId;

  ///The order of the wallets e.g. by name, total value
  final WalletOrderBy order;

  PortfolioWidget({
    Key? key,
    this.selectedPortfolioId = 0,
    this.order = WalletOrderBy.totalValueDesc
  }) : super(key: key);

  @override
  _PortfolioWidgetState createState() => _PortfolioWidgetState();
}

class _PortfolioWidgetState extends State<PortfolioWidget> {
  late TextStyle _changeGreen;
  late TextStyle _changeRed;
  late TextStyle _linkFont;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  ///should all data be refreshed?
  bool _refreshTriggered = false;

  String _currency = SettingsDAO().getCurrencyHive();

  Portfolio _selectedPortfolio = Portfolio(0, "");
  List<Wallet> _portfolioWallets = [Wallet(0, "", 0)];
  List<MarketData> _marketData = [];
  double _totalValue = 0;
  double _changePercentage = 0;

  PortfolioDao _portfolioDao = PortfolioDao();
  WalletDao _walletDao = WalletDao();
  SettingsDAO _settingsDAO = SettingsDAO();
  MarketDataDao _marketDataDao = MarketDataDao();

  ApiInteraction _api = CoinGeckoAPI();

  late Future<List<MarketData>> _fetchMarketData;

  ///Selected time interval in the graph toggle buttons
  TimePeriod _selectedTimePeriod = TimePeriod.day;

  @override
  void initState() {
    super.initState();

    _changeGreen = currentTheme.isThemeDark()
        ? TextStyle(fontSize: 16.0, color: Constants.priceChangeGreenDark)
        : TextStyle(fontSize: 16.0, color: Constants.priceChangeGreenLight);

    _changeRed = currentTheme.isThemeDark()
        ? TextStyle(fontSize: 16.0, color: Constants.priceChangeRedDark)
        : TextStyle(fontSize: 16.0, color: Constants.priceChangeRedLight);

    if (widget.selectedPortfolioId != Constants.NODEFAULTPORTFOLIOID) {
      _updatePortfolio();
    }

    _portfolioDao.getPortfolioWatcher(widget.selectedPortfolioId).addListener(_portfolioUpdateListener);

    //update list if the selected portfolio changes
    _walletDao.getAllWalletsWatcher().addListener(_walletUpdateListener); //TODO only trigger when portfolio wallets change (new or delete), result -1 in transaction
  }

  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 15.0, color: Theme.of(context).colorScheme.secondary);

    if (widget.selectedPortfolioId == Constants.NODEFAULTPORTFOLIOID) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            S.of(context).noPortfolios,
            textAlign: TextAlign.center,
          )
        ],
      );
    }

    //different portfolio selected
    if (_selectedPortfolio.id != widget.selectedPortfolioId) {
      //update listener to the new portfolio
      _portfolioDao.getPortfolioWatcher(_selectedPortfolio.id).removeListener(_portfolioUpdateListener);
      _updatePortfolio();
      _portfolioDao.getPortfolioWatcher(widget.selectedPortfolioId).addListener(_portfolioUpdateListener);
    }

    _portfolioWallets.sort(_getSortingFunction());
    if (_portfolioWallets.isNotEmpty && _marketData.isEmpty) { //market data from api
      _refreshTriggered = true;
      return _buildPortfolioFuture();
    } else if (_portfolioWallets.isNotEmpty && _marketData.isNotEmpty) { //market data from database
      return _buildPortfolioDatabase();
    } else {
      return _buildEmptyPortfolio();
    }
  }

  ///Displays the portfolio overview after getting new data from the API if necessary
  Widget _buildPortfolioFuture() {
    return Align(
      alignment: Alignment.topCenter,
      child: FutureBuilder<List<MarketData>>(
        future: _fetchMarketData,
        builder: (context, snapshot) {
          //snapshot data doesn't change when reloading future (reassigning _fetchMarketData)
          //only connection state changes, so future will complete early with the old data
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 10),
                  Text(S.of(context).loadingDataMessage)
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            );
          }
          if (snapshot.hasData) {
            print("future");
            _marketData = snapshot.data!;
            _totalValue = _getTotalValue(_portfolioWallets, _marketData);
            _changePercentage = _getChangePercentage(_portfolioWallets, _marketData, _selectedTimePeriod);
            _refreshTriggered = false;

            return _portfolioScreen();
          } else if (snapshot.hasError) {
            if (snapshot.error is SocketException) {
              return _buildError(S.of(context).errorMessageSocket);
            }

            if (snapshot.error is HttpException) {
              return _buildError(S.of(context).errorMessageHttp);
            }

            if (snapshot.error is RateLimitationException) {
              return RefreshIndicator(
                  key: _refreshIndicatorKey,
                  onRefresh: _onRefresh,
                  child: LayoutBuilder(
                    builder: (context, constraints) => ListView( //no single child scrollview to constrain height
                      children: [
                        Container(
                          height: constraints.maxHeight,
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  S.of(context).errorMessageRateLimitation,
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(height: 10),
                                RichText(
                                    overflow: TextOverflow.ellipsis,
                                    text: TextSpan(
                                        text: S.of(context).moreInformation,
                                        style: _linkFont,
                                        recognizer: TapGestureRecognizer()..onTap = () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(builder: (context) => RateLimitationInfo())
                                          );
                                        }
                                    )
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
              );
            }

            return _buildError(S.of(context).error + ": " + snapshot.error.toString());
          }

          return Center(
            child: Column(
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 10),
                Text(S.of(context).loadingDataMessage)
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          );
        }
      ),
    );
  }

  ///Displays the portfolio overview with market data from the database
  Widget _buildPortfolioDatabase() {
    print("database");
    return Align(
      alignment: Alignment.topCenter,
      child: _portfolioScreen()
    );
  }

  ///Displays an empty portfolio overview
  Widget _buildEmptyPortfolio() {
    return Align(
      alignment: Alignment.topCenter,
      child: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).portfolioTotalValue,
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                              "0 ${Constants.currencySymbols[_currency]}",
                              style: TextStyle(fontSize: 18)
                          ),
                          SizedBox(height: 10),
                          Text(
                              "0.00%",
                              style: (_changePercentage.isNegative ? _changeRed : _changeGreen)
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Text(S.of(context).portfolioIsEmpty),
                SizedBox(height: 72) //don't obscure last entry with FAB
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Builds the portfolio screen
  Widget _portfolioScreen() {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _onRefresh,
      child: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      S.of(context).portfolioTotalValue,
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        GestureDetector(
                          child: PrivatePriceText(
                            price: Util.formatPrice(_totalValue, twoDecimalsAbove: 1),
                            symbol: Constants.currencySymbols[_currency]!,
                            style: TextStyle(fontSize: 18),
                          ),
                          onLongPress: () {
                            if (_settingsDAO.getPrivateMode()) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(S.of(context).notAvailableInPrivateMode),
                                  duration: Duration(seconds: 2),
                                ),
                              );
                            } else {
                              Clipboard.setData(ClipboardData(text: _totalValue.toString()));
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(S.of(context).valueCopied),
                                  duration: Duration(seconds: 2),
                                ),
                              );
                            }
                          },
                        ),
                        SizedBox(height: 10),
                        PrivatePriceText(
                          price: Util.formatPercentage(_changePercentage),
                          symbol: "%",
                          percentage: true,
                          style: (_changePercentage.isNegative ? _changeRed : _changeGreen)
                        )
                      ],
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(8, 8, 8, 24),
                child: PortfolioPieChart(wallets: _portfolioWallets, totalValue: _totalValue),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _portfolioWallets.length,
                itemBuilder: (context, i) {
                  return WalletRowWidget(
                    key: Key(_selectedPortfolio.id.toString() + _portfolioWallets[i].coinId),
                    openOnTap: WalletDetailScreen.routeName,
                    walletId: _portfolioWallets[i].id
                  );
                }
              ),
              SizedBox(height: 72) //don't obscure last entry with FAB
            ],
          ),
        ),
      ),
    );
  }

  ///Displays the error message and adds pull to refresh
  Widget _buildError(String text) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _onRefresh,
      child: LayoutBuilder(
        builder: (context, constraints) => ListView( //no single child scrollview to constrain height
          children: [
            Container(
              height: constraints.maxHeight,
              child: Center(
                child: Text(text),
              ),
            )
          ],
        ),
      )
    );
  }

  Future<void> _onRefresh() async {
    setState(() {
      _refreshTriggered = true;
      _marketData.clear();
      _fetchMarketData = _fetchPortfolioMarketData(_portfolioWallets);
    });
  }

  ///Gets the selected portfolio and updates its data
  void _updatePortfolio() {
    _selectedTimePeriod = _settingsDAO.getPortfolioSelectedPeriod();

    _selectedPortfolio = _portfolioDao.getPortfolio(widget.selectedPortfolioId)!;

    _portfolioWallets = _selectedPortfolio.allWallets;
    _currency = _selectedPortfolio.currency;
    List<String> walletCoinIds = _portfolioWallets.map((e) => e.coinId).toList();

    if (_isDataUpdated(_portfolioWallets)) {
      _marketData = _marketDataDao.getMultipleCoinMarketDataForPeriod(walletCoinIds, Constants.MARKETDATAKEYMONTH, _currency);
      _totalValue = _getTotalValue(_portfolioWallets, _marketData);
      _changePercentage = _getChangePercentage(_portfolioWallets, _marketData, _selectedTimePeriod);
    } else {
      _marketData.clear();
      _fetchMarketData = _fetchPortfolioMarketData(_portfolioWallets);
    }
  }

  ///Check if the month market data of the given portfolios is up to date
  bool _isDataUpdated(List<Wallet> wallets) {
    for (int i = 0; i < wallets.length; i++) {
      final data = _marketDataDao.getCoinMarketDataForPeriod(wallets[i].coinId, Constants.MARKETDATAKEYMONTH, _currency);
      //does updated data exist in the database
      if (data?.lastUpdated == null ||
          DateTime.now().difference(data!.lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES ||
          _refreshTriggered
      ) {
        return false;
      }
    }
    return true;
  }

  ///Fetches the (monthly) market data for all wallets in the portfolio from the API,
  ///if there is no up to date monthly market data in the database
  Future<List<MarketData>> _fetchPortfolioMarketData(List<Wallet> wallets) async {
    List<MarketData> walletData = [];
    List<String> walletsToGet = [];

    wallets.forEach((wallet) {
      final data = _marketDataDao.getCoinMarketDataForPeriod(wallet.coinId, Constants.MARKETDATAKEYMONTH, _currency);
      //does updated data exist in the database
      if (data?.lastUpdated == null ||
          DateTime.now().difference(data!.lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES ||
          _refreshTriggered
      ) {
        walletsToGet.add(wallet.coinId);
      }
    });

    List<MarketData> apiData = [];
    if (walletsToGet.length != 0) {
      apiData = await _api.getMultipleMarketDataDays(walletsToGet, _currency, Constants.MARKETDATAKEYMONTH);
    }
    _marketDataDao.insertMultipleMarketData(apiData);

    List<String> portfolioIds = wallets.map((e) => e.coinId).toList();

    walletData = _marketDataDao.getMultipleCoinMarketDataForPeriod(portfolioIds, Constants.MARKETDATAKEYMONTH, _currency);
    for (int i = 0; i < walletData.length; i++) {
      wallets[i].currentCoinPrice = walletData[i].latestPrice;
      wallets[i].save();
    }

    return walletData;
  }

  ///Returns the latest total value of the portfolios with the given market data
  double _getTotalValue(List<Wallet> wallets, List<MarketData> marketData) {
    double totalValue = 0;
    for (int i = 0; i < wallets.length; i++) {
      wallets[i].currentCoinPrice = marketData.where((element) => element.coinId == wallets[i].coinId).toList().first.latestPrice;
      totalValue += wallets[i].totalValue;
    }

    return totalValue;
  }

  ///Returns the total value of the portfolios a number of days ago
  double _totalValueDaysAgo(List<Wallet> wallets, List<MarketData> marketData, int daysAgo) {
    double oldTotalValue = 0;
    DateTime oldDate = DateTime.now().subtract(Duration(days: daysAgo));
    for (int i = 0; i < wallets.length; i++) {
      double priceAtDate = marketData[i].getPricesSinceDate(oldDate).first.price;
      oldTotalValue += wallets[i].totalValueAtDate(oldDate, priceAtDate);
    }

    return oldTotalValue;
  }

  ///Returns the total value change percentage of the portfolios in the period that is selected
  double _getChangePercentage(List<Wallet> wallets, List<MarketData> marketData, TimePeriod period) {
    double currentTotalValue = _getTotalValue(wallets, marketData);
    switch (period) {
      case TimePeriod.day : { //day
        double oldTotalValue = _totalValueDaysAgo(wallets, marketData, 1);
        double changePercentage = 0;
        if (oldTotalValue == 0) { //no transactions at old date
          changePercentage = 100;
        } else {
          changePercentage = ((currentTotalValue - oldTotalValue) / oldTotalValue) * 100;
        }
        return changePercentage;
      }

      case TimePeriod.week : { //week
        double oldTotalValue = _totalValueDaysAgo(wallets, marketData, 7);
        double changePercentage = 0;
        if (oldTotalValue == 0) { //no transactions at old date
          changePercentage = 100;
        } else {
          changePercentage = ((currentTotalValue - oldTotalValue) / oldTotalValue) * 100;
        }
        return changePercentage;
      }

      case TimePeriod.month : { //month
        double oldTotalValue = _totalValueDaysAgo(wallets, marketData, 30);
        double changePercentage = 0;
        if (oldTotalValue == 0) { //no transactions at old date
          changePercentage = 100;
        } else {
          changePercentage = ((currentTotalValue - oldTotalValue) / oldTotalValue) * 100;
        }
        return changePercentage;
      }

      default : {
        return 0;
      }
    }
  }

  ///Calculates the total value of the wallets from the old values saved in the database
  double _getOldTotalValue(List<Wallet> wallets) {
    double oldTotalValue = 0;
    _selectedPortfolio.allWallets.forEach((element) {
      oldTotalValue += element.totalCoinAmount * element.currentCoinPrice;
    });
    return oldTotalValue;
  }

  ///Returns the correct sorting function matching the order and direction values of the widget
  int Function(Wallet, Wallet) _getSortingFunction() {
    switch (widget.order) {
      case WalletOrderBy.totalValueDesc: {
        return CoinComparators.walletTotalValueDesc;
      }

      case WalletOrderBy.totalValueAsc: {
        return CoinComparators.walletTotalValueAsc;
      }

      case WalletOrderBy.coinNameDesc : {
        return CoinComparators.walletCoinNameDesc;
      }

      case WalletOrderBy.coinNameAsc : {
        return CoinComparators.walletCoinNameAsc;

      }
    }
  }

  ///Listener function that is triggered by the portfolio listener
  void _portfolioUpdateListener() {
    if (this.mounted) {
      setState(() { //rebuild the widget
        if (!_refreshTriggered && _portfolioDao.getPortfolio(widget.selectedPortfolioId) != null) { //don't call after deleting portfolio
          _updatePortfolio();
        }
      });
    }
  }

  ///Listener function that is triggered by the wallet listener
  void _walletUpdateListener() {
    if (this.mounted) {
      setState(() { //rebuild the widget
        if (!_refreshTriggered && _portfolioDao.getPortfolio(widget.selectedPortfolioId) != null) { //don't call after deleting portfolio
          _updatePortfolio();
        }
      });
    }
  }
}

///Order of the wallets
enum WalletOrderBy {
  totalValueDesc,
  totalValueAsc,
  coinNameAsc,
  coinNameDesc
}