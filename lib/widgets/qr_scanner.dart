import 'dart:io';

import 'package:crypto_prices/generated/l10n.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

///Uses the camera to scan a QR-Code
class QrScanner extends StatefulWidget {
  QrScanner({Key? key}) : super(key: key);

  @override
  _QrScannerState createState() => _QrScannerState();
}

class _QrScannerState extends State<QrScanner> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;

  TextStyle _normalFont = TextStyle(fontSize: 16.0);

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
            flex: 5,
            child: _buildQrView(context),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: (result != null)
                  ? Text(
                  '${S.of(context).qrScannerBarcodeType} ${describeEnum(result!.format)}   ${S.of(context).qrScannerData} ${result!.code}')
                  : Text(S.of(context).qrScannerScanCode, style: _normalFont),
            ),
          )
        ],
      ),
    );
  }

  ///Builds a QR-Scanner view with a red rectangle overlay
  Widget _buildQrView(BuildContext context) {
    // check how wide or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
        MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;

    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  ///Gets called when the view is created. Listens to the scanner stream
  ///and closes the view with the result code if a code is successfully scanned
  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.first.then((scanData) {
      setState(() {
        result = scanData;
        Navigator.pop(context, result!.code);
      });
    }).catchError(
      (error) {
        print("No code scanned");
      },
      test: (e) => e is StateError
    );
  }

  ///Callback for when the camera permission is set
  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    print('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(S.of(context).qrScannerNoPermission)),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}