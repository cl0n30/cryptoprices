import 'dart:io';

import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/database/coins_dao.dart';
import 'package:crypto_prices/database/favorites_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/coin_detail.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/widgets/coinListScreen/coin_row_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../api/coingecko_api.dart';
import '../../constants.dart';
import '../../database/coin_comparators.dart';
import '../detailScreen/coin_detail_screen.dart';
import '../settingsScreens/rate_limitation_info.dart';

///Displays all favorite coins in a customizable order
class FavoriteList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FavoriteListState();
}

class _FavoriteListState extends State<FavoriteList> {
  late TextStyle _linkFont;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  ApiInteraction api = CoinGeckoAPI();

  late Future<List<Coin>> _favoriteMarketData;

  List<Coin> _favoriteData = [];

  FavoritesDao _favoritesDao = FavoritesDao();

  CoinsDao _coinsDao = CoinsDao();

  SettingsDAO _settingsDAO = SettingsDAO();

  String _currency = "";

  @override
  void initState() {
    super.initState();

    var favorites = _favoritesDao.getAllFavorites()..sort(CoinComparators.favoriteListPosition); //get coins in correct order later from api or DB
    if (favorites.isEmpty) {
      return;
    }

    DateTime coinsLastUpdated = _settingsDAO.getCoinsLastUpdatedHive()!;
    DateTime? favoritesLastUpdated = _settingsDAO.getFavoritesLastUpdatedHive();
    bool currencyChanged = _favoritesDao.getCurrencyChanged();
    _currency = _settingsDAO.getCurrencyHive();

    bool doFavoritesExist = _favoritesExistInCoins(favorites);
    bool coinsUpdated = DateTime.now().difference(coinsLastUpdated).inMinutes < Constants.UPDATETIMERMINUTES;
    bool favoritesUpdated = favoritesLastUpdated != null && DateTime.now().difference(favoritesLastUpdated).inMinutes < Constants.UPDATETIMERMINUTES;

    //use current coin data from coin list if possible
    if (doFavoritesExist && coinsUpdated && !currencyChanged
    || doFavoritesExist && favoritesUpdated && !currencyChanged
    ) {
      _favoriteData = _getExistingFavorites(favorites);

    } else {
      _favoriteMarketData = _fetchFavorites(favorites);
      _favoritesDao.setCurrencyChanged(false);
    }

    //update list if the FavoriteBox changes (adding or deleting)
    _favoritesDao.getAllFavoritesWatcher().listen((event) {
      if (this.mounted) {
        final favorites = _favoritesDao.getAllFavorites()..sort(CoinComparators.favoriteListPosition);
        setState(() {
          if (_favoritesExistInCoins(favorites)) {
            _favoriteData = _getExistingFavorites(favorites);
          } else {
            _refresh();
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 15.0, color: Theme.of(context).colorScheme.secondary);
    if(_favoritesDao.getAllFavorites().isNotEmpty && _favoriteData.isEmpty) {
      return FutureBuilder<List<Coin>>(
        future: _favoriteMarketData,
        builder: (context, snapshot) {
          //snapshot state doesn't change until loading is finished, so when refreshing after error
          //the state will stay 'hasError' and no loading screen will be displayed
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 10),
                  Text(S.of(context).loadingDataMessage)
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            );
          }

          if (snapshot.hasData) {
            if (snapshot.data!.length == 0) { //snapshot.data length can be 0 in some builds during loading
              return Center(child:
                Column(children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 10),
                  Text(S.of(context).loadingDataMessage)
                ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
              );
            }

            _coinsDao.insertAllCoins(snapshot.data!);
            _settingsDAO.setFavoritesLastUpdated(DateTime.now());
            _favoriteData = snapshot.data!;

            return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: _refresh,
                child: ReorderableListView.builder(
                  padding: EdgeInsets.all(8.0),
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, i) {
                    //return _buildRow(coin);
                    return CoinRowWidget(
                      key: Key(_favoriteData[i].id),
                      openOnTap: CoinDetailScreen.routeName,
                      index: (i + 1).toString(),
                      coinData: _favoriteData[i],
                      showFavoriteWidget: true,
                    );
                  },
                  onReorder: (int oldIndex, int newIndex) {
                    setState(() {
                      //update positions of items between old and new position
                      if (oldIndex < newIndex) {
                        newIndex -= 1;
                        _favoritesDao.moveUpAllBetween(oldIndex, newIndex);
                      } else {
                        _favoritesDao.moveDownAllBetween(newIndex, oldIndex);
                      }

                      Coin item = _favoriteData.removeAt(oldIndex);
                      _favoriteData.insert(newIndex, item);

                      var favorite = _favoritesDao.getFavorite(item.id)!;
                      favorite.favoriteListPosition = newIndex;
                      _favoritesDao.insertFavorite(favorite);
                    });
                  },
                )
            );
          } else if (snapshot.hasError) {
            if (snapshot.error is SocketException) {
              return _buildError(S.of(context).errorMessageSocket);
            }

            if (snapshot.error is HttpException) {
              return _buildError(S.of(context).errorMessageHttp);
            }

            if (snapshot.error is RateLimitationException) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: _refresh,
                child: LayoutBuilder(
                  builder: (context, constraints) => ListView( //no single child scrollview to constrain height
                    children: [
                      Container(
                        height: constraints.maxHeight,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                S.of(context).errorMessageRateLimitation,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 10),
                              RichText(
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: S.of(context).moreInformation,
                                  style: _linkFont,
                                  recognizer: TapGestureRecognizer()..onTap = () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context) => RateLimitationInfo())
                                    );
                                  }
                                )
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              );
            }

            return _buildError(S.of(context).error + ": " + snapshot.error.toString());
          }

          return Center(child:
            Column(children: [
                CircularProgressIndicator(),
                SizedBox(height: 10),
                Text(S.of(context).loadingDataMessage)
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          );
        },
      );
    } else if (_favoritesDao.getAllFavorites().isEmpty) {
        return Text(S.of(context).noFavorites);
    } else {
      return RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _refresh,
          child: ReorderableListView.builder(
            padding: EdgeInsets.all(8.0),
            itemCount: _favoriteData.length,
            itemBuilder: (context, i) {
              return CoinRowWidget(
                key: Key(_favoriteData[i].id),
                openOnTap: CoinDetailScreen.routeName,
                index: (i + 1).toString(),
                coinData: _favoriteData[i],
                showFavoriteWidget: true,
              );
            },
            onReorder: (int oldIndex, int newIndex) {
              setState(() {
                if (oldIndex < newIndex) {
                  newIndex -= 1;
                  _favoritesDao.moveUpAllBetween(oldIndex, newIndex);
                } else {
                  _favoritesDao.moveDownAllBetween(newIndex, oldIndex);
                }

                Coin item = _favoriteData.removeAt(oldIndex);
                _favoriteData.insert(newIndex, item);

                var favorite = _favoritesDao.getFavorite(item.id)!;
                favorite.favoriteListPosition = newIndex;
                _favoritesDao.insertFavorite(favorite);
              });
            },
          )
      );
    }
  }

  ///Displays the error message and adds pull to refresh
  Widget _buildError(String text) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _refresh,
      child: LayoutBuilder(
        builder: (context, constraints) => ListView( //no single child scrollview to constrain height
          children: [
            Container(
              height: constraints.maxHeight,
              child: Center(
                child: Text(text),
              ),
            )
          ],
        ),
      )
    );
  }

  ///Force an update of the data from the api
  Future<void> _refresh() async {
    setState(() {
      var favorites = _favoritesDao.getAllFavorites()..sort(CoinComparators.favoriteListPosition);
      _favoriteMarketData = _fetchFavorites(favorites);
      _favoriteData.clear(); //always get network data
    });
  }

  ///Gets the favorite data for the given favorites from the API
  Future<List<Coin>> _fetchFavorites(List<FavoriteCoin> favorites) async {
    final pageEntries = 250;
    final pageNumber = 1;
    _coinsDao.deleteAllEntries();
    List<Coin> coinList = await api.getMarketDataPage( //get first market page to potentially save API calls
        _currency,
        api.getOrderString(OrderBy.market_cap, OrderDirection.desc),
        pageEntries,
        pageNumber
    );

    _coinsDao.insertAllCoins(coinList);
    _settingsDAO.setCoinsLastUpdated(DateTime.now());

    if (_favoritesExistInCoins(favorites)) {
      return _getExistingFavorites(favorites);
    } else {
      final existingCoins = _getExistingFavorites(favorites);
      final existingIds = existingCoins.map((e) => e.id);
      final favoritesToGet = favorites.where((f) => !existingIds.contains(f.id)); //only get coins not in database

      final apiCoins = await api.getMultipleCoinData(favoritesToGet.map((e) {
        return e.id;
      }).toList(), _currency);

      return existingCoins..addAll(apiCoins);
    }
  }

  ///Checks if the coins with the given ids already exist in the database
  bool _favoritesExistInCoins(List<FavoriteCoin> favs) {
    for (int i = 0; i < favs.length; i++) {
      if (_coinsDao.getCoin(favs[i].id) == null) {
        return false;
      }
    }
    return true;
  }

  ///Returns all coins of the given favorites that are already cached in the database
  List<Coin> _getExistingFavorites(List<FavoriteCoin> favorites) {
    List<Coin> coins = [];
    for (int i = 0; i < favorites.length; i++) {
      final coin = _coinsDao.getCoin(favorites[i].id);
      if (coin != null) {
        coins.add(coin);
      }
    }
    return coins;
  }
}