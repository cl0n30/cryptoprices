import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/available_coins_dao.dart';
import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/market_data_dao.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/date_price.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:crypto_prices/util/image_urls.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/charts/wallet_detail_graph.dart';
import 'package:crypto_prices/widgets/private_price_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:tuple/tuple.dart';

import '../../constants.dart';
import '../favoritesScreen/favorite_widget.dart';
import '../portfolioScreen/transaction_edit_widget.dart';
import '../settingsScreens/rate_limitation_info.dart';

class WalletDetailScreen extends StatefulWidget {
  static const routeName = "/walletDetail";
  final String coinId;

  ///Id of the currently selected portfolio
  final int selectedPortfolioId;

  WalletDetailScreen({
    Key? key,
    required this.coinId,
    this.selectedPortfolioId = 0
  }) : super(key: key);

  @override
  _WalletDetailScreenState createState() => _WalletDetailScreenState();
}

class _WalletDetailScreenState extends State<WalletDetailScreen> {
  final _boldFont = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);
  final _smallerFont = TextStyle(fontSize: 16.0);
  late final _smallerFontRed;
  late final _smallerFontGreen;
  final _normalFont = TextStyle(fontSize: 18.0);
  late final _normalFontRed;
  late final _normalFontGreen;
  late final _normalFontGrey;
  late var _linkFont;
  final _headingFont = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
  Color? _green;
  Color? _red;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  bool _disableScrolling = false;
  double _graphTouchedValue = -1;
  double _graphTouchedChange = 0;

  ApiInteraction _api = CoinGeckoAPI();

  PortfolioDao _portfolioDao = PortfolioDao();
  WalletDao _walletDao = WalletDao();
  TransactionDao _transactionDao = TransactionDao();
  MarketDataDao _marketDataDao = MarketDataDao();
  SettingsDAO _settingsDAO = SettingsDAO();

  ///Selected time interval in the graph toggle buttons
  TimePeriod _selectedTimePeriod = TimePeriod.day;

  String _currency = SettingsDAO().getCurrencyHive();

  Portfolio? _portfolio;
  Wallet _walletData = Wallet(0, "", 0);
  List<MarketData> _marketChart = [];
  List<Transaction> _transactions = [];
  List<DateTime> _transactionDates = [];

  late Future<List<MarketData>> _getMarketChart;

  bool _refreshTriggered = false;

  bool _showCoinVolume = false;

  @override
  void initState() {
    super.initState();

    if (currentTheme.isThemeDark()) {
      _smallerFontRed = TextStyle(fontSize: 16.0, color: Constants.priceChangeRedDark);
      _smallerFontGreen = TextStyle(fontSize: 16.0, color: Constants.priceChangeGreenDark);
      _normalFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedDark);
      _normalFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenDark);
      _normalFontGrey = TextStyle(fontSize: 18.0, color: Colors.grey[400]);
      _green = Constants.priceChangeGreenDark;
      _red = Constants.priceChangeRedDark;
    } else {
      _smallerFontRed = TextStyle(fontSize: 16.0, color: Constants.priceChangeRedLight);
      _smallerFontGreen = TextStyle(fontSize: 16.0, color: Constants.priceChangeGreenLight);
      _normalFontRed = TextStyle(fontSize: 18.0, color: Constants.priceChangeRedLight);
      _normalFontGreen = TextStyle(fontSize: 18.0, color: Constants.priceChangeGreenLight);
      _normalFontGrey = TextStyle(fontSize: 18.0, color: Colors.grey[600]);
      _green = Constants.priceChangeGreenLight;
      _red = Constants.priceChangeRedLight;
    }

    _updatePortfolio();

    _transactionDao.getAllTransactionsWatcher().addListener(() {
      if (this.mounted) {
        setState(() {
          final wallet = _portfolio?.getPortfolioWalletByCoinId(widget.coinId);
          if (wallet != null && wallet.allTransactions.isNotEmpty) {
            _walletData = wallet;
            _transactions = _walletData.allTransactions.toList();
            _transactionDates = _getTransactionDates(_transactions)..sort(CoinComparators.mostRecentDateFirst);
          } else {
            final coin = AvailableCoinsDao().getAvailableCoin(widget.coinId)!;
            _walletData = Wallet(
                Util.generateId(),
                widget.coinId,
                widget.selectedPortfolioId,
                coinName: coin.name,
                coinSymbol: coin.symbol
            );
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 18.0, color: Theme.of(context).colorScheme.secondary);

    //different portfolio selected
    if (_portfolio != null && _portfolio!.id != widget.selectedPortfolioId) {
      _updatePortfolio();
    }

    if (!_isDataUpdated(_walletData.coinId)) {
      _getMarketChart = _getHistoricalMarketData(_walletData.coinId, _currency);
      return FutureBuilder<List<MarketData>>(
        future: _getMarketChart,
        builder: (context, snapshot) {
          //snapshot state doesn't change until loading is finished, so when refreshing after error
          //the state will stay 'hasError' and no loading screen will be displayed
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 10),
                  Text(S.of(context).loadingDataMessage)
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            );
          }

          if (snapshot.hasData) {
            _marketChart = snapshot.data!;
            _walletData.currentCoinPrice = _marketChart.first.latestPrice;
            if (_walletData.allTransactions.isNotEmpty) //don't insert empty wallet
              _walletData.save();

            return RefreshIndicator(
              key: _refreshIndicatorKey,
              onRefresh: _refresh,
              child: SingleChildScrollView(
                physics: (_disableScrolling ? NeverScrollableScrollPhysics() : AlwaysScrollableScrollPhysics()),
                child: Column(
                  children: [
                    _buildTopRow(_walletData),
                    WalletDetailGraph(
                      wallet: _walletData,
                      marketChart: _marketChart,
                      onSelectionChange: _onTimePeriodSelectionChange,
                      graphTouched: _onGraphTouched,
                      currency: _currency,
                      displayCoinAmount: _showCoinVolume,
                    ),
                    SizedBox(height: 25),
                    if (_walletData.allTransactions.isNotEmpty)
                      _buildTransactions(_walletData.allTransactions.toList())
                    else
                      _buildNoTransactions(),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            if (snapshot.error is SocketException) {
              return _buildError(S.of(context).errorMessageSocket);
            }

            if (snapshot.error is HttpException) {
              return _buildError(S.of(context).errorMessageHttp);
            }

            if (snapshot.error is RateLimitationException) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: _refresh,
                child: LayoutBuilder(
                  builder: (context, constraints) => ListView( //no single child scrollview to constrain height
                    children: [
                      Container(
                        height: constraints.maxHeight,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                S.of(context).errorMessageRateLimitation,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 10),
                              RichText(
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: S.of(context).moreInformation,
                                  style: _linkFont,
                                  recognizer: TapGestureRecognizer()..onTap = () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context) => RateLimitationInfo())
                                    );
                                  }
                                )
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              );
            }

            return _buildError(S.of(context).error + ": " + snapshot.error.toString());
          }

          return Center(
            child: Column(
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 10),
                Text(S.of(context).loadingDataMessage)
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          );
        }
      );
    } else {
      _walletData.currentCoinPrice = _marketChart.first.latestPrice;
      if (_walletData.allTransactions.isNotEmpty) //don't insert empty wallet
        _walletData.save();
      return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: SingleChildScrollView(
          physics: (_disableScrolling ? NeverScrollableScrollPhysics() : AlwaysScrollableScrollPhysics()),
          child: Column(
            children: [
              _buildTopRow(_walletData),
              WalletDetailGraph(
                wallet: _walletData,
                marketChart: _marketChart,
                onSelectionChange: _onTimePeriodSelectionChange,
                graphTouched: _onGraphTouched,
                currency: _currency,
                displayCoinAmount: _showCoinVolume,
              ),
              SizedBox(height: 25),
              if (_walletData.allTransactions.isNotEmpty)
                _buildTransactions(_walletData.allTransactions.toList())
              else
                _buildNoTransactions(),

            ],
          ),
        ),
      );
    }
  }

  ///Return the top row of the widget with the coin name, logo, price
  Widget _buildTopRow(Wallet wallet) {
    return Container(
      margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 16.0),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.all(8.0),
            child: CachedNetworkImage(
              imageUrl: ImageUrls().getLargeImageUrl(wallet.coinId),
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error_outline),
              height: 50.0,
              width: 50.0,
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    margin: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            wallet.coinName,
                            style: _boldFont
                        ),
                        SizedBox(height: 10),
                        Text(
                            wallet.coinSymbol.toUpperCase(),
                            style: _smallerFont
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      _buildAmountText(wallet),
                      SizedBox(height: 10),
                      PrivatePriceText(
                        price: (_graphTouchedValue != -1)
                            ? Util.formatPercentage(_graphTouchedChange)
                            : Util.formatPercentage(_getPriceChangePercent(wallet, _selectedTimePeriod)),
                        symbol: "%",
                        percentage: true,
                        style: ((_graphTouchedValue != -1
                            ? _graphTouchedChange
                            : _getPriceChangePercent(wallet, _selectedTimePeriod)).isNegative ? _smallerFontRed : _smallerFontGreen
                        )
                      ),
                    ],
                  ),
                )
              ],
            )
          ),
          FavoriteWidget(coin: Coin(wallet.coinId, name: wallet.coinName, symbol: wallet.coinSymbol))
        ],
      ),
    );
  }

  ///Returns a tappable text that displays either coin value or amount and copies it to the clipboard on long press
  Widget _buildAmountText(Wallet wallet) {
    if (_showCoinVolume) {
      return GestureDetector(
        child: PrivatePriceText(
          price: (_graphTouchedValue != -1
            ? Util.formatPrice(_graphTouchedValue, twoDecimalsAbove: 1)
            : Util.formatPrice(wallet.totalCoinAmount, twoDecimalsAbove: 1)
          ),
          symbol: wallet.coinSymbol.toUpperCase(),
          style: _normalFont,
        ),
        onTap: () {
          setState(() {
            _showCoinVolume = !_showCoinVolume;
          });
        },
        onLongPress: () {
          if (_settingsDAO.getPrivateMode()) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).notAvailableInPrivateMode),
                duration: Duration(seconds: 2),
              ),
            );
          } else {
            Clipboard.setData(ClipboardData(text: wallet.totalCoinAmount.toString()));
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).coinAmountCopied(wallet.coinSymbol.toUpperCase())),
                duration: Duration(seconds: 2),
              ),
            );
          }
        },
      );
    } else {
      return GestureDetector(
        child: PrivatePriceText(
          price: (_graphTouchedValue != -1
              ? Util.formatPrice(_graphTouchedValue, twoDecimalsAbove: 1)
              : Util.formatPrice(wallet.totalValue, twoDecimalsAbove: 1)
          ),
          symbol: Constants.currencySymbols[_currency]!,
          style: _normalFont,
        ),
        onTap: () {
          setState(() {
            _showCoinVolume = !_showCoinVolume;
          });
        },
        onLongPress: () {
          if (_settingsDAO.getPrivateMode()) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).notAvailableInPrivateMode),
                duration: Duration(seconds: 2),
              ),
            );
          } else {
            Clipboard.setData(ClipboardData(text: wallet.totalValue.toString()));
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(S.of(context).valueCopied),
                duration: Duration(seconds: 2),
              ),
            );
          }
        },
      );
    }
  }

  ///Display the given transactions
  Widget _buildTransactions(List<Transaction> transactions) {
    return Column(
      children: [
        Center(child: Text(S.of(context).walletDetailTransactions, style: _normalFont)),
        Container(
          padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
          child: ListView.builder(
            padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _transactionDates.length,
            itemBuilder: (context, i) {
              return _buildTransactionBlock(_transactionDates[i]);
            }
          )
        ),
      ],
    );
  }

  ///Build one transaction block for the given date
  Widget _buildTransactionBlock(DateTime date) {
    List<Transaction> transactionsOnDate = _transactions.where((element) => element.transactionDate!.isSameDate(date)).toList();
    transactionsOnDate.sort(CoinComparators.transactionDateMostRecentFirst);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(8),
          child: Text(date.getDateOnly(), style: _normalFontGrey,),
        ),
        ListView.builder(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: transactionsOnDate.length,
          itemBuilder: (context, i) {
            return _buildTransactionRow(transactionsOnDate[i]);
          }
        )
      ],
    );
  }

  ///Build one transaction in a single row
  Widget _buildTransactionRow(Transaction transaction) {
    String nameText = "";
    if (transaction.transactionName.isEmpty) {
      if (transaction.coinVolume.isNegative)
        nameText = S.of(context).transactionSent;
      else
        nameText = S.of(context).transactionReceived;
    } else {
      nameText = transaction.transactionName;
    }

    double coinValue = 0;
    String currencySymbol = "";
    if (!_settingsDAO.getDisplayInPortfolioCurrency()) {
      //use own entered coin price or actual price on transaction date
      if (_settingsDAO.getUseOwnCoinPriceHive()) {
        coinValue = transaction.coinValue;
      } else {
        coinValue = transaction.actualValue;
      }
      currencySymbol = Constants.currencySymbols[transaction.currency]!;
    } else {
      //use coin price from portfolio/wallet to display the transactions
      coinValue = transaction.coinVolume * _walletData.currentCoinPrice;
      currencySymbol = Constants.currencySymbols[_portfolio!.currency]!;
    }

    String coinVolumeText = "";
    String coinValueText = "";
    if (transaction.coinVolume.isNegative) {
      coinVolumeText = Util.formatPrice(transaction.coinVolume);
      coinValueText = Util.formatPrice(coinValue, twoDecimalsAbove: 1);
    } else {
      coinVolumeText = "+${(Util.formatPrice(transaction.coinVolume))}";
      coinValueText = "+${(Util.formatPrice(coinValue, twoDecimalsAbove: 1))}";
    }

    return InkWell(
      onTap: () {
        if (_settingsDAO.getPrivateMode()) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(S.of(context).notAvailableInPrivateMode),
              duration: Duration(seconds: 2),
            ),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => TransactionEditWidget(transactionId: transaction.id, selectedPortfolioId: widget.selectedPortfolioId)
            )
          );
        }
      },
      onLongPress: () {
        if (_settingsDAO.getPrivateMode()) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(S.of(context).notAvailableInPrivateMode),
              duration: Duration(seconds: 2),
            ),
          );
        } else {
          Clipboard.setData(ClipboardData(text: transaction.coinVolume.toString()));
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(S.of(context).coinAmountCopied(transaction.coinSymbol.toUpperCase())),
              duration: Duration(seconds: 2),
            ),
          );
        }
      },
      child: Card(
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.all(8.0),
                child: Icon(
                  (transaction.coinVolume.isNegative ? Icons.upload : Icons.download),
                  size: 35,
                  color: (transaction.coinVolume.isNegative ? _red : _green),
                )
            ),
            Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                nameText,
                                style: _normalFont
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          PrivatePriceText(
                            price: coinVolumeText,
                            symbol: transaction.coinSymbol.toUpperCase(),
                            style: (transaction.coinVolume.isNegative ? _normalFontRed : _normalFontGreen)
                          ),
                          SizedBox(height: 10),
                          PrivatePriceText(
                            price: coinValueText,
                            symbol: currencySymbol,
                            style: _smallerFont
                          ),
                        ],
                      ),
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }

  ///Builds a message for no transactions and a button to add one
  Widget _buildNoTransactions() {
    return Column(
      children: [
        Text(S.of(context).noWalletTransactions, style: _normalFont),
        SizedBox(height: 10),
        TextButton(
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => TransactionEditWidget(
                    transactionId: 0,
                    coinId: widget.coinId,
                    selectedPortfolioId: widget.selectedPortfolioId,
                  )
              )
            );
          },
          child: Text("+ ${S.of(context).addTransaction}", style: _linkFont)
        )
      ],
    );
  }

  ///Displays the error message and adds pull to refresh
  Widget _buildError(String text) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _refresh,
      child: LayoutBuilder(
        builder: (context, constraints) => ListView( //no single child scrollview to constrain height
          children: [
            Container(
              height: constraints.maxHeight,
              child: Center(
                child: Text(text),
              ),
            )
          ],
        ),
      )
    );
  }

  Future<void> _refresh() async {
    setState(() {
      _refreshTriggered = true;
    });
  }

  ///Gets the selected portfolio and updates its data
  void _updatePortfolio() {
    _portfolio = _portfolioDao.getPortfolio(widget.selectedPortfolioId);

    if (_portfolio?.getPortfolioWalletByCoinId(widget.coinId) != null) {
      _currency = _portfolio!.currency;
      _walletData = _portfolio!.getPortfolioWalletByCoinId(widget.coinId)!;
      _transactions = _walletData.allTransactions.toList();
      _transactionDates = _getTransactionDates(_transactions)..sort(CoinComparators.mostRecentDateFirst);
    } else {
      final coin = AvailableCoinsDao().getAvailableCoin(widget.coinId)!;
      _walletData = Wallet(
          Util.generateId(),
          widget.coinId,
          widget.selectedPortfolioId,
          coinName: coin.name,
          coinSymbol: coin.symbol
      );
    }

    if (_isDataUpdated(_walletData.coinId)) {
      _marketChart = _marketDataDao.getCoinMarketChart(_walletData.coinId, _currency);
      _walletData.currentCoinPrice = _marketChart.first.latestPrice;
      if (_walletData.allTransactions.isNotEmpty) //don't insert empty wallet
        _walletData.save();
    }
  }

  ///Change the selected time period
  void _onTimePeriodSelectionChange(TimePeriod newValue) {
    setState(() {
      _selectedTimePeriod = newValue;
    });
  }

  ///Disable scrolling when the graph is touched and display the price
  ///and change percentage values of the touched point on the graph
  void _onGraphTouched(Tuple2<double, double> priceAndChange) {
    setState(() {
      _disableScrolling = priceAndChange.item1 != -1;
      _graphTouchedValue = priceAndChange.item1;
      _graphTouchedChange = priceAndChange.item2;
    });
  }

  ///Check if the market chart for the given coin is up to date
  bool _isDataUpdated(String coinId) {
    for (int i = 0; i < marketDataDays.length; i++) {
      final data = _marketDataDao.getCoinMarketDataForPeriod(coinId, marketDataDays[i], _currency);
      //does updated data exist in the database
      if (data?.lastUpdated == null ||
          DateTime.now().difference(data!.lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES ||
          _refreshTriggered
      ) {
        return false;
      }
    }

    return true;
  }

  ///Returns the market chart for the given coin. If the existing database data is not up to date, get new data from API
  Future<List<MarketData>> _getHistoricalMarketData(String coinId, String currency) async {
    try {
      List<MarketData> chart = [];
      List<String> timePeriodsToGet = [];
      marketDataDays.forEach((period) {
        final data = _marketDataDao.getCoinMarketDataForPeriod(coinId, period, currency);
        //does updated data exist in the database
        if (data?.lastUpdated == null ||
            DateTime.now().difference(data!.lastUpdated!).inMinutes > Constants.UPDATETIMERMINUTES ||
            _refreshTriggered
        ) {
          timePeriodsToGet.add(period);
        }
      });

      _refreshTriggered = false;

      if (timePeriodsToGet.length == marketDataDays.length) { //get all data from API
        chart = await _api.getHistoricalMarketChart(coinId, currency);
        _marketDataDao.insertMultipleMarketData(chart);
        return chart;
      }

      List<MarketData> apiData = [];
      for (int i = 0; i < timePeriodsToGet.length; i++) { //get only necessary data from API
        final data = await _api.getMarketDataDays(coinId, currency, timePeriodsToGet[i]);
        apiData.add(data);
      }
      _marketDataDao.insertMultipleMarketData(apiData);

      chart = _marketDataDao.getCoinMarketChart(coinId, currency); //get chart in the correct order

      return chart;
    }
    on SocketException {
      throw SocketException("No internet connection");
    }
    on HttpException {
      throw HttpException("Service is unavailable");
    }
    on RateLimitationException {
      throw RateLimitationException("Too many requests");
    }
  }

  ///Get the correct value change percentage value for the selected time period
  double _getPriceChangePercent(Wallet wallet, TimePeriod period) { //period selected by toggle buttons
    switch (period) {
      case TimePeriod.hour : { //hour
        final oldDate = DateTime.now().subtract(Duration(hours: 1));
        if (_showCoinVolume) {
          final oldAmount = wallet.totalAmountAtDate(oldDate);
          return _calculateValueChangePercentage(wallet.totalCoinAmount, oldAmount);
        }

        var prices = _marketChart[0].getPricesSinceDate(oldDate);
        return _calculateValueChangePercentage(
          wallet.totalValueAtDate(DateTime.now(), _marketChart[0].latestPrice),
          wallet.totalValueAtDate(prices.first.date, prices.first.price) //oldest value in market data
        );
      }

      case TimePeriod.day : { //day
        final oldDate = DateTime.now().subtract(Duration(days: 1));
        if (_showCoinVolume) {
          final oldAmount = wallet.totalAmountAtDate(oldDate);
          return _calculateValueChangePercentage(wallet.totalCoinAmount, oldAmount);
        }

        final prices = _marketChart[0].getPricesSinceDate(oldDate);
        return _calculateValueChangePercentage(
          wallet.totalValueAtDate(DateTime.now(), _marketChart[0].latestPrice),
          wallet.totalValueAtDate(prices.first.date, prices.first.price)
        );
      }

      case TimePeriod.week : { //week
        final oldDate = DateTime.now().subtract(Duration(days: 7));
        if (_showCoinVolume) {
          final oldAmount = wallet.totalAmountAtDate(oldDate);
          return _calculateValueChangePercentage(wallet.totalCoinAmount, oldAmount);
        }

        final prices = _marketChart[1].getPricesSinceDate(oldDate);
        return _calculateValueChangePercentage(
          wallet.totalValueAtDate(DateTime.now(), _marketChart[1].latestPrice),
          wallet.totalValueAtDate(prices.first.date, prices.first.price)
        );
      }

      case TimePeriod.month : { //month
        final oldDate = DateTime.now().subtract(Duration(days: 30));
        if (_showCoinVolume) {
          final oldAmount = wallet.totalAmountAtDate(oldDate);
          return _calculateValueChangePercentage(wallet.totalCoinAmount, oldAmount);
        }

        final prices = _marketChart[1].getPricesSinceDate(oldDate);
        return _calculateValueChangePercentage(
          wallet.totalValueAtDate(DateTime.now(), _marketChart[1].latestPrice),
          wallet.totalValueAtDate(prices.first.date, prices.first.price)
        );
      }

      case TimePeriod.year : { //year
        final oldDate = DateTime.now().subtract(Duration(days: 365));
        if (_showCoinVolume) {
          final oldAmount = wallet.totalAmountAtDate(oldDate);
          return _calculateValueChangePercentage(wallet.totalCoinAmount, oldAmount);
        }

        final prices = _marketChart[2].getPricesSinceDate(oldDate);
        return _calculateValueChangePercentage(
          wallet.totalValueAtDate(DateTime.now(), _marketChart[2].latestPrice),
          wallet.totalValueAtDate(prices.first.date, prices.first.price)
        );
      }

      case TimePeriod.max : { //max
        if (wallet.firstTransactionDate == null)
          return 0;

        MarketData data;
        //use different chart depending on when the first transaction was
        if (DateTime.now().difference(wallet.firstTransactionDate!).inDays < 1) { //less than one day of data
          data = _marketChart[0];
        } else if (DateTime.now().difference(wallet.firstTransactionDate!).inDays < 30) { //less than 30 days of data
          data = _marketChart[1];
        } else {
          data =  _marketChart[2]; //max
        }

        if (_showCoinVolume) {
          final oldAmount = wallet.allTransactions.first.coinVolume;
          return _calculateValueChangePercentage(wallet.totalCoinAmount, oldAmount);
        }

        final prices = data.getPricesSinceDate(wallet.firstTransactionDate!);
        return _calculateValueChangePercentage(
          wallet.totalValueAtDate(DateTime.now(), prices.last.price),
          wallet.totalValueAtDate(prices.first.date, prices.first.price)
        );
      }

      default : {
        return 0;
      }
    }
  }

  ///Returns the change percentage between the first value and the last value
  double _calculateValueChangePercentage(double lastValue, double firstValue) {
    if (firstValue == 0)
      if (lastValue == 0)
        return 0;
      else
        return 100;

    return ((lastValue - firstValue) / firstValue) * 100;
  }

  ///Returns all distinct transaction dates from the given list of transactions
  List<DateTime> _getTransactionDates(List<Transaction> transactions) {
    List<DateTime> list = [];
    for (int i = 0; i < transactions.length; i++) {
      if (!list.contains(transactions[i].transactionDate!.toDate())) {
        list.add(transactions[i].transactionDate!.toDate());
      }
    }
    return list;
  }
}