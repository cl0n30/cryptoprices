import 'dart:io';

import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/api/coingecko_api.dart';
import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/coins_dao.dart';
import 'package:crypto_prices/database/global_market_info_dao.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/generated/l10n.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/global_market_info.dart';
import 'package:crypto_prices/util/rate_limitation_exception.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:crypto_prices/widgets/coinListScreen/coin_row_widget.dart';
import 'package:crypto_prices/widgets/detailScreen/coin_detail_screen.dart';
import 'package:crypto_prices/widgets/global_market_info_widget.dart';
import 'package:crypto_prices/widgets/settingsScreens/rate_limitation_info.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../../constants.dart';

///Displays all coins in a specific order
class CoinList extends StatefulWidget {
  CoinList({Key? key, required this.order, required this.orderDirection}) : super(key: key);

  ///The order of the coins e.g. by name, market cap etc.
  final OrderBy order;

  ///The order direction of the coins e.g. ascending, descending
  final OrderDirection orderDirection;

  @override
  _CoinListState createState() => _CoinListState();
}

class _CoinListState extends State<CoinList> {
  late TextStyle _linkFont;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  ApiInteraction api = CoinGeckoAPI();

  late Future<List<Coin>> marketData;
  CoinsDao _coinsDao = CoinsDao();

  GlobalMarketInfoDao _globalMarketInfoDao = GlobalMarketInfoDao();

  SettingsDAO _settingsDAO = SettingsDAO();

  List<Coin> _coinData = [];

  GlobalMarketInfo _globalMarketInfo = GlobalMarketInfo(0);

  ///The next page to load from the api
  int _page = 1;

  ///Number of entries to load per page
  int _entriesPerPage = 250;

  ///Is loading data from the api
  bool _isLoading = false;

  ///does api return an error
  bool _error = false;

  ///Exception returned by api
  Exception _exception = Exception();

  String _currency = SettingsDAO().getCurrencyHive();

  @override
  void initState() {
    DateTime? lastUpdated = _settingsDAO.getCoinsLastUpdatedHive();
    bool currencyChanged = _coinsDao.getCurrencyChanged();

    //update data only after some time
    if (lastUpdated == null || DateTime.now().difference(lastUpdated).inMinutes > Constants.UPDATETIMERMINUTES || currencyChanged) {
      _coinsDao.deleteAllEntries();
      _buildDataPage();
      _coinsDao.setCurrencyChanged(false);
    } else {
      //get from database
      _coinData = _coinsDao.getAllCoins();
      _globalMarketInfo = _globalMarketInfoDao.getGlobalMarketInfo() ?? GlobalMarketInfo(0);
      _page = (_coinData.length ~/ 250) + 1; //next page if database is not empty
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _linkFont = TextStyle(fontSize: 15.0, color: Theme.of(context).colorScheme.secondary);
    if (_error) {
      if (_exception is SocketException) {
        return _buildError(S.of(context).errorMessageSocket);
      }

      if (_exception is HttpException) {
        return _buildError(S.of(context).errorMessageHttp);
      }

      if (_exception is RateLimitationException) {
        return RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _refresh,
          child: LayoutBuilder(
            builder: (context, constraints) => ListView( //no single child scrollview to constrain height
              children: [
                Container(
                  height: constraints.maxHeight,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          S.of(context).errorMessageRateLimitation,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 10),
                        RichText(
                          overflow: TextOverflow.ellipsis,
                          text: TextSpan(
                            text: S.of(context).moreInformation,
                            style: _linkFont,
                            recognizer: TapGestureRecognizer()..onTap = () {
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (context) => RateLimitationInfo())
                              );
                            }
                          )
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        );
      }

      return _buildError(S.of(context).error + ": " + _exception.toString());
    }
    
    _coinData.sort(_getSortingFunction());
    return LazyLoadScrollView(
      child: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: (_isLoading && _page == 1) ? //only show loading indicator when loading the first page
          Center(
            child: Column(children: [
              CircularProgressIndicator(),
              SizedBox(height: 10),
              Text(S.of(context).loadingDataMessage)
            ],
              mainAxisAlignment: MainAxisAlignment.center,
            )
          ) :
          ListView.builder(
            padding: EdgeInsets.all(8.0),
            itemCount: _coinData.length,
            itemBuilder: (context, i) {
              //return _buildRow(coin);
              if (i == 0) {
                return _buildGlobalInfo();
              }
              return CoinRowWidget(
                key: Key(_coinData[i - 1].id),
                openOnTap: CoinDetailScreen.routeName,
                index: (i).toString(),
                coinData: _coinData[i - 1],
                showFavoriteWidget: true,
              );
            },
          )
      ),
      onEndOfPage: _buildDataPage,
      isLoading: _isLoading,
      scrollOffset: 1000,
    );
  }

  ///Displays a list tile with global market info
  Widget _buildGlobalInfo() {
    String text = "";
    if (widget.order == OrderBy.volume) {
      text = S.of(context).globalVolume(
        Util.formatInt(_globalMarketInfo.total24HVolume.round()),
        Constants.currencySymbols[_settingsDAO.getCurrencyHive()]!
      );
    } else {
      text = S.of(context).globalMarketCap(
        Util.formatInt(_globalMarketInfo.totalMarketCap.round()),
        Constants.currencySymbols[_settingsDAO.getCurrencyHive()]!
      );
    }
    return Container(
      child: ListTile(
        title: Text(text),
        trailing: Icon(Icons.chevron_right),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => GlobalMarketInfoWidget()));
        },
      ),
      padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
    );
  }

  ///Displays the error message and adds pull to refresh
  Widget _buildError(String text) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _refresh,
      child: LayoutBuilder(
        builder: (context, constraints) => ListView( //no single child scrollview to constrain height
          children: [
            Container(
              height: constraints.maxHeight,
              child: Center(
                child: Text(text),
              ),
            )
          ],
        ),
      )
    );
  }

  ///Force an update of the data from the api
  Future<void> _refresh() async {
    setState(() {
      _page = 1;
      _coinData.clear();
      _coinsDao.deleteAllEntries();
      _buildDataPage();
    });
  }

  ///Loads one page of coins from the API and saves them to the database and the local coinData list
  Future _buildDataPage() async {
    setState(() {
      _isLoading = true;
      _error = false;
    });

    try {
      List<Coin> coinList = await api.getMarketDataPage(
          _currency,
          api.getOrderString(OrderBy.market_cap, OrderDirection.desc),
          _entriesPerPage,
          _page
      );

      _coinsDao.insertAllCoins(coinList);
      _settingsDAO.setCoinsLastUpdated(DateTime.now());

      GlobalMarketInfo globalMarketInfo = await api.getGlobalMarketInfo(_currency);
      _globalMarketInfoDao.insertGlobalMarketInfo(globalMarketInfo);
      _settingsDAO.setGlobalMarketInfoLastUpdated(DateTime.now());

      setState(() {
        _isLoading = false;
        _page++;
        _coinData = _coinsDao.getAllCoins();
        _globalMarketInfo = globalMarketInfo;
      });
    }
    on Exception catch(e) {
      setState(() {
        _isLoading = false;
        _error = true;
        _exception = e;
      });
    }
  }

  ///Returns the correct sorting function matching the order and direction values of the widget
  int Function(Coin, Coin) _getSortingFunction() {
    switch (widget.order) {
      case OrderBy.market_cap : {
        if (widget.orderDirection == OrderDirection.asc) {
          return CoinComparators.marketCapAsc;
        }
        return CoinComparators.marketCapDesc;
      }
      case OrderBy.volume : {
        if (widget.orderDirection == OrderDirection.asc) {
          return CoinComparators.volumeAsc;
        }
        return CoinComparators.volumeDesc;
      }
      case OrderBy.price_change : {
        if (widget.orderDirection == OrderDirection.asc) {
          return CoinComparators.priceChangeAsc;
        }
        return CoinComparators.priceChangeDesc;
      }
      case OrderBy.name : {
        if (widget.orderDirection == OrderDirection.asc) {
          return CoinComparators.nameAsc;
        }
        return CoinComparators.nameDesc;
      }
      case OrderBy.symbol : {
        if (widget.orderDirection == OrderDirection.asc) {
          return CoinComparators.symbolAsc;
        }
        return CoinComparators.symbolDesc;
      }
    }
  }
}