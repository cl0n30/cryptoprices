import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/settings_dao.dart';
import 'package:crypto_prices/models/date_price.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:crypto_prices/util/util.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:tuple/tuple.dart';

import '../../generated/l10n.dart';

///Displays a graph from coin market data
class CoinDetailGraph extends StatefulWidget {
  final List<MarketData> marketChart;

  ///Notify the parent that the selected time interval has been changed
  final ValueChanged<TimePeriod> onSelectionChange;

  ///Notify the parent when the graph is being touched to disable scrolling
  ///and to return the price value and the change percentage of the currently
  ///touched point compared to the first point in the time period.
  ///Returns -1 as price when the graph is not touched
  final ValueChanged<Tuple2<double, double>> graphTouched;

  CoinDetailGraph({Key? key, required this.marketChart, required this.onSelectionChange, required this.graphTouched});

  @override
  _CoinDetailGraphState createState() => _CoinDetailGraphState();

}

class _CoinDetailGraphState extends State<CoinDetailGraph> {
  final _boldText = TextStyle(fontWeight: FontWeight.bold);
  late TextStyle _graphText;
  final _graphTextBlackBold = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);

  List<bool> _isSelected = [false, true, false, false, false ,false];

  ///Time periods ["hour", "day", "week", "month", "year", "max"]
  TimePeriod _isSelectedPeriod = TimePeriod.day;

  ///space on the sides of the graph
  final double _graphSidesReservedSize = 22;

  ///space between the graph and the min/max value
  final _minMaxTextPadding = 6.0;

  ///color of the graph
  late Color _graphColor;

  ///text shown above the graph when touching a point on the graph
  String _selectedGraphPoint = "";

  ///is the graph being touched
  bool _touchDown = false;

  SettingsDAO _settingsDAO = SettingsDAO();

  @override
  void initState() {
    super.initState();
    _graphText = currentTheme.isThemeDark()
        ? TextStyle(color: Colors.grey[400], fontSize: 16)
        : TextStyle(color: Colors.grey[600], fontSize: 16);

    _graphColor = currentTheme.isThemeDark()
        ? Constants.accentColorDark!
        : Constants.accentColorLight!;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              _selectedGraphPoint,
              style: _graphTextBlackBold,
            )
          ],
        ),
        AspectRatio(
          aspectRatio: 16/14,
          child: Padding(
            padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
            child: (_isDataAvailable(widget.marketChart))
              ? Listener(
                  child: LineChart(
                    mainData(widget.marketChart),
                  ),
                  onPointerDown: (pointerDownEvent) {
                    setState(() {
                      _touchDown = true;
                    });
                  },
                  //check for pointer up even if touch moves outside the graph or between the graph data points (hourly data)
                  onPointerUp: (pointerUpEvent) {
                    setState(() {
                      _touchDown = false;
                      widget.graphTouched(Tuple2<double, double>(-1, 0));
                      _selectedGraphPoint = "";
                    });
                  },
              )
              : Align(
                  alignment: Alignment.center,
                  child: Text(S.of(context).errorNoDataAvailable),
              )
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ToggleButtons(
                children: [
                  Text(
                    S.of(context).graphToggleHour,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleDay,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleWeek,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleMonth,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleYear,
                    style: _boldText,
                  ),
                  Text(
                    S.of(context).graphToggleMax,
                    style: _boldText,
                  ),
                ],
                fillColor: Theme.of(context).colorScheme.secondary.withOpacity(0.2),
                selectedColor: Theme.of(context).textTheme.bodyText2!.color,
                onPressed: _onDaysSelectionButtonPressed,
                isSelected: _isSelected
            )
          ],
        )
      ],
    );
  }

  ///Changes the selected time interval based on which toggle button is pressed
  void _onDaysSelectionButtonPressed(int buttonIndex) {
    setState(() {
      _isSelectedPeriod = TimePeriod.values[buttonIndex];
      widget.onSelectionChange(_isSelectedPeriod);
      for (int i = 0; i < _isSelected.length; i++) {
        if (i == buttonIndex) {
          _isSelected[i] = true;
        } else {
          _isSelected[i] = false;
        }
      }
    });
  }

  ///Creates the data for the graph
  LineChartData mainData(List<MarketData> marketChart) {
    MarketData marketData = _getCurrentMarketData(marketChart);

    var filteredData = _filterData(marketData);
    var flSpots = _getFlSpots(filteredData);

    //get index of highest and lowest value in the data list
    final highestPrice = _getHighestPrice(filteredData);
    final highestIndex = _correctPriceDisplayPosition(
        highestPrice,
        filteredData.indexWhere((element) => element.price == highestPrice),
        filteredData.length
    );

    final lowestPrice = _getLowestPrice(filteredData);
    final lowestIndex = _correctPriceDisplayPosition(
        lowestPrice,
        filteredData.indexWhere((element) => element.price == lowestPrice),
        filteredData.length
    );

    return LineChartData(
        gridData: FlGridData(
          show: false,
        ),
        titlesData: FlTitlesData(
          show: true,
          topTitles: AxisTitles(
            sideTitles: SideTitles(
              showTitles: true,
              reservedSize: _graphText.fontSize! + _minMaxTextPadding,
              interval: (highestIndex.toDouble() == 0) ? 1 : highestIndex.toDouble(),
              getTitlesWidget: (value, titleMeta) {
                if (!_touchDown && value == highestIndex)
                  return Text(
                    "${Util.formatPrice(highestPrice)} ${Constants.currencySymbols[_settingsDAO.getCurrencyHive()]!}",
                    style: _graphText,
                  );
                else
                  return Text('');
              },
            )
          ),
          bottomTitles: AxisTitles(
            sideTitles: (!_touchDown
                ? SideTitles(
                    showTitles: true,
                    reservedSize: _graphText.fontSize! + _minMaxTextPadding,
                    interval: (lowestIndex.toDouble() == 0) ? 1 : lowestIndex.toDouble(),
                    getTitlesWidget: (value, titleMeta) {
                      if (!_touchDown && value == lowestIndex)
                        return Padding(
                          padding: EdgeInsets.only(top: _minMaxTextPadding),
                          child: Text(
                            "${Util.formatPrice(lowestPrice)} ${Constants.currencySymbols[_settingsDAO.getCurrencyHive()]!}",
                            style: _graphText,
                          ),
                        );
                      else
                        return Text('');
                    },
                )
                : SideTitles(
                    showTitles: true,
                    reservedSize: _graphText.fontSize! + _minMaxTextPadding,
                    interval: 1,
                    getTitlesWidget: (value, titleMeta) {
                      if (value == 0
                          || value == filteredData.length ~/ 4
                          || value == filteredData.length ~/ 2
                          || value == 3 * (filteredData.length ~/ 4)
                          || value == filteredData.length - 1
                      )
                        return Padding(
                          padding: EdgeInsets.only(top: _minMaxTextPadding),
                          child: Text(
                            _getSelectedDate(filteredData, value.toInt()),
                            style: _graphText,
                          ),
                        );
                      else
                        return Text('');
                    },
                )
            )
          ),
          leftTitles: AxisTitles(
            sideTitles: SideTitles(
              showTitles: true,
              reservedSize: _graphSidesReservedSize,
              getTitlesWidget: (value, titleMeta) {
                return Text('');
              },
            )
          ),
          rightTitles: AxisTitles(
            sideTitles: SideTitles(
              showTitles: true,
              reservedSize: _graphSidesReservedSize,
              getTitlesWidget: (value, titleMeta) {
                return Text('');
              },
            )
          ),
        ),
        borderData:
        FlBorderData(show: false, border: Border(bottom: BorderSide(color: const Color(0xff37434d), width: 1), left: BorderSide(color: const Color(0xff37434d), width: 1))),
        minX: 0,
        maxX: filteredData.length.toDouble() - 1,
        minY: lowestPrice, //get price from filtered data instead of marketData to avoid using prices that have been filtered out
        maxY: highestPrice,
        lineBarsData: [
          LineChartBarData(
            spots: flSpots,
            color: _graphColor,
            barWidth: 2,
            isStrokeCapRound: true,
            dotData: FlDotData(
              show: false,
            ),
            belowBarData: BarAreaData(
              show: true,
              color: _graphColor.withOpacity(0.3),
            ),
          ),
        ],
        lineTouchData: LineTouchData(
            touchTooltipData: LineTouchTooltipData( //don't show tooltip
              tooltipBgColor: Colors.white.withAlpha(0),
              getTooltipItems: (lineBarSpots) {
                return [
                  LineTooltipItem("", TextStyle())
                ];
              }
            ),
            touchCallback: (FlTouchEvent event, LineTouchResponse? touchResponse) {
              Tuple2<double, double> priceAndChange = Tuple2(-1, 0);

              if (touchResponse?.lineBarSpots != null) {
                DateTime date = filteredData[touchResponse!.lineBarSpots!.first.x.toInt()].date;
                double price = filteredData[touchResponse.lineBarSpots!.first.x.toInt()].price;
                priceAndChange = priceAndChange.withItem1(price);

                setState(() {
                  //display date of the current point
                  _selectedGraphPoint = date.localeFormat();
                  double changePercentage = _calculatePriceChangePercentage(price, filteredData);
                  //disable scrolling and return selected price and change percentage
                  widget.graphTouched(priceAndChange.withItem1(price).withItem2(changePercentage));
                });

                if (event is FlLongPressEnd || event is FlPanCancelEvent || event is FlTapUpEvent) {
                  setState(() {
                    _touchDown = false;
                    widget.graphTouched(Tuple2<double, double>(-1, 0));
                    _selectedGraphPoint = "";
                  });
                }
              }
            }
        )
    );
  }

  ///Return the the market data currently selected by the buttons
  MarketData _getCurrentMarketData(List<MarketData> marketChart) {
    switch (_isSelectedPeriod) { //api only gives daily data. Select daily data for hour too
      case TimePeriod.hour : {
        return marketChart[0]; //use day data
      }

      case TimePeriod.day : {
        return marketChart[0]; //day
      }

      case TimePeriod.week : {
        return marketChart[1]; //week use month data
      }

      case TimePeriod.month : {
        return marketChart[1]; //month
      }

      case TimePeriod.year : {
        return marketChart[2]; //year use max data
      }

      case TimePeriod.max : {
        return marketChart[2]; //max
      }

      default : {
        return marketChart[0];
      }
    }
  }

  ///Returns the current list of dates and prices for the selected time period
  List<DatePrice> _getCurrentDatePrices(MarketData marketData) {
    switch (_isSelectedPeriod) {
      case TimePeriod.hour : { //hour
        return marketData.getPricesSinceDate(DateTime.now().subtract(Duration(hours: 1)));
      }

      case TimePeriod.day : { //day
        final data = marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 1)));
        return data.everyNthEntry(3); //every third entry, ~ every 15 min
      }

      case TimePeriod.week : { //week
        final data = marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 7)));
        return data.everyNthEntry(2); //every second entry, ~ every 2 hours, using monthly data (hourly)
      }

      case TimePeriod.month : { //month
        final data = marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 30)));
        return data.everyNthEntry(6); //every 6th entry, ~ every 6 hours
      }

      case TimePeriod.year : { //year
        return marketData.getPricesSinceDate(DateTime.now().subtract(Duration(days: 365))); //every day, using max data (daily)
      }

      case TimePeriod.max : { //max
        int interval = (marketData.prices.length ~/ 365) + 1;
        return marketData.prices.everyNthEntry(interval);
      }

      default : {
        return [];
      }
    }
  }

  ///Filters the market data and selects only a certain number of entries
  List<DatePrice> _filterData(MarketData marketData) {
    List<DatePrice> list = [];
    final data = _getCurrentDatePrices(marketData);

    switch (_isSelectedPeriod) {
      case TimePeriod.hour : { //hour
        for (int i = 0; i < data.length; i++) {
          list.add(data[i]); //only the last hour, ~ every 5 min
        }
        return list;
      }

      case TimePeriod.day : { //day
        for (int i = 0; i < data.length; i++) {
          list.add(data[i]);
        }
        return list;
      }

      case TimePeriod.week : { //week
        for (int i = 0; i < data.length; i++) {
          list.add(data[i]);
        }
        return list;
      }

      case TimePeriod.month : { //month
        for (int i = 0; i < data.length; i++) {
          list.add(data[i]);
        }
        return list;
      }

      case TimePeriod.year : { //year
        for (int i = 0; i < data.length; i++) {
          list.add(data[i]);
        }
        return list;
      }

      case TimePeriod.max : { //max
        for (int i = 0; i < data.length; i++) {
          list.add(data[i]);
        }
        return list;
      }

      default : {
        return [];
      }
    }
  }

  ///Returns a list of FlSpots with the given data for the graph to show
  List<FlSpot> _getFlSpots(List<DatePrice> filteredData) { //period from _isSelectedIndex
    List<FlSpot> list = [];

    for (int i = 0; i < filteredData.length; i++) {
      list.add(FlSpot(i.toDouble(), filteredData[i].price));
    }

    return list;
  }

  ///Returns the highest price from the given list of DatePrices
  double _getHighestPrice(List<DatePrice> prices) {
    double max = 0;
    for (int i = 0; i < prices.length; i++) {
      if (prices[i].price > max) {
        max = prices[i].price;
      }
    }
    return max;
  }

  ///Returns the lowest price from the given list of DatePrices
  double _getLowestPrice(List<DatePrice> prices) {
    double min = prices[0].price;
    for (int i = 0; i < prices.length; i++) {
      if (prices[i].price < min) {
        min = prices[i].price;
      }
    }
    return min;
  }

  ///Checks if the price value has more than 9 symbols and is on the beginning or the end of the graph.
  ///If yes the position of the value in the graph is changed to 1/8 or 7/8 of all X-values in the graph
  int _correctPriceDisplayPosition(double price, int priceIndex, int listLength) {
    final numSymbols = price.toString().length + 2; //+ space and currency symbol

    if (numSymbols >= 9) { //potentially not enough room to display
      if (priceIndex < listLength ~/ 8) { //is value at the beginning of the graph
        return listLength ~/ 8;
      }
      if (priceIndex > 7 * (listLength ~/ 8)) { //is value at the end of the graph
        if (listLength < 15) { //use value closer to the right to avoid moving much
          return 9 * (listLength ~/ 10);
        }
        return 7 * (listLength ~/ 8);
      }
    }
    return priceIndex;
  }

  ///Get the date string from the list entry with the given index from the given list.
  ///If the currently selected time period is year or max, the year will be shown
  String _getSelectedDate(List<DatePrice> data, int listIndex) {
    if (_isSelectedPeriod == TimePeriod.hour || _isSelectedPeriod == TimePeriod.day) { //hour or day
      return data[listIndex].date.getTimeOnly();
    }
    if (_isSelectedPeriod == TimePeriod.year || _isSelectedPeriod == TimePeriod.max) //year or max
      return _getFormatedDate(data[listIndex].date);

    return _getFormatedDate(data[listIndex].date, showYear: false); //week or month
  }

  ///Returns the change percentage between the selected price and the first price of the given DatePrice list
  double _calculatePriceChangePercentage(double selectedPrice, List<DatePrice> filteredData) {
    double firstPrice = filteredData.first.price;
    return ((selectedPrice - firstPrice) / firstPrice) * 100;
  }

  ///Checks if data is available for the currently selected period on the given market chart
  bool _isDataAvailable(List<MarketData> marketChart) {
    final marketData = _getCurrentMarketData(marketChart);
    final data = _getCurrentDatePrices(marketData);
    if (data.length <= 1) {
      return false;
    }
    return true;
  }

  ///Gets a date string in mm/yy or dd/mm format from DateTime in locale format.
  static String _getFormatedDate(DateTime dateTime, {bool showYear = true}) {
    if (showYear)
      return dateTime.yM();
    else
      return dateTime.mD();
  }
}