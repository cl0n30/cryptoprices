// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  static String m0(count) =>
      "${Intl.plural(count, one: 'Alarm', other: 'Alarme')}";

  static String m1(coinSymbol) => "Alle ${coinSymbol} Alarme löschen";

  static String m2(date, time) => "${date}, ${time}";

  static String m3(char) => "Ungültiges Zeichen: ${char}";

  static String m4(count) =>
      "${Intl.plural(count, one: 'Vor 1 Stunde, ', other: 'Vor ${count} Stunden, ')}";

  static String m5(time) => "Jetzt gerade, ${time}";

  static String m6(count) =>
      "${Intl.plural(count, one: 'Vor 1 Minute, ', other: 'Vor ${count} Minuten, ')}";

  static String m7(time) => "Gestern, ${time}";

  static String m8(coinSymbol) =>
      "${coinSymbol} Menge in Zwischenablage kopiert";

  static String m9(marketCap, currencySymbol) =>
      "Globale Marktkapitalisierung: ${marketCap} ${currencySymbol}";

  static String m10(active) => "Aktive Kryptowährungen: ${active}";

  static String m11(volume, currencySymbol) =>
      "24H Volumen: ${volume} ${currencySymbol}";

  static String m12(marketCap, currencySymbol) =>
      "Globale Marktkap.: ${marketCap} ${currencySymbol}";

  static String m13(volume, currencySymbol) =>
      "Globales 24H Volumen: ${volume} ${currencySymbol}";

  static String m14(count) => "${count} Minuten";

  static String m15(coinName, percentage, coinPrice, currencySymbol) =>
      "${coinName} Preis veränderte sich um ${percentage}% in den letzten 24H. Jetzt bei ${coinPrice} ${currencySymbol}";

  static String m16(coinName) => "Große ${coinName} Preisbewegung";

  static String m17(movementText, targetValue) =>
      "24H Änderung ${movementText} ${targetValue}%";

  static String m18(movementText, targetValue, coinPrice, currencySymbol) =>
      "24H Änderung ${movementText} ${targetValue}%. Jetzt bei ${coinPrice} ${currencySymbol}";

  static String m19(movementText, targetValue, currencySymbol) =>
      "Preis ${movementText} ${targetValue} ${currencySymbol}";

  static String m20(coinName) => "${coinName} Preis Alarm";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aboutAuthorCountry":
            MessageLookupByLibrary.simpleMessage("Deutschland"),
        "aboutAuthorMail":
            MessageLookupByLibrary.simpleMessage("Eine E-Mail schreiben"),
        "aboutAuthorName": MessageLookupByLibrary.simpleMessage("Clone Apps"),
        "aboutChangelog":
            MessageLookupByLibrary.simpleMessage("Änderungsprotokoll"),
        "aboutCoinGeckoName":
            MessageLookupByLibrary.simpleMessage("CoinGecko API"),
        "aboutCoinGeckoWebsite":
            MessageLookupByLibrary.simpleMessage("Website besuchen"),
        "aboutGitLab": MessageLookupByLibrary.simpleMessage("GitLab"),
        "aboutGitLabText": MessageLookupByLibrary.simpleMessage(
            "Quellcode einsehen, Probleme melden"),
        "aboutGroupAuthor": MessageLookupByLibrary.simpleMessage("Entwickler"),
        "aboutGroupCoinGecko":
            MessageLookupByLibrary.simpleMessage("Powered by"),
        "aboutGroupSupport":
            MessageLookupByLibrary.simpleMessage("Unterstützen"),
        "aboutLicenses": MessageLookupByLibrary.simpleMessage("Lizenzen"),
        "aboutLicensesText": MessageLookupByLibrary.simpleMessage(
            "Lizenzen der Bibliotheken, die verwendet werden"),
        "aboutSupportRate": MessageLookupByLibrary.simpleMessage("Bewerten"),
        "aboutSupportRateText": MessageLookupByLibrary.simpleMessage(
            "Hinterlasse eine Bewertung im Google Play Store"),
        "aboutVersion": MessageLookupByLibrary.simpleMessage("Version"),
        "addPortfolio":
            MessageLookupByLibrary.simpleMessage("Portfolio hinzufügen"),
        "addTransaction":
            MessageLookupByLibrary.simpleMessage("Transaktion hinzufügen"),
        "addTransactionUrl":
            MessageLookupByLibrary.simpleMessage("Neue URL hinzufügen"),
        "alertAbove": MessageLookupByLibrary.simpleMessage("Bewegt sich über"),
        "alertAddTooltip":
            MessageLookupByLibrary.simpleMessage("Alarm hinzufügen"),
        "alertBelow": MessageLookupByLibrary.simpleMessage("Bewegt sich unter"),
        "alertChange": MessageLookupByLibrary.simpleMessage("Änderung 24H (%)"),
        "alertCoin": MessageLookupByLibrary.simpleMessage("Coin"),
        "alertCount": m0,
        "alertCurrency": MessageLookupByLibrary.simpleMessage("Währung"),
        "alertCurrentPrice":
            MessageLookupByLibrary.simpleMessage("Aktueller Preis"),
        "alertDeleteTooltip":
            MessageLookupByLibrary.simpleMessage("Alarm löschen"),
        "alertDialogCancel": MessageLookupByLibrary.simpleMessage("Abbrechen"),
        "alertDialogClose": MessageLookupByLibrary.simpleMessage("Schließen"),
        "alertDialogCloseAndDelete":
            MessageLookupByLibrary.simpleMessage("Schließen und löschen"),
        "alertDialogContinue":
            MessageLookupByLibrary.simpleMessage("Fortfahren"),
        "alertDialogDelete": MessageLookupByLibrary.simpleMessage("Löschen"),
        "alertDialogDeleteCoinPriceAlertTitle": m1,
        "alertDialogDeletePriceAlertMessage":
            MessageLookupByLibrary.simpleMessage(
                "Diese Aktion kann nicht rückgängig gemacht werden"),
        "alertDialogDeletePriceAlertTitle":
            MessageLookupByLibrary.simpleMessage("Alarm löschen"),
        "alertDialogGrant": MessageLookupByLibrary.simpleMessage("Erteilen"),
        "alertDialogRemove": MessageLookupByLibrary.simpleMessage("Entfernen"),
        "alertDialogRename": MessageLookupByLibrary.simpleMessage("Umbenennen"),
        "alertDialogSelect": MessageLookupByLibrary.simpleMessage("Auswählen"),
        "alertEntryDeleteAllAlertsTooltip":
            MessageLookupByLibrary.simpleMessage("Alle Alarme löschen"),
        "alertEntryOpenDetailTooltip":
            MessageLookupByLibrary.simpleMessage("Zeige Coin Details"),
        "alertEqual": MessageLookupByLibrary.simpleMessage("Ist gleich"),
        "alertErrorText": MessageLookupByLibrary.simpleMessage(
            "Bitte geben Sie eine valide Zahl ein"),
        "alertFrequency": MessageLookupByLibrary.simpleMessage("Alarmfrequenz"),
        "alertHintText":
            MessageLookupByLibrary.simpleMessage("Eine Zahl eingeben"),
        "alertOnce": MessageLookupByLibrary.simpleMessage("Einmal"),
        "alertPrice": MessageLookupByLibrary.simpleMessage("Preis"),
        "alertPriceMovement":
            MessageLookupByLibrary.simpleMessage("Preisbewegung"),
        "alertRepeat": MessageLookupByLibrary.simpleMessage("Wiederholt"),
        "alertTargetValue": MessageLookupByLibrary.simpleMessage("Zielwert"),
        "alertTitle": MessageLookupByLibrary.simpleMessage("Alarm"),
        "appName": MessageLookupByLibrary.simpleMessage("Krypto Preise"),
        "authenticateWrongPasswordError":
            MessageLookupByLibrary.simpleMessage("Falsches Passwort"),
        "authenticationContinueWithoutPassword":
            MessageLookupByLibrary.simpleMessage("Ohne Passwort fortfahren"),
        "authenticationNoPasswordError": MessageLookupByLibrary.simpleMessage(
            "Bitte geben Sie ein Passwort ein"),
        "authenticationRequired":
            MessageLookupByLibrary.simpleMessage("Authentifizierung notwendig"),
        "authenticationTitle":
            MessageLookupByLibrary.simpleMessage("App entsperren"),
        "authenticationUnlockButton":
            MessageLookupByLibrary.simpleMessage("Entsperren"),
        "authenticationUseBiometrics":
            MessageLookupByLibrary.simpleMessage("Nutze biometrische Daten"),
        "autoBackupChangeFileName":
            MessageLookupByLibrary.simpleMessage("Datei umbenennen"),
        "autoBackupDate": m2,
        "autoBackupEnterFileNameHint":
            MessageLookupByLibrary.simpleMessage("Dateinamen eingeben"),
        "autoBackupErrorInvalidCharacter": m3,
        "autoBackupErrorNoFileName": MessageLookupByLibrary.simpleMessage(
            "Bitte geben Sie einen Namen ein"),
        "autoBackupHours": m4,
        "autoBackupJustNow": m5,
        "autoBackupMinutes": m6,
        "autoBackupNoBackupLocation":
            MessageLookupByLibrary.simpleMessage("Kein Backup Speicherort"),
        "autoBackupPermissionRationaleText": MessageLookupByLibrary.simpleMessage(
            "Die App benötigt Zugriff auf den Speicher um automatisch Backups Ihrer Daten zu erstellen."),
        "autoBackupPermissionRationaleTitle":
            MessageLookupByLibrary.simpleMessage(
                "Speicherberechtigung erteilen"),
        "autoBackupYesterday": m7,
        "bitcoin": MessageLookupByLibrary.simpleMessage("Bitcoin"),
        "coinAmountCopied": m8,
        "coinsListTabName": MessageLookupByLibrary.simpleMessage("Coins"),
        "collapseAll": MessageLookupByLibrary.simpleMessage("Alle einklappen"),
        "copyToClipBoard":
            MessageLookupByLibrary.simpleMessage("In Zwischenablage kopieren"),
        "copyToClipBoardAddressMessage": MessageLookupByLibrary.simpleMessage(
            "Adresse in Zwischenablage kopiert"),
        "deletePortfolio":
            MessageLookupByLibrary.simpleMessage("Portfolio löschen"),
        "deleteSearchEntryDialogMessage":
            MessageLookupByLibrary.simpleMessage("Aus Suchverlauf entfernen?"),
        "deleteTransaction":
            MessageLookupByLibrary.simpleMessage("Transaktion löschen"),
        "deleteWallet": MessageLookupByLibrary.simpleMessage("Wallet löschen"),
        "detailAppbarTitle": MessageLookupByLibrary.simpleMessage("Details"),
        "detailField24HHigh": MessageLookupByLibrary.simpleMessage("24H Hoch"),
        "detailField24HLow": MessageLookupByLibrary.simpleMessage("24H Tief"),
        "detailFieldAth": MessageLookupByLibrary.simpleMessage("Allzeithoch"),
        "detailFieldAtl": MessageLookupByLibrary.simpleMessage("Allzeittief"),
        "detailFieldGenesisDate":
            MessageLookupByLibrary.simpleMessage("Entstehungsdatum"),
        "detailFieldHomepage": MessageLookupByLibrary.simpleMessage("Homepage"),
        "detailFieldMarketCap":
            MessageLookupByLibrary.simpleMessage("Marktkap. "),
        "detailFieldMarketCapRank":
            MessageLookupByLibrary.simpleMessage("Marktkap. Rang"),
        "detailFieldSupply":
            MessageLookupByLibrary.simpleMessage("Umlaufmenge"),
        "detailFieldVolume": MessageLookupByLibrary.simpleMessage("Volumen"),
        "detailHeadingAbout": MessageLookupByLibrary.simpleMessage("Über"),
        "detailHeadingMarketStats":
            MessageLookupByLibrary.simpleMessage("Markt Statistiken"),
        "donateTitle": MessageLookupByLibrary.simpleMessage("Spenden"),
        "editPasswordEnterHint":
            MessageLookupByLibrary.simpleMessage("Passwort eingeben"),
        "editPasswordNoPasswordError": MessageLookupByLibrary.simpleMessage(
            "Bitte geben Sie ein Passwort ein"),
        "editPasswordRemove":
            MessageLookupByLibrary.simpleMessage("Passwort entfernen"),
        "editPasswordRemoveDialogTitle":
            MessageLookupByLibrary.simpleMessage("Entferne Password-Sperre"),
        "editPasswordRepeatHint":
            MessageLookupByLibrary.simpleMessage("Passwort wiederholen"),
        "editPasswordRepeatPasswordError": MessageLookupByLibrary.simpleMessage(
            "Bitte wiederholen Sie das Passwort"),
        "editPasswordShowPassword":
            MessageLookupByLibrary.simpleMessage("Passwort zeigen"),
        "error": MessageLookupByLibrary.simpleMessage("Fehler"),
        "errorMessageHttp": MessageLookupByLibrary.simpleMessage(
            "Dienst momentan nicht verfügbar"),
        "errorMessageRateLimitation": MessageLookupByLibrary.simpleMessage(
            "Zu viele Anfragen. Bitte versuchen Sie es nach einer Minute erneut"),
        "errorMessageRateLimitationShort":
            MessageLookupByLibrary.simpleMessage("Zu viele Anfragen"),
        "errorMessageSocket":
            MessageLookupByLibrary.simpleMessage("Keine Internetverbindung"),
        "errorNoDataAvailable":
            MessageLookupByLibrary.simpleMessage("Keine Daten verfügbar"),
        "ethereumAndTokens":
            MessageLookupByLibrary.simpleMessage("Ethereum (und ERC20 Tokens)"),
        "expandAll": MessageLookupByLibrary.simpleMessage("Alle ausklappen"),
        "explorerUrlNoCoinSelected":
            MessageLookupByLibrary.simpleMessage("Kein Coin ausgewählt"),
        "explorerUrlSelectACoin":
            MessageLookupByLibrary.simpleMessage("Wähle einen Coin"),
        "explorerUrlTitle":
            MessageLookupByLibrary.simpleMessage("Explorer URL"),
        "explorerUrlTransactionUrl":
            MessageLookupByLibrary.simpleMessage("Transaktions URL"),
        "explorerUrlTransactionUrlHint":
            MessageLookupByLibrary.simpleMessage("Eine URL eingeben"),
        "explorerUrlTransactionUrlInfo": MessageLookupByLibrary.simpleMessage(
            "Die Transaktions ID wird direkt hinter die URL gesetzt, daher muss die URL folgendes Format haben:"),
        "explorerUrlTransactionUrlInfoExample":
            MessageLookupByLibrary.simpleMessage(
                "https://explorer.com/pfad/zur/transaktion/"),
        "favoriteRemoved":
            MessageLookupByLibrary.simpleMessage("Favorit entfernt"),
        "favoritesTabName": MessageLookupByLibrary.simpleMessage("Favoriten"),
        "globalMarketCap": m9,
        "globalMarketInfoActiveCrypto": m10,
        "globalMarketInfoGlobalVolume": m11,
        "globalMarketInfoMarketCap": m12,
        "globalMarketInfoMarketCapChange":
            MessageLookupByLibrary.simpleMessage("Marktkap. 24H Änderung: "),
        "globalMarketInfoMarketShare":
            MessageLookupByLibrary.simpleMessage("Coin Marktanteile: "),
        "globalMarketInfoTile":
            MessageLookupByLibrary.simpleMessage("Globale Markt Info"),
        "globalVolume": m13,
        "graphToggleDay": MessageLookupByLibrary.simpleMessage("1D"),
        "graphToggleHour": MessageLookupByLibrary.simpleMessage("1H"),
        "graphToggleMax": MessageLookupByLibrary.simpleMessage("Max"),
        "graphToggleMonth": MessageLookupByLibrary.simpleMessage("1M"),
        "graphToggleWeek": MessageLookupByLibrary.simpleMessage("1W"),
        "graphToggleYear": MessageLookupByLibrary.simpleMessage("1Y"),
        "liberapay": MessageLookupByLibrary.simpleMessage("Liberapay"),
        "loadingDataMessage":
            MessageLookupByLibrary.simpleMessage("Lade Daten"),
        "migrationAssistantAlertDialogMessage":
            MessageLookupByLibrary.simpleMessage(
                "Alle inkorrekten Daten werden gelöscht um sicherzustellen dass die App weiterhin korrekt funktioniert.\nDiese Aktion kann nicht rückgängig gemacht werden."),
        "migrationAssistantBackButton":
            MessageLookupByLibrary.simpleMessage("< Zurück"),
        "migrationAssistantContinueButton":
            MessageLookupByLibrary.simpleMessage("Weiter >"),
        "migrationAssistantEntrySearchCoins":
            MessageLookupByLibrary.simpleMessage("Coins suchen"),
        "migrationAssistantEntrySelectCoin":
            MessageLookupByLibrary.simpleMessage(
                "Wählen Sie den korrekten Coin"),
        "migrationAssistantFinishButton":
            MessageLookupByLibrary.simpleMessage("Fertig"),
        "migrationAssistantText1": MessageLookupByLibrary.simpleMessage(
            "Die Coin-IDs, welche die Web-API verwendet, haben sich geändert und eine automatische Migration ist nicht möglich."),
        "migrationAssistantText2": MessageLookupByLibrary.simpleMessage(
            "Eine manuelle Migration ist notwendig, damit die App weiterhin die richtigen Coin-Informationen abrufen kann und korrekt funktioniert."),
        "migrationAssistantText3": MessageLookupByLibrary.simpleMessage(
            "Auf der folgenden Seite müssen Sie eine neue ID für jeden geänderten Coin auswählen.\nEs werden einige mögliche Optionen angezeigt."),
        "migrationAssistantTitle":
            MessageLookupByLibrary.simpleMessage("ID Migrations Assistent"),
        "minutes": m14,
        "monthlyDonation": MessageLookupByLibrary.simpleMessage(
            "Monatliche Spende einrichten"),
        "moreInformation":
            MessageLookupByLibrary.simpleMessage("Mehr Informationen"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "never": MessageLookupByLibrary.simpleMessage("Niemals"),
        "newPortfolio": MessageLookupByLibrary.simpleMessage("Neues Portfolio"),
        "newTransaction":
            MessageLookupByLibrary.simpleMessage("Neue Transaktion"),
        "noBiometrics": MessageLookupByLibrary.simpleMessage(
            "Keine biometrischen Daten vorhanden"),
        "noFavorites":
            MessageLookupByLibrary.simpleMessage("Noch keine Favoriten"),
        "noPortfolios":
            MessageLookupByLibrary.simpleMessage("Noch keine Portfolios"),
        "noPriceAlerts":
            MessageLookupByLibrary.simpleMessage("Noch keine Preisalarme"),
        "noWalletTransactions": MessageLookupByLibrary.simpleMessage(
            "Noch keine Wallet Transaktionen"),
        "notAvailableInPrivateMode": MessageLookupByLibrary.simpleMessage(
            "Nicht verfügbar im Privatmodus"),
        "notificationLargeMoveAlertChannelName":
            MessageLookupByLibrary.simpleMessage("Große Preisbewegungen"),
        "notificationLargeMoveAlertCoinNamePlaceholder":
            MessageLookupByLibrary.simpleMessage("Favoriten"),
        "notificationLargeMoveAlertMessage": m15,
        "notificationLargeMoveAlertTitle": m16,
        "notificationPriceAlertAbove":
            MessageLookupByLibrary.simpleMessage("bewegte sich über"),
        "notificationPriceAlertBelow":
            MessageLookupByLibrary.simpleMessage("bewegte sich unter"),
        "notificationPriceAlertEqual":
            MessageLookupByLibrary.simpleMessage("ist gleich"),
        "notificationPriceAlertMessagePercent": m17,
        "notificationPriceAlertMessagePercentWithPrice": m18,
        "notificationPriceAlertMessagePrice": m19,
        "notificationPriceAlertTitle": m20,
        "password": MessageLookupByLibrary.simpleMessage("Passwort"),
        "portfolioEditDefaultPortfolio":
            MessageLookupByLibrary.simpleMessage("Standard Portfolio"),
        "portfolioEditEditPortfolio":
            MessageLookupByLibrary.simpleMessage("Portfolio bearbeiten"),
        "portfolioEditNameErrorText": MessageLookupByLibrary.simpleMessage(
            "Bitte geben Sie einen Namen ein"),
        "portfolioEditNameHintText":
            MessageLookupByLibrary.simpleMessage("Portfolio Namen eingeben"),
        "portfolioEditUseAppCurrency":
            MessageLookupByLibrary.simpleMessage("App Währung nutzen"),
        "portfolioIsEmpty":
            MessageLookupByLibrary.simpleMessage("Portfolio ist leer"),
        "portfolioLocked":
            MessageLookupByLibrary.simpleMessage("Portfolio gesperrt"),
        "portfolioPieChartOther":
            MessageLookupByLibrary.simpleMessage("Andere "),
        "portfolioTabName": MessageLookupByLibrary.simpleMessage("Portfolio"),
        "portfolioTotalValue":
            MessageLookupByLibrary.simpleMessage("Gesamtwert: "),
        "priceAlertsTabName":
            MessageLookupByLibrary.simpleMessage("Preisalarme"),
        "privateMode": MessageLookupByLibrary.simpleMessage("Privatmodus"),
        "qrScannerBarcodeType":
            MessageLookupByLibrary.simpleMessage("Barcode Typ: "),
        "qrScannerData": MessageLookupByLibrary.simpleMessage("Daten: "),
        "qrScannerNoPermission":
            MessageLookupByLibrary.simpleMessage("Keine Berechtigung"),
        "qrScannerScanCode":
            MessageLookupByLibrary.simpleMessage("Einen Code scannen"),
        "rateLimitInfoCoinGeckoPrices":
            MessageLookupByLibrary.simpleMessage("Coingecko API Preise"),
        "rateLimitInfoExplanation":
            MessageLookupByLibrary.simpleMessage("Erklärung"),
        "rateLimitInfoExplanationFirst": MessageLookupByLibrary.simpleMessage(
            "Die kostenlose Version der Coingecko API, welche diese App benutzt, beschränkt die Zahl der Anfragen, die gesendet werden können, auf 50 pro Minute."),
        "rateLimitInfoExplanationSecond": MessageLookupByLibrary.simpleMessage(
            "Wenn Sie viele Favoriten oder Wallets haben, kann diese Nummer schnell überschritten werden, zum Beispiel durch häufiges Aktualisieren."),
        "rateLimitInfoSolution": MessageLookupByLibrary.simpleMessage("Lösung"),
        "rateLimitInfoSolutionFirst": MessageLookupByLibrary.simpleMessage(
            "Falls dieser Fehler auftritt, müssen Sie ungefähr eine Minute warten, bevor Sie wieder aktuelle Daten bekommen können."),
        "rateLimitInfoSolutionFourth": MessageLookupByLibrary.simpleMessage(
            "Falls Sie helfen wollen, für einen API-Schlüssel zu bezahlen und die Entwicklung dieser App zu unterstützen, können Sie dies über die Spendenlinks unten."),
        "rateLimitInfoSolutionSecond": MessageLookupByLibrary.simpleMessage(
            "Die einzig andere Option ist für einen API-Schlüssel zu bezahlen."),
        "rateLimitInfoSolutionThird": MessageLookupByLibrary.simpleMessage(
            "Die momentan billigste Bezahlversion kostet 129 \$ im Monat, daher kann ich es mir leider nicht leisten, dafür allein zu bezahlen."),
        "rateLimitInfoTitle":
            MessageLookupByLibrary.simpleMessage("Über Ratenbeschränkung"),
        "searchFieldHintText": MessageLookupByLibrary.simpleMessage("Suchen"),
        "searchToolTip": MessageLookupByLibrary.simpleMessage("Suche"),
        "selectCurrencyTitle":
            MessageLookupByLibrary.simpleMessage("Währung auswählen"),
        "selectLanguageTitle":
            MessageLookupByLibrary.simpleMessage("Sprache auswählen"),
        "selectStartScreenTitle":
            MessageLookupByLibrary.simpleMessage("Startbildschirm auswählen"),
        "settingsAbout": MessageLookupByLibrary.simpleMessage("Über"),
        "settingsAppearance":
            MessageLookupByLibrary.simpleMessage("Erscheinungsbild"),
        "settingsAppearanceApp": MessageLookupByLibrary.simpleMessage("App"),
        "settingsAppearanceColor":
            MessageLookupByLibrary.simpleMessage("Farbe"),
        "settingsAppearanceColorDialogTitle":
            MessageLookupByLibrary.simpleMessage("Wähle eine Farbe"),
        "settingsAppearanceColorSubtitle":
            MessageLookupByLibrary.simpleMessage("Wähle die Farbe der App"),
        "settingsAppearanceDateFormat":
            MessageLookupByLibrary.simpleMessage("Datumsformat"),
        "settingsAppearanceDateFormatTitle":
            MessageLookupByLibrary.simpleMessage("Datumsformat auswählen"),
        "settingsAppearanceNumberFormat":
            MessageLookupByLibrary.simpleMessage("Nummernformat"),
        "settingsAppearanceNumberFormatTitle":
            MessageLookupByLibrary.simpleMessage("Nummernformat auswählen"),
        "settingsAppearanceNumbers":
            MessageLookupByLibrary.simpleMessage("Nummern"),
        "settingsAppearanceTheme":
            MessageLookupByLibrary.simpleMessage("Design"),
        "settingsAppearanceThemeAmoled":
            MessageLookupByLibrary.simpleMessage("AMOLED-Design"),
        "settingsAppearanceThemeDark":
            MessageLookupByLibrary.simpleMessage("Dunkles Design"),
        "settingsAppearanceThemeDialogTitle":
            MessageLookupByLibrary.simpleMessage("Wähle ein Design"),
        "settingsAppearanceThemeLight":
            MessageLookupByLibrary.simpleMessage("Helles Design"),
        "settingsAppearanceThemeSystem": MessageLookupByLibrary.simpleMessage(
            "Durch Energiesparmodus bestimmt"),
        "settingsAppearanceThemeSystemAmoled":
            MessageLookupByLibrary.simpleMessage(
                "Durch Energiesparmodus bestimmt (AMOLED)"),
        "settingsAutoBackupFileName":
            MessageLookupByLibrary.simpleMessage("Backup Dateiname"),
        "settingsAutoBackupLocation":
            MessageLookupByLibrary.simpleMessage("Backup Speicherort"),
        "settingsAutoBackupOverwriteSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Überschreibe die alte Datei wenn ein neues Backup erstellt wird"),
        "settingsAutoBackupOverwriteTitle":
            MessageLookupByLibrary.simpleMessage("Alte Datei überschreiben"),
        "settingsCalculator": MessageLookupByLibrary.simpleMessage("Rechner"),
        "settingsCurrency": MessageLookupByLibrary.simpleMessage("Währung"),
        "settingsEnglish": MessageLookupByLibrary.simpleMessage("English"),
        "settingsGerman": MessageLookupByLibrary.simpleMessage("Deutsch"),
        "settingsGroupTools": MessageLookupByLibrary.simpleMessage("Werkzeuge"),
        "settingsImportExport": MessageLookupByLibrary.simpleMessage("Backup"),
        "settingsImportExportAutoBackup":
            MessageLookupByLibrary.simpleMessage("Automatisches Backup"),
        "settingsImportExportExportFileSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Favoriten, Alarme und Portfolios exportieren"),
        "settingsImportExportExportFileTitle":
            MessageLookupByLibrary.simpleMessage("Exportieren"),
        "settingsImportExportExportSuccessful":
            MessageLookupByLibrary.simpleMessage("Export erfolgreich"),
        "settingsImportExportImportFileSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Daten aus einer json Datei importieren"),
        "settingsImportExportImportFileTitle":
            MessageLookupByLibrary.simpleMessage("Aus Datei importieren"),
        "settingsImportExportImportPortfolioName":
            MessageLookupByLibrary.simpleMessage("Import Portfolio"),
        "settingsImportExportImportSuccessful":
            MessageLookupByLibrary.simpleMessage("Import erfolgreich"),
        "settingsImportExportOverwriteAlertMessage":
            MessageLookupByLibrary.simpleMessage(
                "Vorhandene Daten werden dauerhaft überschrieben.\nWollen Sie fortfahren?"),
        "settingsImportExportOverwriteAlertTitle":
            MessageLookupByLibrary.simpleMessage("Daten überschreiben"),
        "settingsImportExportWrongFormat":
            MessageLookupByLibrary.simpleMessage("Falsches Dateiformat"),
        "settingsLanguage": MessageLookupByLibrary.simpleMessage("Sprache"),
        "settingsNotifications":
            MessageLookupByLibrary.simpleMessage("Benachrichtigungen"),
        "settingsNotificationsEnableAlertsText":
            MessageLookupByLibrary.simpleMessage(
                "Zeige Benachrichtigung bei großen Preisbewegungen von Favoriten"),
        "settingsNotificationsEnableMoveAlerts":
            MessageLookupByLibrary.simpleMessage("Alarme aktivieren"),
        "settingsNotificationsLargePriceMoveAlerts":
            MessageLookupByLibrary.simpleMessage("Große Preisbewegungen"),
        "settingsNotificationsMoveThreshold":
            MessageLookupByLibrary.simpleMessage("Preisbewegung Schwellwert"),
        "settingsNotificationsMoveThresholdHint":
            MessageLookupByLibrary.simpleMessage("Eine Zahl eingeben"),
        "settingsNotificationsShowPriceSubTitle":
            MessageLookupByLibrary.simpleMessage(
                "Zeige den Coinpreis in 24H-Änderungs-Alarmen"),
        "settingsNotificationsShowPriceTitle":
            MessageLookupByLibrary.simpleMessage(
                "Zeige Preis in Prozent-Alarmen"),
        "settingsPortfolio": MessageLookupByLibrary.simpleMessage("Portfolio"),
        "settingsPortfolioAddFavoriteTitle":
            MessageLookupByLibrary.simpleMessage("Zu Favoriten hinzufügen"),
        "settingsPortfolioAddToFavoriteSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Neue Wallet zu den Favoriten hinzufügen"),
        "settingsPortfolioChangePercentagePeriodTitle":
            MessageLookupByLibrary.simpleMessage("Wertänderungszeitraum"),
        "settingsPortfolioTransactions":
            MessageLookupByLibrary.simpleMessage("Transaktionen"),
        "settingsPortfolioTransactionsExplorerUrlsSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Transaktions URLs von Block-Explorern ändern"),
        "settingsPortfolioTransactionsExplorerUrlsTitle":
            MessageLookupByLibrary.simpleMessage("Block-Explorer URLs"),
        "settingsPortfolioTransactionsPortfolioCurrencySubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Zeige Transaktionswert in Portfolio Währung an"),
        "settingsPortfolioTransactionsPortfolioCurrencyTitle":
            MessageLookupByLibrary.simpleMessage("Wert in Portfolio Währung"),
        "settingsSecurity": MessageLookupByLibrary.simpleMessage("Sicherheit"),
        "settingsSecurityAuthOnAppStart": MessageLookupByLibrary.simpleMessage(
            "Beim App Start Authentifizieren"),
        "settingsSecurityAuthOnAppStartSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Öffne den Passwort Bildschirm wenn die App startet"),
        "settingsSecurityBiometricUnlock": MessageLookupByLibrary.simpleMessage(
            "Erlaube biometrisches Entsperren"),
        "settingsSecurityBiometricUnlockSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Erlaube biometrische Authentifizierung um die App zu entsperren"),
        "settingsSecurityChangePassword":
            MessageLookupByLibrary.simpleMessage("Passwort ändern"),
        "settingsSecurityKeepUnlocked": MessageLookupByLibrary.simpleMessage(
            "App nach dem Schließen entsperrt lassen"),
        "settingsSecurityKeepUnlockedDialogTitle":
            MessageLookupByLibrary.simpleMessage("Entsperrt lassen für"),
        "settingsSecurityLockNow":
            MessageLookupByLibrary.simpleMessage("Jetzt sperren"),
        "settingsSecurityPasswordLock":
            MessageLookupByLibrary.simpleMessage("Password-Sperre"),
        "settingsSecurityPasswordLockSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Sperre die App mit einem Passwort oder biometrischen Daten"),
        "settingsSecurityPrivateModeSubtitle":
            MessageLookupByLibrary.simpleMessage(
                "Verstecke das Guthaben überall in der App"),
        "settingsStartScreen":
            MessageLookupByLibrary.simpleMessage("Startbildschirm"),
        "settingsTabName":
            MessageLookupByLibrary.simpleMessage("Einstellungen"),
        "showCoinDetails":
            MessageLookupByLibrary.simpleMessage("Zeige Coin Details"),
        "showWallet": MessageLookupByLibrary.simpleMessage("Zeige Wallet"),
        "sortByToolTip": MessageLookupByLibrary.simpleMessage("Sortieren nach"),
        "sortMenuOptionMarketCap":
            MessageLookupByLibrary.simpleMessage("Marktkapitalisierung"),
        "sortMenuOptionName": MessageLookupByLibrary.simpleMessage("Name"),
        "sortMenuOptionNameAsc":
            MessageLookupByLibrary.simpleMessage("Name (A-Z)"),
        "sortMenuOptionNameDesc":
            MessageLookupByLibrary.simpleMessage("Name (Z-A)"),
        "sortMenuOptionPriceChange":
            MessageLookupByLibrary.simpleMessage("Preisveränderung"),
        "sortMenuOptionSymbol": MessageLookupByLibrary.simpleMessage("Symbol"),
        "sortMenuOptionTotalValue":
            MessageLookupByLibrary.simpleMessage("Gesamtwert"),
        "sortMenuOptionTotalValueAsc":
            MessageLookupByLibrary.simpleMessage("Niedrigster Wert"),
        "sortMenuOptionTotalValueDesc":
            MessageLookupByLibrary.simpleMessage("Höchster Wert"),
        "sortMenuOptionVolume": MessageLookupByLibrary.simpleMessage("Volumen"),
        "sortingDirectionTooltip":
            MessageLookupByLibrary.simpleMessage("Sortierrichtung"),
        "timePeriodDay": MessageLookupByLibrary.simpleMessage("Tag"),
        "timePeriodHour": MessageLookupByLibrary.simpleMessage("Stunde"),
        "timePeriodMax": MessageLookupByLibrary.simpleMessage("Max"),
        "timePeriodMonth": MessageLookupByLibrary.simpleMessage("Monat"),
        "timePeriodWeek": MessageLookupByLibrary.simpleMessage("Woche"),
        "timePeriodYear": MessageLookupByLibrary.simpleMessage("Jahr"),
        "transaction": MessageLookupByLibrary.simpleMessage("Transaktion"),
        "transactionEditCoinAmountHint":
            MessageLookupByLibrary.simpleMessage("Eine Zahl eingeben"),
        "transactionEditCoinPrice":
            MessageLookupByLibrary.simpleMessage("Coin Preis"),
        "transactionEditCoinPriceHint":
            MessageLookupByLibrary.simpleMessage("Einen Preis eingeben"),
        "transactionEditCurrencyAmountHint":
            MessageLookupByLibrary.simpleMessage("Eine Zahl eingeben"),
        "transactionEditDescription":
            MessageLookupByLibrary.simpleMessage("Beschreibung"),
        "transactionEditDescriptionHint":
            MessageLookupByLibrary.simpleMessage("Beschreibung eingeben"),
        "transactionEditDestinationAddress":
            MessageLookupByLibrary.simpleMessage("Zieladresse"),
        "transactionEditDestinationAddressHint":
            MessageLookupByLibrary.simpleMessage("Zieladresse eingeben"),
        "transactionEditFee":
            MessageLookupByLibrary.simpleMessage("Transaktionsgebühr"),
        "transactionEditMoreOptions":
            MessageLookupByLibrary.simpleMessage("Mehr Optionen"),
        "transactionEditNoDataForDate": MessageLookupByLibrary.simpleMessage(
            "Keine Daten für das gewählte Datum"),
        "transactionEditNoExplorerUrl":
            MessageLookupByLibrary.simpleMessage("Keine Explorer URL gesetzt"),
        "transactionEditNoTransactionId":
            MessageLookupByLibrary.simpleMessage("Keine Transaktions ID"),
        "transactionEditPriceFromDate": MessageLookupByLibrary.simpleMessage(
            "Nutze Preis von Transaktionsdatum"),
        "transactionEditQrCodeTooltip":
            MessageLookupByLibrary.simpleMessage("Aus QR-Code lesen"),
        "transactionEditSelectPortfolio": MessageLookupByLibrary.simpleMessage(
            "Bitte wählen Sie ein Portfolio aus"),
        "transactionEditSetUrl":
            MessageLookupByLibrary.simpleMessage("Setze URL"),
        "transactionEditSourceAddress":
            MessageLookupByLibrary.simpleMessage("Quelladresse"),
        "transactionEditSourceAddressHint":
            MessageLookupByLibrary.simpleMessage("Quelladresse eingeben"),
        "transactionEditTransactionDate":
            MessageLookupByLibrary.simpleMessage("Transaktionsdatum"),
        "transactionEditTransactionId":
            MessageLookupByLibrary.simpleMessage("Transaktions ID"),
        "transactionEditTransactionIdHint":
            MessageLookupByLibrary.simpleMessage("Transaktions ID eingeben"),
        "transactionEditTransactionName":
            MessageLookupByLibrary.simpleMessage("Transaktionsname"),
        "transactionEditTransactionNameHint":
            MessageLookupByLibrary.simpleMessage("Transaktionsnamen eingeben"),
        "transactionEditTransactionVolume":
            MessageLookupByLibrary.simpleMessage("Transaktionsvolumen"),
        "transactionEditViewInExplorer":
            MessageLookupByLibrary.simpleMessage("In Explorer zeigen"),
        "transactionReceived": MessageLookupByLibrary.simpleMessage("Erhalten"),
        "transactionSent": MessageLookupByLibrary.simpleMessage("Gesendet"),
        "transactionUrlsListTitle":
            MessageLookupByLibrary.simpleMessage("Transaktions URLs"),
        "undo": MessageLookupByLibrary.simpleMessage("Rückgängig"),
        "urlLaunchAlertDialogMessage": MessageLookupByLibrary.simpleMessage(
            "Link konnte nicht geöffnet werden.\nPrüfen sie ob ein Internet Browser installiert ist."),
        "urlLaunchAlertDialogTitle":
            MessageLookupByLibrary.simpleMessage("Link öffnen fehlgeschlagen"),
        "useOwnCoinPriceSubtitle": MessageLookupByLibrary.simpleMessage(
            "Nutze den Transaktions-Coinpreis für den Währungswert in der Liste"),
        "useOwnCoinPriceTitle": MessageLookupByLibrary.simpleMessage(
            "Nutze Transaktions-Coinpreis"),
        "valueCopied": MessageLookupByLibrary.simpleMessage(
            "Wert in Zwischenablage kopiert"),
        "walletDetailTransactions":
            MessageLookupByLibrary.simpleMessage("Transaktionen")
      };
}
