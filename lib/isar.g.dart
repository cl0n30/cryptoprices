// ignore_for_file: unused_import, implementation_imports

import 'dart:ffi';
import 'dart:convert';
import 'dart:isolate';
import 'dart:typed_data';
import 'dart:io';
import 'package:isar/isar.dart';
import 'package:isar/src/isar_native.dart';
import 'package:isar/src/query_builder.dart';
import 'package:ffi/ffi.dart';
import 'package:path/path.dart' as p;
import 'models/large_move_alert.dart';
import 'models/price_alert.dart';
import 'models/price_alert_list_entry.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:crypto_prices/database/converters.dart';

const _utf8Encoder = Utf8Encoder();

final _schema =
    '[{"name":"LargeMoveAlert","idProperty":"id","properties":[{"name":"id","type":3},{"name":"coinId","type":5},{"name":"coinName","type":5},{"name":"coinSymbol","type":5},{"name":"currency","type":5},{"name":"lastNotificationSent","type":3}],"indexes":[{"unique":false,"replace":false,"properties":[{"name":"coinId","indexType":1,"caseSensitive":true}]}],"links":[]},{"name":"PriceAlert","idProperty":"id","properties":[{"name":"id","type":3},{"name":"coinId","type":5},{"name":"coinName","type":5},{"name":"coinSymbol","type":5},{"name":"currency","type":5},{"name":"targetValue","type":4},{"name":"targetValueType","type":3},{"name":"priceMovement","type":3},{"name":"alertFrequency","type":3},{"name":"dateAdded","type":3},{"name":"lastTargetValue","type":4}],"indexes":[{"unique":false,"replace":false,"properties":[{"name":"coinId","indexType":1,"caseSensitive":true}]}],"links":[]},{"name":"PriceAlertListEntry","idProperty":"id","properties":[{"name":"id","type":3},{"name":"coinId","type":5},{"name":"coinName","type":5},{"name":"coinSymbol","type":5},{"name":"listPosition","type":3},{"name":"isCollapsed","type":0}],"indexes":[{"unique":false,"replace":false,"properties":[{"name":"coinId","indexType":1,"caseSensitive":true}]},{"unique":false,"replace":false,"properties":[{"name":"isCollapsed","indexType":0,"caseSensitive":null}]}],"links":[]}]';

Future<Isar> openIsar(
    {String name = 'isar',
    String? directory,
    int maxSize = 1000000000,
    Uint8List? encryptionKey}) async {
  final path = await _preparePath(directory);
  return openIsarInternal(
      name: name,
      directory: path,
      maxSize: maxSize,
      encryptionKey: encryptionKey,
      schema: _schema,
      getCollections: (isar) {
        final collectionPtrPtr = malloc<Pointer>();
        final propertyOffsetsPtr = malloc<Uint32>(11);
        final propertyOffsets = propertyOffsetsPtr.asTypedList(11);
        final collections = <String, IsarCollection>{};
        nCall(IC.isar_get_collection(isar.ptr, collectionPtrPtr, 0));
        IC.isar_get_property_offsets(
            collectionPtrPtr.value, propertyOffsetsPtr);
        collections['LargeMoveAlert'] = IsarCollectionImpl<LargeMoveAlert>(
          isar: isar,
          adapter: _LargeMoveAlertAdapter(),
          ptr: collectionPtrPtr.value,
          propertyOffsets: propertyOffsets.sublist(0, 6),
          propertyIds: {
            'id': 0,
            'coinId': 1,
            'coinName': 2,
            'coinSymbol': 3,
            'currency': 4,
            'lastNotificationSent': 5
          },
          indexIds: {'coinId': 0},
          linkIds: {},
          backlinkIds: {},
          getId: (obj) => obj.id,
          setId: (obj, id) => obj.id = id,
        );
        nCall(IC.isar_get_collection(isar.ptr, collectionPtrPtr, 1));
        IC.isar_get_property_offsets(
            collectionPtrPtr.value, propertyOffsetsPtr);
        collections['PriceAlert'] = IsarCollectionImpl<PriceAlert>(
          isar: isar,
          adapter: _PriceAlertAdapter(),
          ptr: collectionPtrPtr.value,
          propertyOffsets: propertyOffsets.sublist(0, 11),
          propertyIds: {
            'id': 0,
            'coinId': 1,
            'coinName': 2,
            'coinSymbol': 3,
            'currency': 4,
            'targetValue': 5,
            'targetValueType': 6,
            'priceMovement': 7,
            'alertFrequency': 8,
            'dateAdded': 9,
            'lastTargetValue': 10
          },
          indexIds: {'coinId': 0},
          linkIds: {},
          backlinkIds: {},
          getId: (obj) => obj.id,
          setId: (obj, id) => obj.id = id,
        );
        nCall(IC.isar_get_collection(isar.ptr, collectionPtrPtr, 2));
        IC.isar_get_property_offsets(
            collectionPtrPtr.value, propertyOffsetsPtr);
        collections['PriceAlertListEntry'] =
            IsarCollectionImpl<PriceAlertListEntry>(
          isar: isar,
          adapter: _PriceAlertListEntryAdapter(),
          ptr: collectionPtrPtr.value,
          propertyOffsets: propertyOffsets.sublist(0, 6),
          propertyIds: {
            'id': 0,
            'coinId': 1,
            'coinName': 2,
            'coinSymbol': 3,
            'listPosition': 4,
            'isCollapsed': 5
          },
          indexIds: {'coinId': 0, 'isCollapsed': 1},
          linkIds: {},
          backlinkIds: {},
          getId: (obj) => obj.id,
          setId: (obj, id) => obj.id = id,
        );
        malloc.free(propertyOffsetsPtr);
        malloc.free(collectionPtrPtr);

        return collections;
      });
}

Future<String> _preparePath(String? path) async {
  if (path == null || p.isRelative(path)) {
    WidgetsFlutterBinding.ensureInitialized();
    final dir = await getApplicationDocumentsDirectory();
    return p.join(dir.path, path ?? 'isar');
  } else {
    return path;
  }
}

class _LargeMoveAlertAdapter extends TypeAdapter<LargeMoveAlert> {
  @override
  int serialize(IsarCollectionImpl<LargeMoveAlert> collection, RawObject rawObj,
      LargeMoveAlert object, List<int> offsets,
      [int? existingBufferSize]) {
    var dynamicSize = 0;
    final value0 = object.id;
    final _id = value0;
    final value1 = object.coinId;
    final _coinId = _utf8Encoder.convert(value1);
    dynamicSize += _coinId.length;
    final value2 = object.coinName;
    final _coinName = _utf8Encoder.convert(value2);
    dynamicSize += _coinName.length;
    final value3 = object.coinSymbol;
    final _coinSymbol = _utf8Encoder.convert(value3);
    dynamicSize += _coinSymbol.length;
    final value4 = object.currency;
    final _currency = _utf8Encoder.convert(value4);
    dynamicSize += _currency.length;
    final value5 = object.lastNotificationSent;
    final _lastNotificationSent = value5;
    final size = dynamicSize + 50;

    late int bufferSize;
    if (existingBufferSize != null) {
      if (existingBufferSize < size) {
        malloc.free(rawObj.buffer);
        rawObj.buffer = malloc(size);
        bufferSize = size;
      } else {
        bufferSize = existingBufferSize;
      }
    } else {
      rawObj.buffer = malloc(size);
      bufferSize = size;
    }
    rawObj.buffer_length = size;
    final buffer = rawObj.buffer.asTypedList(size);
    final writer = BinaryWriter(buffer, 50);
    writer.writeLong(offsets[0], _id);
    writer.writeBytes(offsets[1], _coinId);
    writer.writeBytes(offsets[2], _coinName);
    writer.writeBytes(offsets[3], _coinSymbol);
    writer.writeBytes(offsets[4], _currency);
    writer.writeDateTime(offsets[5], _lastNotificationSent);
    return bufferSize;
  }

  @override
  LargeMoveAlert deserialize(IsarCollectionImpl<LargeMoveAlert> collection,
      BinaryReader reader, List<int> offsets) {
    final object = LargeMoveAlert();
    object.id = reader.readLong(offsets[0]);
    object.coinId = reader.readString(offsets[1]);
    object.coinName = reader.readString(offsets[2]);
    object.coinSymbol = reader.readString(offsets[3]);
    object.currency = reader.readString(offsets[4]);
    object.lastNotificationSent = reader.readDateTimeOrNull(offsets[5]);
    return object;
  }

  @override
  P deserializeProperty<P>(BinaryReader reader, int propertyIndex, int offset) {
    switch (propertyIndex) {
      case 0:
        return (reader.readLong(offset)) as P;
      case 1:
        return (reader.readString(offset)) as P;
      case 2:
        return (reader.readString(offset)) as P;
      case 3:
        return (reader.readString(offset)) as P;
      case 4:
        return (reader.readString(offset)) as P;
      case 5:
        return (reader.readDateTimeOrNull(offset)) as P;
      default:
        throw 'Illegal propertyIndex';
    }
  }
}

class _PriceAlertAdapter extends TypeAdapter<PriceAlert> {
  static const _TargetValueTypeConverter = TargetValueTypeConverter();
  static const _PriceMovementConverter = PriceMovementConverter();
  static const _AlertFrequencyConverter = AlertFrequencyConverter();

  @override
  int serialize(IsarCollectionImpl<PriceAlert> collection, RawObject rawObj,
      PriceAlert object, List<int> offsets,
      [int? existingBufferSize]) {
    var dynamicSize = 0;
    final value0 = object.id;
    final _id = value0;
    final value1 = object.coinId;
    final _coinId = _utf8Encoder.convert(value1);
    dynamicSize += _coinId.length;
    final value2 = object.coinName;
    final _coinName = _utf8Encoder.convert(value2);
    dynamicSize += _coinName.length;
    final value3 = object.coinSymbol;
    final _coinSymbol = _utf8Encoder.convert(value3);
    dynamicSize += _coinSymbol.length;
    final value4 = object.currency;
    final _currency = _utf8Encoder.convert(value4);
    dynamicSize += _currency.length;
    final value5 = object.targetValue;
    final _targetValue = value5;
    final value6 = _PriceAlertAdapter._TargetValueTypeConverter.toIsar(
        object.targetValueType);
    final _targetValueType = value6;
    final value7 =
        _PriceAlertAdapter._PriceMovementConverter.toIsar(object.priceMovement);
    final _priceMovement = value7;
    final value8 = _PriceAlertAdapter._AlertFrequencyConverter.toIsar(
        object.alertFrequency);
    final _alertFrequency = value8;
    final value9 = object.dateAdded;
    final _dateAdded = value9;
    final value10 = object.lastTargetValue;
    final _lastTargetValue = value10;
    final size = dynamicSize + 90;

    late int bufferSize;
    if (existingBufferSize != null) {
      if (existingBufferSize < size) {
        malloc.free(rawObj.buffer);
        rawObj.buffer = malloc(size);
        bufferSize = size;
      } else {
        bufferSize = existingBufferSize;
      }
    } else {
      rawObj.buffer = malloc(size);
      bufferSize = size;
    }
    rawObj.buffer_length = size;
    final buffer = rawObj.buffer.asTypedList(size);
    final writer = BinaryWriter(buffer, 90);
    writer.writeLong(offsets[0], _id);
    writer.writeBytes(offsets[1], _coinId);
    writer.writeBytes(offsets[2], _coinName);
    writer.writeBytes(offsets[3], _coinSymbol);
    writer.writeBytes(offsets[4], _currency);
    writer.writeDouble(offsets[5], _targetValue);
    writer.writeLong(offsets[6], _targetValueType);
    writer.writeLong(offsets[7], _priceMovement);
    writer.writeLong(offsets[8], _alertFrequency);
    writer.writeDateTime(offsets[9], _dateAdded);
    writer.writeDouble(offsets[10], _lastTargetValue);
    return bufferSize;
  }

  @override
  PriceAlert deserialize(IsarCollectionImpl<PriceAlert> collection,
      BinaryReader reader, List<int> offsets) {
    final object = PriceAlert();
    object.id = reader.readLong(offsets[0]);
    object.coinId = reader.readString(offsets[1]);
    object.coinName = reader.readString(offsets[2]);
    object.coinSymbol = reader.readString(offsets[3]);
    object.currency = reader.readString(offsets[4]);
    object.targetValue = reader.readDouble(offsets[5]);
    object.targetValueType =
        _PriceAlertAdapter._TargetValueTypeConverter.fromIsar(
            reader.readLong(offsets[6]));
    object.priceMovement = _PriceAlertAdapter._PriceMovementConverter.fromIsar(
        reader.readLong(offsets[7]));
    object.alertFrequency =
        _PriceAlertAdapter._AlertFrequencyConverter.fromIsar(
            reader.readLong(offsets[8]));
    object.dateAdded = reader.readDateTimeOrNull(offsets[9]);
    object.lastTargetValue = reader.readDouble(offsets[10]);
    return object;
  }

  @override
  P deserializeProperty<P>(BinaryReader reader, int propertyIndex, int offset) {
    switch (propertyIndex) {
      case 0:
        return (reader.readLong(offset)) as P;
      case 1:
        return (reader.readString(offset)) as P;
      case 2:
        return (reader.readString(offset)) as P;
      case 3:
        return (reader.readString(offset)) as P;
      case 4:
        return (reader.readString(offset)) as P;
      case 5:
        return (reader.readDouble(offset)) as P;
      case 6:
        return (_PriceAlertAdapter._TargetValueTypeConverter.fromIsar(
            reader.readLong(offset))) as P;
      case 7:
        return (_PriceAlertAdapter._PriceMovementConverter.fromIsar(
            reader.readLong(offset))) as P;
      case 8:
        return (_PriceAlertAdapter._AlertFrequencyConverter.fromIsar(
            reader.readLong(offset))) as P;
      case 9:
        return (reader.readDateTimeOrNull(offset)) as P;
      case 10:
        return (reader.readDouble(offset)) as P;
      default:
        throw 'Illegal propertyIndex';
    }
  }
}

class _PriceAlertListEntryAdapter extends TypeAdapter<PriceAlertListEntry> {
  @override
  int serialize(IsarCollectionImpl<PriceAlertListEntry> collection,
      RawObject rawObj, PriceAlertListEntry object, List<int> offsets,
      [int? existingBufferSize]) {
    var dynamicSize = 0;
    final value0 = object.id;
    final _id = value0;
    final value1 = object.coinId;
    final _coinId = _utf8Encoder.convert(value1);
    dynamicSize += _coinId.length;
    final value2 = object.coinName;
    final _coinName = _utf8Encoder.convert(value2);
    dynamicSize += _coinName.length;
    final value3 = object.coinSymbol;
    final _coinSymbol = _utf8Encoder.convert(value3);
    dynamicSize += _coinSymbol.length;
    final value4 = object.listPosition;
    final _listPosition = value4;
    final value5 = object.isCollapsed;
    final _isCollapsed = value5;
    final size = dynamicSize + 43;

    late int bufferSize;
    if (existingBufferSize != null) {
      if (existingBufferSize < size) {
        malloc.free(rawObj.buffer);
        rawObj.buffer = malloc(size);
        bufferSize = size;
      } else {
        bufferSize = existingBufferSize;
      }
    } else {
      rawObj.buffer = malloc(size);
      bufferSize = size;
    }
    rawObj.buffer_length = size;
    final buffer = rawObj.buffer.asTypedList(size);
    final writer = BinaryWriter(buffer, 43);
    writer.writeLong(offsets[0], _id);
    writer.writeBytes(offsets[1], _coinId);
    writer.writeBytes(offsets[2], _coinName);
    writer.writeBytes(offsets[3], _coinSymbol);
    writer.writeLong(offsets[4], _listPosition);
    writer.writeBool(offsets[5], _isCollapsed);
    return bufferSize;
  }

  @override
  PriceAlertListEntry deserialize(
      IsarCollectionImpl<PriceAlertListEntry> collection,
      BinaryReader reader,
      List<int> offsets) {
    final object = PriceAlertListEntry();
    object.id = reader.readLong(offsets[0]);
    object.coinId = reader.readString(offsets[1]);
    object.coinName = reader.readString(offsets[2]);
    object.coinSymbol = reader.readString(offsets[3]);
    object.listPosition = reader.readLong(offsets[4]);
    object.isCollapsed = reader.readBool(offsets[5]);
    return object;
  }

  @override
  P deserializeProperty<P>(BinaryReader reader, int propertyIndex, int offset) {
    switch (propertyIndex) {
      case 0:
        return (reader.readLong(offset)) as P;
      case 1:
        return (reader.readString(offset)) as P;
      case 2:
        return (reader.readString(offset)) as P;
      case 3:
        return (reader.readString(offset)) as P;
      case 4:
        return (reader.readLong(offset)) as P;
      case 5:
        return (reader.readBool(offset)) as P;
      default:
        throw 'Illegal propertyIndex';
    }
  }
}

extension GetCollection on Isar {
  IsarCollection<LargeMoveAlert> get largeMoveAlerts {
    return getCollection('LargeMoveAlert');
  }

  IsarCollection<PriceAlert> get priceAlerts {
    return getCollection('PriceAlert');
  }

  IsarCollection<PriceAlertListEntry> get priceAlertListEntrys {
    return getCollection('PriceAlertListEntry');
  }
}

extension LargeMoveAlertQueryWhereSort on QueryBuilder<LargeMoveAlert, QWhere> {
  QueryBuilder<LargeMoveAlert, QAfterWhere> anyId() {
    return addWhereClause(WhereClause(indexName: 'id'));
  }
}

extension LargeMoveAlertQueryWhere
    on QueryBuilder<LargeMoveAlert, QWhereClause> {
  QueryBuilder<LargeMoveAlert, QAfterWhereClause> coinIdEqualTo(String coinId) {
    return addWhereClause(WhereClause(
      indexName: 'coinId',
      upper: [coinId],
      includeUpper: true,
      lower: [coinId],
      includeLower: true,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterWhereClause> coinIdNotEqualTo(
      String coinId) {
    return addWhereClause(WhereClause(
      indexName: 'coinId',
      upper: [coinId],
      includeUpper: false,
    )).addWhereClause(WhereClause(
      indexName: 'coinId',
      lower: [coinId],
      includeLower: false,
    ));
  }
}

extension PriceAlertQueryWhereSort on QueryBuilder<PriceAlert, QWhere> {
  QueryBuilder<PriceAlert, QAfterWhere> anyId() {
    return addWhereClause(WhereClause(indexName: 'id'));
  }
}

extension PriceAlertQueryWhere on QueryBuilder<PriceAlert, QWhereClause> {
  QueryBuilder<PriceAlert, QAfterWhereClause> coinIdEqualTo(String coinId) {
    return addWhereClause(WhereClause(
      indexName: 'coinId',
      upper: [coinId],
      includeUpper: true,
      lower: [coinId],
      includeLower: true,
    ));
  }

  QueryBuilder<PriceAlert, QAfterWhereClause> coinIdNotEqualTo(String coinId) {
    return addWhereClause(WhereClause(
      indexName: 'coinId',
      upper: [coinId],
      includeUpper: false,
    )).addWhereClause(WhereClause(
      indexName: 'coinId',
      lower: [coinId],
      includeLower: false,
    ));
  }
}

extension PriceAlertListEntryQueryWhereSort
    on QueryBuilder<PriceAlertListEntry, QWhere> {
  QueryBuilder<PriceAlertListEntry, QAfterWhere> anyId() {
    return addWhereClause(WhereClause(indexName: 'id'));
  }

  QueryBuilder<PriceAlertListEntry, QAfterWhere> anyIsCollapsed() {
    return addWhereClause(WhereClause(indexName: 'isCollapsed'));
  }
}

extension PriceAlertListEntryQueryWhere
    on QueryBuilder<PriceAlertListEntry, QWhereClause> {
  QueryBuilder<PriceAlertListEntry, QAfterWhereClause> coinIdEqualTo(
      String coinId) {
    return addWhereClause(WhereClause(
      indexName: 'coinId',
      upper: [coinId],
      includeUpper: true,
      lower: [coinId],
      includeLower: true,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterWhereClause> coinIdNotEqualTo(
      String coinId) {
    return addWhereClause(WhereClause(
      indexName: 'coinId',
      upper: [coinId],
      includeUpper: false,
    )).addWhereClause(WhereClause(
      indexName: 'coinId',
      lower: [coinId],
      includeLower: false,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterWhereClause> isCollapsedEqualTo(
      bool isCollapsed) {
    return addWhereClause(WhereClause(
      indexName: 'isCollapsed',
      upper: [isCollapsed],
      includeUpper: true,
      lower: [isCollapsed],
      includeLower: true,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterWhereClause> isCollapsedNotEqualTo(
      bool isCollapsed) {
    return addWhereClause(WhereClause(
      indexName: 'isCollapsed',
      upper: [isCollapsed],
      includeUpper: false,
    )).addWhereClause(WhereClause(
      indexName: 'isCollapsed',
      lower: [isCollapsed],
      includeLower: false,
    ));
  }
}

extension LargeMoveAlertQueryFilter
    on QueryBuilder<LargeMoveAlert, QFilterCondition> {
  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> idEqualTo(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> idGreaterThan(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> idLessThan(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> idBetween(
      int lower, int upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'id',
      lower: lower,
      upper: upper,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinIdEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinId',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinIdStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinId',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinIdEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinId',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinIdContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinId',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinIdMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinId',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinNameEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinName',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinNameStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinName',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinNameEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinName',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinNameContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinName',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinName',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinSymbolEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinSymbol',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinSymbolStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinSymbol',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinSymbolEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinSymbol',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinSymbolContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinSymbol',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> coinSymbolMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinSymbol',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> currencyEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'currency',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> currencyStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'currency',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> currencyEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'currency',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> currencyContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'currency',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition> currencyMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'currency',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition>
      lastNotificationSentIsNull() {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'lastNotificationSent',
      value: null,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition>
      lastNotificationSentEqualTo(DateTime? value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'lastNotificationSent',
      value: value,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition>
      lastNotificationSentGreaterThan(DateTime? value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'lastNotificationSent',
      value: value,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition>
      lastNotificationSentLessThan(DateTime? value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'lastNotificationSent',
      value: value,
    ));
  }

  QueryBuilder<LargeMoveAlert, QAfterFilterCondition>
      lastNotificationSentBetween(DateTime? lower, DateTime? upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'lastNotificationSent',
      lower: lower,
      upper: upper,
    ));
  }
}

extension PriceAlertQueryFilter on QueryBuilder<PriceAlert, QFilterCondition> {
  QueryBuilder<PriceAlert, QAfterFilterCondition> idEqualTo(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> idGreaterThan(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> idLessThan(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> idBetween(
      int lower, int upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'id',
      lower: lower,
      upper: upper,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinIdEqualTo(String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinId',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinIdStartsWith(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinId',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinIdEndsWith(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinId',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinIdContains(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinId',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinIdMatches(String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinId',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinNameEqualTo(String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinName',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinNameStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinName',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinNameEndsWith(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinName',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinNameContains(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinName',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinName',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinSymbolEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinSymbol',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinSymbolStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinSymbol',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinSymbolEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinSymbol',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinSymbolContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinSymbol',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> coinSymbolMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinSymbol',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> currencyEqualTo(String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'currency',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> currencyStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'currency',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> currencyEndsWith(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'currency',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> currencyContains(String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'currency',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> currencyMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'currency',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueGreaterThan(
      double value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'targetValue',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueLessThan(
      double value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'targetValue',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueBetween(
      double lower, double upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'targetValue',
      lower: lower,
      upper: upper,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueTypeEqualTo(
      TargetValueType value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'targetValueType',
      value: _PriceAlertAdapter._TargetValueTypeConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueTypeGreaterThan(
      TargetValueType value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'targetValueType',
      value: _PriceAlertAdapter._TargetValueTypeConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueTypeLessThan(
      TargetValueType value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'targetValueType',
      value: _PriceAlertAdapter._TargetValueTypeConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> targetValueTypeBetween(
      TargetValueType lower, TargetValueType upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'targetValueType',
      lower: _PriceAlertAdapter._TargetValueTypeConverter.toIsar(lower),
      upper: _PriceAlertAdapter._TargetValueTypeConverter.toIsar(upper),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> priceMovementEqualTo(
      PriceMovement value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'priceMovement',
      value: _PriceAlertAdapter._PriceMovementConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> priceMovementGreaterThan(
      PriceMovement value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'priceMovement',
      value: _PriceAlertAdapter._PriceMovementConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> priceMovementLessThan(
      PriceMovement value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'priceMovement',
      value: _PriceAlertAdapter._PriceMovementConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> priceMovementBetween(
      PriceMovement lower, PriceMovement upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'priceMovement',
      lower: _PriceAlertAdapter._PriceMovementConverter.toIsar(lower),
      upper: _PriceAlertAdapter._PriceMovementConverter.toIsar(upper),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> alertFrequencyEqualTo(
      AlertFrequency value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'alertFrequency',
      value: _PriceAlertAdapter._AlertFrequencyConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> alertFrequencyGreaterThan(
      AlertFrequency value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'alertFrequency',
      value: _PriceAlertAdapter._AlertFrequencyConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> alertFrequencyLessThan(
      AlertFrequency value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'alertFrequency',
      value: _PriceAlertAdapter._AlertFrequencyConverter.toIsar(value),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> alertFrequencyBetween(
      AlertFrequency lower, AlertFrequency upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'alertFrequency',
      lower: _PriceAlertAdapter._AlertFrequencyConverter.toIsar(lower),
      upper: _PriceAlertAdapter._AlertFrequencyConverter.toIsar(upper),
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> dateAddedIsNull() {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'dateAdded',
      value: null,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> dateAddedEqualTo(
      DateTime? value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'dateAdded',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> dateAddedGreaterThan(
      DateTime? value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'dateAdded',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> dateAddedLessThan(
      DateTime? value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'dateAdded',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> dateAddedBetween(
      DateTime? lower, DateTime? upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'dateAdded',
      lower: lower,
      upper: upper,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> lastTargetValueGreaterThan(
      double value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'lastTargetValue',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> lastTargetValueLessThan(
      double value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'lastTargetValue',
      value: value,
    ));
  }

  QueryBuilder<PriceAlert, QAfterFilterCondition> lastTargetValueBetween(
      double lower, double upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'lastTargetValue',
      lower: lower,
      upper: upper,
    ));
  }
}

extension PriceAlertListEntryQueryFilter
    on QueryBuilder<PriceAlertListEntry, QFilterCondition> {
  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> idEqualTo(
      int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> idGreaterThan(
      int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> idLessThan(
      int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'id',
      value: value,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> idBetween(
      int lower, int upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'id',
      lower: lower,
      upper: upper,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinIdEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinId',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinIdStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinId',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinIdEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinId',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinIdContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinId',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinIdMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinId',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinNameEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinName',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinNameStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinName',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinNameEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinName',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinNameContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinName',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinName',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinSymbolEqualTo(
      String value,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'coinSymbol',
      value: value,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinSymbolStartsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.StartsWith,
      property: 'coinSymbol',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinSymbolEndsWith(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.EndsWith,
      property: 'coinSymbol',
      value: convertedValue,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinSymbolContains(
      String value,
      {bool caseSensitive = true}) {
    final convertedValue = value;
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinSymbol',
      value: '*$convertedValue*',
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> coinSymbolMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Matches,
      property: 'coinSymbol',
      value: pattern,
      caseSensitive: caseSensitive,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> listPositionEqualTo(
      int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'listPosition',
      value: value,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition>
      listPositionGreaterThan(int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Gt,
      property: 'listPosition',
      value: value,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> listPositionLessThan(
      int value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Lt,
      property: 'listPosition',
      value: value,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> listPositionBetween(
      int lower, int upper) {
    return addFilterCondition(FilterCondition.between(
      property: 'listPosition',
      lower: lower,
      upper: upper,
    ));
  }

  QueryBuilder<PriceAlertListEntry, QAfterFilterCondition> isCollapsedEqualTo(
      bool value) {
    return addFilterCondition(FilterCondition(
      type: ConditionType.Eq,
      property: 'isCollapsed',
      value: value,
    ));
  }
}

extension LargeMoveAlertQueryLinks
    on QueryBuilder<LargeMoveAlert, QFilterCondition> {}

extension PriceAlertQueryLinks on QueryBuilder<PriceAlert, QFilterCondition> {}

extension PriceAlertListEntryQueryLinks
    on QueryBuilder<PriceAlertListEntry, QFilterCondition> {}

extension LargeMoveAlertQueryWhereSortBy
    on QueryBuilder<LargeMoveAlert, QSortBy> {
  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortById() {
    return addSortByInternal('id', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByIdDesc() {
    return addSortByInternal('id', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCoinId() {
    return addSortByInternal('coinId', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCoinIdDesc() {
    return addSortByInternal('coinId', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCoinName() {
    return addSortByInternal('coinName', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCoinNameDesc() {
    return addSortByInternal('coinName', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCoinSymbol() {
    return addSortByInternal('coinSymbol', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCoinSymbolDesc() {
    return addSortByInternal('coinSymbol', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCurrency() {
    return addSortByInternal('currency', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByCurrencyDesc() {
    return addSortByInternal('currency', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByLastNotificationSent() {
    return addSortByInternal('lastNotificationSent', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> sortByLastNotificationSentDesc() {
    return addSortByInternal('lastNotificationSent', Sort.Desc);
  }
}

extension LargeMoveAlertQueryWhereSortThenBy
    on QueryBuilder<LargeMoveAlert, QSortThenBy> {
  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenById() {
    return addSortByInternal('id', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByIdDesc() {
    return addSortByInternal('id', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCoinId() {
    return addSortByInternal('coinId', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCoinIdDesc() {
    return addSortByInternal('coinId', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCoinName() {
    return addSortByInternal('coinName', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCoinNameDesc() {
    return addSortByInternal('coinName', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCoinSymbol() {
    return addSortByInternal('coinSymbol', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCoinSymbolDesc() {
    return addSortByInternal('coinSymbol', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCurrency() {
    return addSortByInternal('currency', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByCurrencyDesc() {
    return addSortByInternal('currency', Sort.Desc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByLastNotificationSent() {
    return addSortByInternal('lastNotificationSent', Sort.Asc);
  }

  QueryBuilder<LargeMoveAlert, QAfterSortBy> thenByLastNotificationSentDesc() {
    return addSortByInternal('lastNotificationSent', Sort.Desc);
  }
}

extension PriceAlertQueryWhereSortBy on QueryBuilder<PriceAlert, QSortBy> {
  QueryBuilder<PriceAlert, QAfterSortBy> sortById() {
    return addSortByInternal('id', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByIdDesc() {
    return addSortByInternal('id', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCoinId() {
    return addSortByInternal('coinId', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCoinIdDesc() {
    return addSortByInternal('coinId', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCoinName() {
    return addSortByInternal('coinName', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCoinNameDesc() {
    return addSortByInternal('coinName', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCoinSymbol() {
    return addSortByInternal('coinSymbol', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCoinSymbolDesc() {
    return addSortByInternal('coinSymbol', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCurrency() {
    return addSortByInternal('currency', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByCurrencyDesc() {
    return addSortByInternal('currency', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByTargetValue() {
    return addSortByInternal('targetValue', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByTargetValueDesc() {
    return addSortByInternal('targetValue', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByTargetValueType() {
    return addSortByInternal('targetValueType', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByTargetValueTypeDesc() {
    return addSortByInternal('targetValueType', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByPriceMovement() {
    return addSortByInternal('priceMovement', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByPriceMovementDesc() {
    return addSortByInternal('priceMovement', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByAlertFrequency() {
    return addSortByInternal('alertFrequency', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByAlertFrequencyDesc() {
    return addSortByInternal('alertFrequency', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByDateAdded() {
    return addSortByInternal('dateAdded', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByDateAddedDesc() {
    return addSortByInternal('dateAdded', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByLastTargetValue() {
    return addSortByInternal('lastTargetValue', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> sortByLastTargetValueDesc() {
    return addSortByInternal('lastTargetValue', Sort.Desc);
  }
}

extension PriceAlertQueryWhereSortThenBy
    on QueryBuilder<PriceAlert, QSortThenBy> {
  QueryBuilder<PriceAlert, QAfterSortBy> thenById() {
    return addSortByInternal('id', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByIdDesc() {
    return addSortByInternal('id', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCoinId() {
    return addSortByInternal('coinId', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCoinIdDesc() {
    return addSortByInternal('coinId', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCoinName() {
    return addSortByInternal('coinName', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCoinNameDesc() {
    return addSortByInternal('coinName', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCoinSymbol() {
    return addSortByInternal('coinSymbol', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCoinSymbolDesc() {
    return addSortByInternal('coinSymbol', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCurrency() {
    return addSortByInternal('currency', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByCurrencyDesc() {
    return addSortByInternal('currency', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByTargetValue() {
    return addSortByInternal('targetValue', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByTargetValueDesc() {
    return addSortByInternal('targetValue', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByTargetValueType() {
    return addSortByInternal('targetValueType', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByTargetValueTypeDesc() {
    return addSortByInternal('targetValueType', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByPriceMovement() {
    return addSortByInternal('priceMovement', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByPriceMovementDesc() {
    return addSortByInternal('priceMovement', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByAlertFrequency() {
    return addSortByInternal('alertFrequency', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByAlertFrequencyDesc() {
    return addSortByInternal('alertFrequency', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByDateAdded() {
    return addSortByInternal('dateAdded', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByDateAddedDesc() {
    return addSortByInternal('dateAdded', Sort.Desc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByLastTargetValue() {
    return addSortByInternal('lastTargetValue', Sort.Asc);
  }

  QueryBuilder<PriceAlert, QAfterSortBy> thenByLastTargetValueDesc() {
    return addSortByInternal('lastTargetValue', Sort.Desc);
  }
}

extension PriceAlertListEntryQueryWhereSortBy
    on QueryBuilder<PriceAlertListEntry, QSortBy> {
  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortById() {
    return addSortByInternal('id', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByIdDesc() {
    return addSortByInternal('id', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByCoinId() {
    return addSortByInternal('coinId', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByCoinIdDesc() {
    return addSortByInternal('coinId', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByCoinName() {
    return addSortByInternal('coinName', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByCoinNameDesc() {
    return addSortByInternal('coinName', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByCoinSymbol() {
    return addSortByInternal('coinSymbol', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByCoinSymbolDesc() {
    return addSortByInternal('coinSymbol', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByListPosition() {
    return addSortByInternal('listPosition', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByListPositionDesc() {
    return addSortByInternal('listPosition', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByIsCollapsed() {
    return addSortByInternal('isCollapsed', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> sortByIsCollapsedDesc() {
    return addSortByInternal('isCollapsed', Sort.Desc);
  }
}

extension PriceAlertListEntryQueryWhereSortThenBy
    on QueryBuilder<PriceAlertListEntry, QSortThenBy> {
  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenById() {
    return addSortByInternal('id', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByIdDesc() {
    return addSortByInternal('id', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByCoinId() {
    return addSortByInternal('coinId', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByCoinIdDesc() {
    return addSortByInternal('coinId', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByCoinName() {
    return addSortByInternal('coinName', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByCoinNameDesc() {
    return addSortByInternal('coinName', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByCoinSymbol() {
    return addSortByInternal('coinSymbol', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByCoinSymbolDesc() {
    return addSortByInternal('coinSymbol', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByListPosition() {
    return addSortByInternal('listPosition', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByListPositionDesc() {
    return addSortByInternal('listPosition', Sort.Desc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByIsCollapsed() {
    return addSortByInternal('isCollapsed', Sort.Asc);
  }

  QueryBuilder<PriceAlertListEntry, QAfterSortBy> thenByIsCollapsedDesc() {
    return addSortByInternal('isCollapsed', Sort.Desc);
  }
}

extension LargeMoveAlertQueryWhereDistinct
    on QueryBuilder<LargeMoveAlert, QDistinct> {
  QueryBuilder<LargeMoveAlert, QDistinct> distinctById() {
    return addDistinctByInternal('id');
  }

  QueryBuilder<LargeMoveAlert, QDistinct> distinctByCoinId(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinId', caseSensitive: caseSensitive);
  }

  QueryBuilder<LargeMoveAlert, QDistinct> distinctByCoinName(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinName', caseSensitive: caseSensitive);
  }

  QueryBuilder<LargeMoveAlert, QDistinct> distinctByCoinSymbol(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinSymbol', caseSensitive: caseSensitive);
  }

  QueryBuilder<LargeMoveAlert, QDistinct> distinctByCurrency(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('currency', caseSensitive: caseSensitive);
  }

  QueryBuilder<LargeMoveAlert, QDistinct> distinctByLastNotificationSent() {
    return addDistinctByInternal('lastNotificationSent');
  }
}

extension PriceAlertQueryWhereDistinct on QueryBuilder<PriceAlert, QDistinct> {
  QueryBuilder<PriceAlert, QDistinct> distinctById() {
    return addDistinctByInternal('id');
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByCoinId(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinId', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByCoinName(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinName', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByCoinSymbol(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinSymbol', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByCurrency(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('currency', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByTargetValue() {
    return addDistinctByInternal('targetValue');
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByTargetValueType() {
    return addDistinctByInternal('targetValueType');
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByPriceMovement() {
    return addDistinctByInternal('priceMovement');
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByAlertFrequency() {
    return addDistinctByInternal('alertFrequency');
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByDateAdded() {
    return addDistinctByInternal('dateAdded');
  }

  QueryBuilder<PriceAlert, QDistinct> distinctByLastTargetValue() {
    return addDistinctByInternal('lastTargetValue');
  }
}

extension PriceAlertListEntryQueryWhereDistinct
    on QueryBuilder<PriceAlertListEntry, QDistinct> {
  QueryBuilder<PriceAlertListEntry, QDistinct> distinctById() {
    return addDistinctByInternal('id');
  }

  QueryBuilder<PriceAlertListEntry, QDistinct> distinctByCoinId(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinId', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlertListEntry, QDistinct> distinctByCoinName(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinName', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlertListEntry, QDistinct> distinctByCoinSymbol(
      {bool caseSensitive = true}) {
    return addDistinctByInternal('coinSymbol', caseSensitive: caseSensitive);
  }

  QueryBuilder<PriceAlertListEntry, QDistinct> distinctByListPosition() {
    return addDistinctByInternal('listPosition');
  }

  QueryBuilder<PriceAlertListEntry, QDistinct> distinctByIsCollapsed() {
    return addDistinctByInternal('isCollapsed');
  }
}

extension LargeMoveAlertQueryProperty
    on QueryBuilder<LargeMoveAlert, QQueryProperty> {
  QueryBuilder<int, QQueryOperations> idProperty() {
    return addPropertyName('id');
  }

  QueryBuilder<String, QQueryOperations> coinIdProperty() {
    return addPropertyName('coinId');
  }

  QueryBuilder<String, QQueryOperations> coinNameProperty() {
    return addPropertyName('coinName');
  }

  QueryBuilder<String, QQueryOperations> coinSymbolProperty() {
    return addPropertyName('coinSymbol');
  }

  QueryBuilder<String, QQueryOperations> currencyProperty() {
    return addPropertyName('currency');
  }

  QueryBuilder<DateTime?, QQueryOperations> lastNotificationSentProperty() {
    return addPropertyName('lastNotificationSent');
  }
}

extension PriceAlertQueryProperty on QueryBuilder<PriceAlert, QQueryProperty> {
  QueryBuilder<int, QQueryOperations> idProperty() {
    return addPropertyName('id');
  }

  QueryBuilder<String, QQueryOperations> coinIdProperty() {
    return addPropertyName('coinId');
  }

  QueryBuilder<String, QQueryOperations> coinNameProperty() {
    return addPropertyName('coinName');
  }

  QueryBuilder<String, QQueryOperations> coinSymbolProperty() {
    return addPropertyName('coinSymbol');
  }

  QueryBuilder<String, QQueryOperations> currencyProperty() {
    return addPropertyName('currency');
  }

  QueryBuilder<double, QQueryOperations> targetValueProperty() {
    return addPropertyName('targetValue');
  }

  QueryBuilder<TargetValueType, QQueryOperations> targetValueTypeProperty() {
    return addPropertyName('targetValueType');
  }

  QueryBuilder<PriceMovement, QQueryOperations> priceMovementProperty() {
    return addPropertyName('priceMovement');
  }

  QueryBuilder<AlertFrequency, QQueryOperations> alertFrequencyProperty() {
    return addPropertyName('alertFrequency');
  }

  QueryBuilder<DateTime?, QQueryOperations> dateAddedProperty() {
    return addPropertyName('dateAdded');
  }

  QueryBuilder<double, QQueryOperations> lastTargetValueProperty() {
    return addPropertyName('lastTargetValue');
  }
}

extension PriceAlertListEntryQueryProperty
    on QueryBuilder<PriceAlertListEntry, QQueryProperty> {
  QueryBuilder<int, QQueryOperations> idProperty() {
    return addPropertyName('id');
  }

  QueryBuilder<String, QQueryOperations> coinIdProperty() {
    return addPropertyName('coinId');
  }

  QueryBuilder<String, QQueryOperations> coinNameProperty() {
    return addPropertyName('coinName');
  }

  QueryBuilder<String, QQueryOperations> coinSymbolProperty() {
    return addPropertyName('coinSymbol');
  }

  QueryBuilder<int, QQueryOperations> listPositionProperty() {
    return addPropertyName('listPosition');
  }

  QueryBuilder<bool, QQueryOperations> isCollapsedProperty() {
    return addPropertyName('isCollapsed');
  }
}
