import 'dart:math';

import 'package:crypto_prices/isar.g.dart';
import 'package:crypto_prices/models/large_move_alert.dart';
import 'package:isar/isar.dart';

import '../main.dart';

///DAO of the LargeMoveAlert collection
class LargeMoveAlertDAO {
  late IsarCollection<LargeMoveAlert> _collection;
  late Isar _isar;

  LargeMoveAlertDAO() {
    _isar = isar!;
    _collection = isar!.largeMoveAlerts;
  }

  ///Returns one LargeMoveAlert by id
  LargeMoveAlert? getLargeMoveAlert(int id) {
    return _collection.getSync(id);
  }

  ///Returns one LargeMoveAlert by coinId
  LargeMoveAlert? getLargeMoveAlertByCoin(String coinId) {
    return _collection.where().coinIdEqualTo(coinId).findFirstSync();
  }

  ///Returns all LargeMoveAlerts
  List<LargeMoveAlert> getAllLargeMoveAlerts() {
    return _collection.where().findAllSync();
  }

  ///Inserts one LargeMoveAlert
  void insertLargeMoveAlert(LargeMoveAlert alert) {
    if (alert.id == 0) {
      alert.id = Random.secure().nextInt(1<<32);
    }

    _isar.writeTxnSync((isar) {
      _collection.putSync(alert);
    });
  }

  ///Deletes a LargeMoveAlert
  void deleteLargeMoveAlert(int id) {
    _isar.writeTxnSync((isar) {
      _collection.deleteSync(id);
    });
  }

  ///Deletes a LargeMoveAlert by coinId
  void deleteLargeMoveAlertByCoin(String coinId) {
    if (getLargeMoveAlertByCoin(coinId) == null)
      return;

    int id = getLargeMoveAlertByCoin(coinId)!.id;
    deleteLargeMoveAlert(id);
  }
}