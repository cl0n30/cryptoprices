import 'dart:convert';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/explorer_transaction_url.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';

///DAO of the Explorer transaction urls box
class ExplorerTransactionUrlDao {

  ///Returns the explorer transaction url for the given coin
  ExplorerTransactionUrl? getCoinExplorerTransactionUrl(String coinId) {
    var box = Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
    return box.get(coinId);
  }

  ///Returns all explorer transaction urls
  List<ExplorerTransactionUrl> getAllCoinExplorerTransactionUrls() {
    var box = Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
    return box.values.toList();
  }

  ///Returns a ValueListenable that listens for changes to the Explorer transactions box
  ValueListenable<Box<ExplorerTransactionUrl>> getAllExplorerTransactionUrlsWatcher() {
    return Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME).listenable();
  }

  ///Inserts one explorer transaction url
  void insertCoinExplorerTransactionUrl(ExplorerTransactionUrl url) {
    var box = Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
    box.put(url.coinId, url);
  }

  ///Deletes the explorer transaction url for the given coin
  void deleteCoinExplorerTransactionUrl(String coinId) {
    var box = Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
    box.delete(coinId);
  }

  ///Decodes the json file with the default urls of the explorer transactions of some coins
  ///and adds them to the database
  void decodeDefaultTransactionUrls() async {
    String urlsJson = await rootBundle.loadString("assets/defaultExplorerTransactionUrls.json");
    List jsonUrls = jsonDecode(urlsJson);
    Map<String , ExplorerTransactionUrl> urls = {};

    jsonUrls.forEach((element) {
      final url = ExplorerTransactionUrl.fromJson(element);
      urls[url.coinId] = url;
    });

    var box = Hive.box<ExplorerTransactionUrl>(Constants.EXPLORERSTRANSACTIONBOXNAME);
    box.putAll(urls);
  }
}