import 'package:crypto_prices/models/price_alert.dart';
import 'package:isar/isar.dart';

class TargetValueTypeConverter extends TypeConverter<TargetValueType, int> {
  const TargetValueTypeConverter();

  @override
  TargetValueType fromIsar(int targetValueTypeIndex) {
    return TargetValueType.values[targetValueTypeIndex];
  }

  @override
  int toIsar(TargetValueType targetValueType) {
    return targetValueType.index;
  }
}

class PriceMovementConverter extends TypeConverter<PriceMovement, int> {
  const PriceMovementConverter();

  @override
  PriceMovement fromIsar(int priceMovementIndex) {
    return PriceMovement.values[priceMovementIndex];
  }

  @override
  int toIsar(PriceMovement priceMovement) {
    return priceMovement.index;
  }
}

class AlertFrequencyConverter extends TypeConverter<AlertFrequency, int> {
  const AlertFrequencyConverter();

  @override
  AlertFrequency fromIsar(int alertFrequencyindex) {
    return AlertFrequency.values[alertFrequencyindex];
  }

  @override
  int toIsar(AlertFrequency alertFrequency) {
    return alertFrequency.index;
  }
}