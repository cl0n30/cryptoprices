import 'dart:math';

import 'package:crypto_prices/database/coin_comparators.dart';
import 'package:crypto_prices/database/price_alert_list_entry_dao.dart';
import 'package:crypto_prices/isar.g.dart';
import 'package:crypto_prices/main.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:isar/isar.dart';

///DAO of the PriceAlert box
class PriceAlertDao {
  late IsarCollection<PriceAlert> _collection;
  late Isar _isar;

  PriceAlertDao() {
    _isar = isar!;
    _collection = isar!.priceAlerts;
  }

  ///Returns one PriceAlert by id
  PriceAlert? getPriceAlert(int id) {
    return _collection.getSync(id); //if entry doesn't exist, returns null
  }

  ///Returns all PriceAlert of a coin
  List<PriceAlert> getAllCoinPriceAlerts(String coinId) {
    PriceAlertListEntryDao priceAlertListEntryDao = PriceAlertListEntryDao();
    if (priceAlertListEntryDao.getEntry(coinId) == null) {
      return [];
    } else {
      return _collection.where().coinIdEqualTo(coinId).findAllSync().toList()
        ..sort(CoinComparators.priceAlertDateAdded);
    }
  }

  ///Returns the ids of all PriceAlert of a coin
  List<int> getAllCoinPriceAlertIds(String coinId) {
    PriceAlertListEntryDao priceAlertListEntryDao = PriceAlertListEntryDao();
    if (priceAlertListEntryDao.getEntry(coinId) == null) {
      return [];
    } else {
      return _collection.where().coinIdEqualTo(coinId).idProperty().findAllSync().toList();
    }
  }

  ///Return all PriceAlerts
  List<PriceAlert> getAllPriceAlerts() {
    return _collection.where().findAllSync().toList();
  }

  ///Returns a stream that listens for database changes and returns the new list on change
  Stream<List<PriceAlert>> getAllPriceAlertsChangeWatcher() {
    return _collection.where().watch();
  }

  ///Inserts one PriceAlert. If it is the first alert for a coin also adds an entry in the
  ///price alert list
  void insertPriceAlert(PriceAlert alert) {
    PriceAlertListEntryDao priceAlertListEntryDao = PriceAlertListEntryDao();
    if (priceAlertListEntryDao.getEntry(alert.coinId) == null) {
      PriceAlertListEntry alertListEntry = PriceAlertListEntry(
        coinId: alert.coinId,
        coinName: alert.coinName,
        coinSymbol: alert.coinSymbol,
        listPosition: priceAlertListEntryDao.getAllEntries().length
      );

      priceAlertListEntryDao.insertEntry(alertListEntry);
    }

    if (alert.id == 0) {
      alert.id = Random.secure().nextInt(1<<32);
    }

    _isar.writeTxnSync((isar) {
      _collection.putSync(alert);
    });
  }

  ///Inserts multiple PriceAlerts from a list If it is the first alert for a coin also adds an entry in the
  ///price alert list
  void insertAllPriceAlerts(List<PriceAlert> alerts) {
    PriceAlertListEntryDao priceAlertListEntryDao = PriceAlertListEntryDao();
    for (int i = 0; i < alerts.length; i++) {
      if (priceAlertListEntryDao.getEntry(alerts[i].coinId) == null) {
        PriceAlertListEntry alertListEntry = PriceAlertListEntry(
            coinId: alerts[i].coinId,
            coinName: alerts[i].coinName,
            coinSymbol: alerts[i].coinSymbol,
            listPosition: priceAlertListEntryDao.getAllEntries().length
        );

        priceAlertListEntryDao.insertEntry(alertListEntry);
      }

      if (alerts[i].id == 0) {
        alerts[i].id = Random.secure().nextInt(1<<32);
      }
    }

    _isar.writeTxnSync((isar) {
      _collection.putAllSync(alerts);
    });
  }

  ///Deletes a PriceAlert. If it is the last alert for a coin also remove the entry in the
  ///price alert list
  void deletePriceAlert(int id) {
    PriceAlertListEntryDao priceAlertListEntryDao = PriceAlertListEntryDao();
    PriceAlert? alert = getPriceAlert(id);
    if (alert == null)
      return;

    PriceAlertListEntry entry = priceAlertListEntryDao.getEntry(alert.coinId)!;
    if (getAllCoinPriceAlerts(alert.coinId).length == 1) { //only this alert remaining
      priceAlertListEntryDao.deleteEntry(entry.coinId);
    }

    _isar.writeTxnSync((isar) {
      _collection.deleteSync(id);
    });
  }

  ///Deletes all PriceAlerts of a coin and its entry in the price alert list
  void deleteAllCoinPriceAlerts(String coinId) {
    _isar.writeTxnSync((isar) {
      final ids = _collection.where().coinIdEqualTo(coinId).idProperty().findAllSync();
      _collection.deleteAllSync(ids);
    });
    PriceAlertListEntryDao().deleteEntry(coinId);
  }

  ///Deletes all PriceAlerts
  void deleteAllAlerts() {
    _isar.writeTxnSync((isar) {
      final ids = _collection.where().idProperty().findAllSync();
      _collection.deleteAllSync(ids);
    });
    PriceAlertListEntryDao().deleteAllEntries();
  }
}