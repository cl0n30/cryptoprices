import 'dart:math';

import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/wallet_dao.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/models/transaction.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../constants.dart';
import '../util/util.dart';
import 'favorites_dao.dart';
import 'settings_dao.dart';

///the DAO of the TransactionsBox
class TransactionDao {

  ///Returns one transaction
  Transaction? getTransaction(int id) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    return box.get(id);
  }

  ///Returns multiple transactions with the given ids
  List<Transaction> getMultipleTransactions(List<int> ids) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    List<Transaction> list = [];
    ids.forEach((id) {
      if (box.containsKey(id))
        list.add(box.get(id)!);
    });
    return list;
  }

  ///Returns all transactions
  List<Transaction> getAllTransactions() {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    return box.values.toList();
  }

  ///Returns a ValueListenable that listens for changes to the TransactionsBox
  ValueListenable<Box<Transaction>> getAllTransactionsWatcher() {
    return Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME).listenable();
  }

  ///Inserts one transaction into a portfolio.
  ///If it is the first transaction for a coin in the portfolio also adds an entry in the wallet list
  void insertTransaction(Transaction transaction, Portfolio portfolio) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    WalletDao walletDao = WalletDao();

    if (transaction.id == 0)
      transaction.id = Util.generateId();

    box.put(transaction.id, transaction); //HiveList needs objects in a box

    if (portfolio.getPortfolioWalletByCoinId(transaction.coinId) == null) {
      final walletId = Util.generateId();
      walletDao.insertWallet(
        Wallet(
          walletId,
          transaction.coinId,
          portfolio.id,
          coinName: transaction.coinName,
          coinSymbol: transaction.coinSymbol,
          listPosition: walletDao.getAllWallets().length
        )..allTransactions = [transaction]
      );

      transaction.connectedWalletId = walletId;
      transaction.save();
    } else {
      final wallet = portfolio.getPortfolioWalletByCoinId(transaction.coinId)!;
      if (!wallet.allTransactions.contains(transaction)) { //no update, add new transaction
        transaction.connectedWalletId = wallet.id;
        wallet.allTransactions.add(transaction);
        transaction.save();
        wallet.save();
      }
    }
  }

  ///Inserts multiple transactions into one portfolio.
  ///Adds a wallet if one doesn't exist
  void insertMultipleTransaction(List<Transaction> transactions, Portfolio portfolio) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    WalletDao walletDao = WalletDao();

    transactions.forEach((transaction) {
      if (transaction.id == 0)
        transaction.id = Util.generateId();

      box.put(transaction.id, transaction); //transaction has to be in database to be added to a wallet below

      if (portfolio.getPortfolioWalletByCoinId(transaction.coinId) == null) {
        final walletId = Util.generateId();
        walletDao.insertWallet(
          Wallet(
            walletId,
            transaction.coinId,
            portfolio.id,
            coinName: transaction.coinName,
            coinSymbol: transaction.coinSymbol,
            listPosition: walletDao.getAllWallets().length
          )..allTransactions = [transaction]
        );

        transaction.connectedWalletId = walletId;
        transaction.save();
      } else {
        final wallet = portfolio.getPortfolioWalletByCoinId(transaction.coinId)!;
        if (!wallet.allTransactions.contains(transaction)) { //no update, add new transaction
          transaction.connectedWalletId = wallet.id;
          wallet.allTransactions.add(transaction);
          transaction.save();
          wallet.save();
        }
      }
    });
  }

  ///Deletes one transaction. If it is the last transaction of a wallet, deletes this too
  void deleteTransaction(int id) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    WalletDao walletDao = WalletDao();
    if (getTransaction(id) == null)
      return;

    Transaction oldTransaction = getTransaction(id)!;
    if (walletDao.getWallet(oldTransaction.connectedWalletId)!.allTransactions.length == 1) {
      walletDao.deleteWallet(oldTransaction.connectedWalletId);
    }

    box.delete(id);
  }

  ///Deletes multiple transactions
  void deleteMultipleTransactions(List<int> ids) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    box.deleteAll(ids);
  }

  ///Deletes all transactions of a coin
  void deleteAllCoinTransactions(String coinId) {
    var box = Hive.box<Transaction>(Constants.TRANSACTIONSBOXNAME);
    final all = getAllTransactions();
    final toDelete = all.where((element) => element.coinId == coinId).toList();
    final ids = toDelete.map((e) => e.id).toList();
    box.deleteAll(ids);
  }
}