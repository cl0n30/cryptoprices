import 'package:crypto_prices/models/available_coin.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/portfolio.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:crypto_prices/models/price_alert.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:crypto_prices/models/transaction.dart';

///Comparator functions to sort lists of Coins by various values
class CoinComparators {
  ///Sorts Coins descending by marketCap value
  static int marketCapDesc(Coin c1, Coin c2) {
    if (c1.marketCap > c2.marketCap) {
      return -1;
    } else if (c1.marketCap < c2.marketCap) {
      return 1;
    } else {
      return 0;
    }
  }

  ///Sorts Coins ascending by marketCap value
  static int marketCapAsc(Coin c1, Coin c2) {
    if (c1.marketCap > c2.marketCap) {
      return 1;
    } else if (c1.marketCap < c2.marketCap) {
      return -1;
    } else {
      return 0;
    }
  }

  ///Sorts Coins descending by volume value
  static int volumeDesc(Coin c1, Coin c2) {
    if (c1.totalVolume > c2.totalVolume) {
      return -1;
    } else if (c1.totalVolume < c2.totalVolume) {
      return 1;
    } else {
      return 0;
    }
  }

  ///Sorts Coins ascending by volume value
  static int volumeAsc(Coin c1, Coin c2) {
    if (c1.totalVolume > c2.totalVolume) {
      return 1;
    } else if (c1.totalVolume < c2.totalVolume) {
      return -1;
    } else {
      return 0;
    }
  }

  ///Sorts Coins descending by price value
  static int priceChangeDesc(Coin c1, Coin c2) {
    if (c1.priceChangePercentage24h > c2.priceChangePercentage24h) {
      return -1;
    } else if (c1.priceChangePercentage24h < c2.priceChangePercentage24h) {
      return 1;
    } else {
      return 0;
    }
  }

  ///Sorts Coins ascending by price value
  static int priceChangeAsc(Coin c1, Coin c2) {
    if (c1.priceChangePercentage24h > c2.priceChangePercentage24h) {
      return 1;
    } else if (c1.priceChangePercentage24h < c2.priceChangePercentage24h) {
      return -1;
    } else {
      return 0;
    }
  }

  ///Sorts Coins descending by name value
  static int nameDesc(Coin c1, Coin c2) {
    switch (c1.name.toLowerCase().compareTo(c2.name.toLowerCase())) {
      case -1 :
        return 1;
      case 1 :
        return -1;
      default :
        return 0;
    }
  }

  ///Sorts Coins ascending by name value
  static int nameAsc(Coin c1, Coin c2) {
    switch (c1.name.toLowerCase().compareTo(c2.name.toLowerCase())) {
      case -1 :
        return -1;
      case 1 :
        return 1;
      default :
        return 0;
    }
  }

  ///Sorts Coins descending by symbol value
  static int symbolDesc(Coin c1, Coin c2) {
    switch (c1.symbol.compareTo(c2.symbol)) {
      case -1 :
        return 1;
      case 1 :
        return -1;
      default :
        return 0;
    }
  }

  ///Sorts Coins ascending by symbol value
  static int symbolAsc(Coin c1, Coin c2) {
    switch (c1.symbol.compareTo(c2.symbol)) {
      case -1 :
        return -1;
      case 1 :
        return 1;
      default :
        return 0;
    }
  }

  ///Sorts FavoriteCoins by favoriteListPosition value ascending
  static int favoriteListPosition(FavoriteCoin c1, FavoriteCoin c2) {
    if (c1.favoriteListPosition > c2.favoriteListPosition) {
      return 1;
    } else if (c1.favoriteListPosition < c2.favoriteListPosition) {
      return -1;
    } else {
      return 0;
    }
  }

  ///Sorts PriceAlertListEntries by listPosition value ascending
  static int priceAlertListPosition(PriceAlertListEntry e1, PriceAlertListEntry e2) {
    if (e1.listPosition > e2.listPosition) {
      return 1;
    } else if (e1.listPosition < e2.listPosition) {
      return -1;
    } else {
      return 0;
    }
  }

  ///Sorts PriceAlerts by dateAdded from later > earlier
  static int priceAlertDateAdded(PriceAlert a1, PriceAlert a2) {
    if (a1.dateAdded == null && a2.dateAdded == null)
      return 0;
    if (a1.dateAdded == null)
      return -1;
    if (a2.dateAdded == null)
      return 1;

    if (a1.dateAdded!.isAfter(a2.dateAdded!)) {
      return 1;
    } else if (a1.dateAdded!.isBefore(a2.dateAdded!)) {
      return -1;
    } else {
      return 0;
    }
  }

  ///Sorts AvailableCoins by lastSearched with more recent entries first
  static int lastSearched(AvailableCoin c1, AvailableCoin c2) {
    if (c1.lastSearched == null && c2.lastSearched == null)
      return 0;
    if (c1.lastSearched == null)
      return 1;
    if (c2.lastSearched == null)
      return -1;
    
    if (c1.lastSearched!.isAfter(c2.lastSearched!)) {
      return -1;
    } else if (c2.lastSearched!.isAfter(c1.lastSearched!)) {
      return 1;
    } else {
      return 0;
    }
  }

  ///Sorts DateTimes with more recent dates first
  static int mostRecentDateFirst(DateTime? d1, DateTime? d2) {
    if (d1 == null && d2 == null)
      return 0;
    if (d1 == null)
      return 1;
    if (d2 == null)
      return -1;

    if (d1.isAfter(d2)) {
      return -1;
    } else if (d2.isAfter(d1)) {
      return 1;
    } else {
      return 0;
    }
  }

  ///Sorts transactions with more recent transaction dates first
  static int transactionDateMostRecentFirst(Transaction t1, Transaction t2) {
    if (t1.transactionDate == null && t2.transactionDate == null)
      return 0;
    if (t1.transactionDate == null)
      return 1;
    if (t2.transactionDate == null)
      return -1;

    if (t1.transactionDate!.isAfter(t2.transactionDate!)) {
      return -1;
    } else if (t2.transactionDate!.isAfter(t1.transactionDate!)) {
      return 1;
    } else {
      return 0;
    }
  }

  ///Sorts wallets descending by their total value
  static int walletTotalValueDesc(Wallet w1, Wallet w2) {
    if (w1.totalValue > w2.totalValue)
      return -1;
    else if (w1.totalValue < w2.totalValue)
      return 1;
    else
      return 0;
  }

  ///Sorts wallets ascending by their total value
  static int walletTotalValueAsc(Wallet w1, Wallet w2) {
    if (w1.totalValue > w2.totalValue)
      return 1;
    else if (w1.totalValue < w2.totalValue)
      return -1;
    else
      return 0;
  }

  ///Sorts wallet descending by coin name
  static int walletCoinNameDesc(Wallet w1, Wallet w2) {
    switch (w1.coinName.toLowerCase().compareTo(w2.coinName.toLowerCase())) {
      case -1 :
        return 1;
      case 1 :
        return -1;
      default :
        return 0;
    }
  }

  ///Sorts wallet ascending by coin name
  static int walletCoinNameAsc(Wallet w1, Wallet w2) {
    switch (w1.coinName.toLowerCase().compareTo(w2.coinName.toLowerCase())) {
      case -1 :
        return -1;
      case 1 :
        return 1;
      default :
        return 0;
    }
  }

  ///Sorts portfolios ascending by name
  static int portfolioNameAsc(Portfolio p1, Portfolio p2) {
    switch (p1.name.toLowerCase().compareTo(p2.name.toLowerCase())) {
      case -1 :
        return -1;
      case 1 :
        return 1;
      default :
        return 0;
    }
  }
}