import 'package:crypto_prices/api/api_interaction.dart';
import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/market_data.dart';
import 'package:hive_flutter/hive_flutter.dart';

///DAO of the MarketData box
class MarketDataDao {

  ///Returns one MarketData by id
  MarketData? getMarketData(String id) {
    var box = Hive.box(Constants.MARKETDATABOXNAME);
    return box.get(id); //if entry doesn't exist, returns null
  }

  ///Returns the MarketData with the given currency for the given time period of the given coin
  MarketData? getCoinMarketDataForPeriod(String coinId, String timePeriod, String currency) {
    return getMarketData(coinId + timePeriod + currency);
  }

  ///Returns the MarketData with the given currency for the given time period of the given coins
  List<MarketData> getMultipleCoinMarketDataForPeriod(List<String> coinIds, String timePeriod, String currency) {
    List<MarketData> list = [];
    coinIds.forEach((id) {
      final marketData = getCoinMarketDataForPeriod(id, timePeriod, currency);
      if (marketData != null) {
        list.add(marketData);
      }
    });
    return list;
  }

  ///Returns all MarketData available for the given coin with the given currency.
  ///Can be empty if data does not exist
  List<MarketData> getCoinMarketChart(String coinId, String currency) {
    List<MarketData> list = [];
    marketDataDays.forEach((period) {
      final marketData = getCoinMarketDataForPeriod(coinId, period, currency);
      if (marketData != null)
        list.add(marketData);
    });
    return list;
  }

  ///Inserts one MarketData
  void insertMarketData(MarketData marketData) {
    var box = Hive.box(Constants.MARKETDATABOXNAME);
    marketData.id = _generateId(marketData);
    box.put(marketData.id, marketData);
  }

  ///Inserts multiple MarketData
  void insertMultipleMarketData(List<MarketData> marketData) {
    var box = Hive.box(Constants.MARKETDATABOXNAME);
    Map<String, MarketData> map = {};
    for (int i = 0; i < marketData.length; i++) {
      marketData[i].id = _generateId(marketData[i]);
      map[marketData[i].id] = marketData[i];
    }
    box.putAll(map);
  }

  ///Generates an id for the MarketDAta objects.
  ///id = coinId + timePeriodDays + currency
  String _generateId(MarketData marketData) {
    return marketData.coinId + marketData.timePeriodDays + marketData.currency;
  }
}