import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/models/coin.dart';
import 'package:hive/hive.dart';

///DAO of the Coins bpx
class CoinsDao {

  ///Returns one Coin by id
  Coin? getCoin(String id) {
    var box = Hive.box(Constants.COINSBOXNAME);
    return box.get(id); //if entry doesn't exist, returns null
  }

  ///Returns all Coins from the box
  List<Coin> getAllCoins() {
    var box = Hive.box(Constants.COINSBOXNAME);
    List<Coin> list = [];
    for (int i = 0; i < box.length; i++) {
      if (box.getAt(i) is Coin) {
        list.add(box.getAt(i));
      }
    }
    return list;
  }

  ///Returns multiple coins by their ids from a list
  List<Coin> getMultipleCoins(List<String> ids) {
    var box = Hive.box(Constants.COINSBOXNAME);
    List<Coin> list = [];
    for (int i = 0; i < ids.length; i++) {
      list.add(box.get(ids[i]));
    }
    return list;
  }

  ///Inserts one Coin
  void insertCoin(Coin coin) {
    var box = Hive.box(Constants.COINSBOXNAME);
    box.put(coin.id, coin);
  }

  ///Inserts multiple Coins from a list
  void insertAllCoins(List<Coin> list) {
    var box = Hive.box(Constants.COINSBOXNAME);
    Map<String, Coin> coins = Map();

    for (int i = 0; i < list.length; i++) {
      coins[list[i].id] = list[i];
    }
    box.putAll(coins);
  }

  ///Deletes one Coin from the box by its id
  void deleteCoin(String id) {
    var box = Hive.box(Constants.COINSBOXNAME);
    box.delete(id);
  }

  ///Deletes all Coins
  void deleteAllEntries() {
    var box = Hive.box(Constants.COINSBOXNAME);
    box.clear();
  }

  ///Sets if the currency for the coins has been changed (in CoinsList)
  void setCurrencyChanged(bool changed) {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    box.put(Constants.COINSCURRENCYCHANGEDKEY, changed);
  }

  ///Returns if the currency for the coins has been changed (in CoinsList)
  bool getCurrencyChanged() {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    return (box.get(Constants.COINSCURRENCYCHANGEDKEY)) == null ? true : box.get(Constants.COINSCURRENCYCHANGEDKEY);
  }
}