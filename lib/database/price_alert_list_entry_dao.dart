import 'dart:math';

import 'package:crypto_prices/database/price_alert_dao.dart';
import 'package:crypto_prices/isar.g.dart';
import 'package:crypto_prices/main.dart';
import 'package:crypto_prices/models/price_alert_list_entry.dart';
import 'package:isar/isar.dart';

import 'coin_comparators.dart';

///DAO of the PriceAlertListEntries box.
///This box contains all coins (ids) that currently have alerts scheduled
class PriceAlertListEntryDao {
  late IsarCollection<PriceAlertListEntry> _collection;
  late Isar _isar;

  PriceAlertListEntryDao() {
    _isar = isar!;
    _collection = isar!.priceAlertListEntrys;
  }

  ///Returns the PriceAlertListEntry by coin id
  PriceAlertListEntry? getEntry(String coinId) {
    //should be only one entry per coinId
    final id = _collection.where().filter().coinIdEqualTo(coinId).idProperty().findFirstSync();
    if (id == null) {
      return null;
    } else {
      return _collection.getSync(id);
    }
  }

  ///Returns all PriceAlertListEntries from the box sorted by listPosition
  List<PriceAlertListEntry> getAllEntries() {
    return _collection.where().findAllSync()..sort(CoinComparators.priceAlertListPosition);
  }

  ///Returns a stream that listens for database changes and returns the new list on change
  Stream<List<PriceAlertListEntry>> getAllEntriesChangeWatcher() {
    return _collection.where().watch();
  }

  ///Inserts one PriceAlertListEntry
  void insertEntry(PriceAlertListEntry entry) {
    if (entry.id == 0) {
      entry.id = Random.secure().nextInt(1<<32);
    }
    _isar.writeTxnSync((isar) {
      _collection.putSync(entry);
    });
  }

  ///deletes one PriceAlertListEntry and its PriceAlerts from the box by id
  void deleteEntry(String coinId) {
    final oldEntry = _collection.where().filter().coinIdEqualTo(coinId).findFirstSync();
    if (oldEntry == null)
      return;

    _decrementPositionIndices(oldEntry.listPosition);
    _isar.writeTxnSync((isar) {
      _collection.deleteSync(oldEntry.id);
    });

    PriceAlertDao().deleteAllCoinPriceAlerts(oldEntry.coinId);
  }

  ///Deletes all PriceAlertListEntries and PriceAlerts
  void deleteAllEntries() {
    PriceAlertDao().deleteAllAlerts();
    _isar.writeTxnSync((isar) {
      final ids = _collection.where().idProperty().findAllSync();
      _collection.deleteAllSync(ids);
    });
  }

  ///Checks if all entries are collapsed or not
  bool areAllCollapsed() {
    return (_collection.where().isCollapsedEqualTo(false).findAllSync().length) == 0;
  }

  ///Returns a stream that listens for changes of all entries that are not collapsed
  Stream<List<PriceAlertListEntry>> getAllNonCollapsedEntriesWatcher() {
    return _collection.where().isCollapsedEqualTo(false).watch();
  }

  ///increments the position indices of all entries between the new item position
  ///and the old item position.
  ///Used when moving an item up in the list
  void moveDownAllBetween(int newPos, int oldPos) {
    final list = _collection.where().filter()
      .group((entry) => entry
        .listPositionGreaterThan(newPos)
        .or()
        .listPositionEqualTo(newPos)
      )
      .and()
      .listPositionLessThan(oldPos)
      .findAllSync();

    for (int i = 0; i < list.length; i++) {
      list[i].listPosition++;
    }

    _isar.writeTxnSync((isar) {
      _collection.putAllSync(list);
    });
  }

  ///decrements the position indices of all entries between the old item position
  ///and the new item position.
  ///Used when moving an item down in the list
  void moveUpAllBetween(int oldPos, int newPos) {
    final list = _collection.where().filter()
      .listPositionGreaterThan(oldPos)
      .and()
      .group((entry) => entry
        .listPositionLessThan(newPos)
        .or()
        .listPositionEqualTo(newPos)
      )
      .findAllSync();

    for (int i = 0; i < list.length; i++) {
      list[i].listPosition--;
    }

    _isar.writeTxnSync((isar) {
      _collection.putAllSync(list);
    });
  }

  ///decrements the position indices of all entries below the deleted entry.
  ///Moves them up by one
  void _decrementPositionIndices(int oldIndex) {
    List<PriceAlertListEntry> list = _collection.where().findAllSync();
    for (int i = 0; i < list.length; i++) {
      if (list[i].listPosition > oldIndex) {
        list[i].listPosition--;
      }
    }
    _isar.writeTxnSync((isar) {
      _collection.putAllSync(list);
    });
  }
}