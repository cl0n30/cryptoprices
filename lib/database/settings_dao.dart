import 'dart:io';

import 'package:crypto_prices/changeNotifiers/theme_change.dart';
import 'package:crypto_prices/constants.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

///DAO for the settings data key-value pairs. Contains methods to get values from preferences or from Hive database.
///Only use Hive in the app and use preferences for background work
class SettingsDAO {

  ///Sets the locale value
  void setLocale(String locale) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.LOCALEKEY, locale);
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.LOCALEKEY, locale);
  }

  ///Gets the locale value from preferences (use in background work)
  Future<String> getLocalePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.LOCALEKEY)) {
      return Constants.LOCALEDEFAULT;
    } else {
      return prefs.getString(Constants.LOCALEKEY)!;
    }
  }

  ///Gets the locale value from Hive (use in the app process)
  String getLocaleHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.LOCALEKEY, defaultValue: Constants.LOCALEDEFAULT);
  }

  ///Sets the currency value
  void setCurrency(String currency) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.CURRENCYKEY, currency);
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.CURRENCYKEY, currency);
  }

  ///Gets the currency value from preferences (use in background work)
  Future<String> getCurrencyPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.CURRENCYKEY)) {
      return Constants.CURRENCYDEFAULT;
    } else {
      return prefs.getString(Constants.CURRENCYKEY)!;
    }
  }

  ///Gets the currency value from Hive (use in the app process)
  String getCurrencyHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.CURRENCYKEY, defaultValue: Constants.CURRENCYDEFAULT);
  }

  ///Gets the current theme from Hive (use in the app process)
  Themes getCurrentThemeHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    if (!box.containsKey(Constants.themeKey)) {
      if (box.containsKey(Constants.SYSTEMDARKMODEKEY) && !box.get(Constants.SYSTEMDARKMODEKEY)) { //old system without theme key
        if (box.get(Constants.DARKMODEKEY)) {
          return Themes.DARK;
        } else {
          return Themes.LIGHT;
        }
      }

      int _sdkNumber = androidDeviceInfo?.version.sdkInt ?? 28;
      if (_sdkNumber >= 29) { //if < 29 don't use system default (light theme)
        return Themes.SYSTEM;
      } else {
        return Themes.DARK;
      }
    } else {
      return Themes.values[box.get(Constants.themeKey)];
    }
  }

  ///Sets the current theme
  void setCurrentTheme(Themes theme) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.themeKey, theme.index);
  }

  ///Sets the marketDataLastUpdated value of a coin
  void setMarketDataLastUpdated(String coinId, DateTime marketDataLastUpdated) {
    Hive.box(Constants.SETTINGSBOXNAME).put(coinId + Constants.MARKETDATALASTUPDATEDKEY, marketDataLastUpdated);
  }

  ///Gets the marketDataLastUpdated value for a coin from Hive (use in the app process)
  DateTime? getMarketDataLastUpdatedHive(String coinId) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(coinId + Constants.MARKETDATALASTUPDATEDKEY);
  }

  ///Sets the coinsLastUpdated value
  void setCoinsLastUpdated(DateTime coinsLastUpdated) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.COINSLASTUPDATEDKEY, coinsLastUpdated);
  }

  ///Gets the coinsLastUpdated value from Hive (use in the app process)
  DateTime? getCoinsLastUpdatedHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.COINSLASTUPDATEDKEY);
  }

  ///Sets the favoritesLastUpdated value
  void setFavoritesLastUpdated(DateTime favoritesLastUpdated) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.FAVORITESLASTUPDATEDKEY, favoritesLastUpdated);
  }

  ///Gets the favoritesLastUpdated value from Hive (use in the app process)
  DateTime? getFavoritesLastUpdatedHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.FAVORITESLASTUPDATEDKEY);
  }

  ///Sets the availableCoinsLastUpdated value
  void setAvailableCoinsLastUpdated(DateTime availableCoinsLastUpdated) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.AVAILABLECOINSLASTUPDATEDKEY, availableCoinsLastUpdated);
  }

  ///Gets the availableCoinsLastUpdated value from Hive (use in the app process)
  DateTime? getAvailableCoinsLastUpdated() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.AVAILABLECOINSLASTUPDATEDKEY);
  }

  ///Sets the walletsLastUpdated value
  void setWalletsLastUpdated(DateTime WalletsLastUpdated) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.WALLETSLASTUPDETEDKEY, WalletsLastUpdated);
  }

  ///Gets the walletsLastUpdated value from Hive (use in the app process)
  DateTime? getWalletsLastUpdatedHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.WALLETSLASTUPDETEDKEY);
  }

  ///Sets the globalMarketInfoLastUpdated value
  void setGlobalMarketInfoLastUpdated(DateTime globalMarketInfoLastUpdated) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.GLOBALMARKETINFOLASTUPDATEDKEY, globalMarketInfoLastUpdated);
  }

  ///Gets the globalMarketInfoLastUpdated value from Hive (use in the app process)
  DateTime? getGlobalMarketInfoLastUpdatedHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.GLOBALMARKETINFOLASTUPDATEDKEY);
  }

  ///Sets the LargeMoveAlertThreshold value
  void setLargeMoveAlertThreshold(double threshold) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble(Constants.LARGEMOVEALERTLIMITKEY, threshold);
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.LARGEMOVEALERTLIMITKEY, threshold);
  }

  ///Gets the LargeMoveAlertThreshold value from preferences (use in background work)
  Future<double> getLargeMoveAlertThresholdPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.LARGEMOVEALERTLIMITKEY)) {
      return Constants.LARGEMOVEALERTTHRESHOLDDEFAULT;
    } else {
      return prefs.getDouble(Constants.LARGEMOVEALERTLIMITKEY)!;
    }
  }

  ///Gets the LargeMoveAlertThreshold value from Hive (use in the app process)
  double getLargeMoveAlertThresholdHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.LARGEMOVEALERTLIMITKEY, defaultValue: Constants.LARGEMOVEALERTTHRESHOLDDEFAULT)!;
  }

  ///Sets the LargeMoveAlertsEnabled value
  void setLargeMoveAlertsEnabled(bool enabled) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(Constants.LARGEMOVEALERTENABLEDKEY, enabled);
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.LARGEMOVEALERTENABLEDKEY, enabled);
  }

  ///Gets the LargeMoveAlertsEnabled value from preferences (use in background work)
  Future<bool> getLargeMoveAlertsEnabledPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.LARGEMOVEALERTENABLEDKEY)) {
      return Constants.LARGEMOVEALERTENABLEDDEFAULT;
    } else {
      return prefs.getBool(Constants.LARGEMOVEALERTENABLEDKEY)!;
    }
  }

  ///Gets the LargeMoveAlertsEnabled value from Hive (use in the app process)
  bool getLargeMoveAlertsEnabledHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.LARGEMOVEALERTENABLEDKEY, defaultValue: Constants.LARGEMOVEALERTENABLEDDEFAULT)!;
  }

  ///Sets if the coin price should be shown in a percentage alert
  void setShowPriceInPercentageAlerts(bool showPrice) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(Constants.showPriceInPercentageAlertKey, showPrice);
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.showPriceInPercentageAlertKey, showPrice);
  }

  ///Gets if the coin price should be shown in a percentage alert from Hive (use in the app process)
  bool getShowPriceInPercentageAlertsHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.showPriceInPercentageAlertKey, defaultValue: Constants.showPriceInPercentageAlertDefault);
  }

  ///Gets if the coin price should be shown in a percentage alert from preferences (use in background work)
  Future<bool> getShowPriceInPercentageAlertsPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.showPriceInPercentageAlertKey)) {
      return Constants.showPriceInPercentageAlertDefault;
    } else {
      return prefs.getBool(Constants.showPriceInPercentageAlertKey)!;
    }
  }

  ///Sets the primary color value
  void setPrimaryColor(Color color) {
    Hive.box(Constants.SETTINGSBOXNAME).put(Constants.PRIMARYCOLORKEY, color.value);
  }

  ///Gets the primary color from Hive (use in the app process)
  Color getPrimaryColorHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    int colorValue = box.get(Constants.PRIMARYCOLORKEY, defaultValue: Constants.defaultPrimaryColor!.value);
    return Color(colorValue);
  }

  ///Sets the value of collapseAll to true if all entries should be collapsed
  ///and to false if all entries should be expanded
  void setCollapseAll(bool collapseAll) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.COLLAPSEALLKEY, collapseAll);
  }

  ///Gets the collapseAll value from Hive (use in the app process)
  bool getCollapseAllHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.COLLAPSEALLKEY, defaultValue: Constants.COLLAPSEALLDEFAULT);
  }

  ///Returns a stream that listens to changes of the collapseAll value
  Stream<BoxEvent> getCollapseAllWatcher() {
    return Hive.box(Constants.SETTINGSBOXNAME).watch(key: Constants.COLLAPSEALLKEY);
  }

  ///Sets the database version
  void setDatabaseVersion(int version) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(Constants.DATABASEVERSIONKEY, version);
  }

  ///Gets the database version from the preferences
  Future<int> getDatabaseVersionPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.DATABASEVERSIONKEY)) {
      return 1; //first version
    } else {
      return prefs.getInt(Constants.DATABASEVERSIONKEY)!;
    }
  }

  ///Sets the add wallet to favorites value
  void setAddWalletToFavorites(bool addToFav) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.ADDWALLETTOFAVORITESKEY, addToFav);
  }

  ///Gets the add wallet to favorites value from Hive
  bool getAddWalletToFavoritesHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.ADDWALLETTOFAVORITESKEY, defaultValue: Constants.ADDWALLETTOFAVORITESDEFAULT)!;
  }

  ///Sets the current app version
  void setCurrentAppVersion(String currentVersion) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.APPVERSIONKEY, currentVersion);
  }

  ///Gets the current app version from Hive
  String getCurrentAppVersionHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.APPVERSIONKEY, defaultValue: Constants.APPVERSIONDEFAULT);
  }

  ///Sets the selected time period of the wallet
  void setPortfolioSelectedPeriod(TimePeriod period) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.PORTFOLIOSELECTEDPERIODKEY, period.index);
  }

  ///Gets the selected time period of the portfolio from Hive
  TimePeriod getPortfolioSelectedPeriod() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    if (!box.containsKey(Constants.PORTFOLIOSELECTEDPERIODKEY)) {
      return TimePeriod.day;
    } else {
      return TimePeriod.values[box.get(Constants.PORTFOLIOSELECTEDPERIODKEY)];
    }
  }

  ///Sets if the coin price entered by the user should be used
  ///for coin value calculation in transaction list
  void setUseOwnCoinPrice(bool useOwnCoinPrice) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.USEOWNCOINPRICEKEY, useOwnCoinPrice);
  }

  ///gets if the coin price entered by the user should be used
  ///for coin value calculation in transaction list
  bool getUseOwnCoinPriceHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.USEOWNCOINPRICEKEY, defaultValue: Constants.USEOWNCOINPRICEDEFAULT);
  }

  ///Sets if the transaction value should be displayed in the portfolio currency
  void setDisplayInPortfolioCurrency(bool displayInPortfolioCurrency) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.DISPLAYINPORTFOLIOCURRENCYKEY, displayInPortfolioCurrency);
  }

  ///Gets if the transaction value should be displayed in the portfolio currency
  bool getDisplayInPortfolioCurrency() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.DISPLAYINPORTFOLIOCURRENCYKEY, defaultValue: Constants.DISPLAYINPORTFOLIOCURRENCYDEFAULT);
  }

  ///Sets the id of the default portfolio
  void setDefaultPortfolioId(int portfolioId) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.DEFAULTPORTFOLIOIDKEY, portfolioId);
  }

  ///Gets the id of the default portfolio or 0 if none exists
  int getDefaultPortfolioIdHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.DEFAULTPORTFOLIOIDKEY, defaultValue: Constants.NODEFAULTPORTFOLIOID);
  }

  ///Sets the start screen of the app
  void setStartScreen(StartScreen startScreen) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.STARTSCREENKEY, startScreen.index);
  }

  ///Gets the start screen of the app
  StartScreen getStartScreen() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return StartScreen.values[box.get(Constants.STARTSCREENKEY, defaultValue: Constants.DEFAULTSTARTSCREEN.index)];
  }

  ///Sets the thumbnailsLastUpdated value
  void setThumbnailsLastUpdated(DateTime thumbnailsLastUpdated) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.thumbnailsLastUpdatedKey, thumbnailsLastUpdated);
  }

  ///Gets the thumbnailsLastUpdated value
  DateTime? getThumbnailsLastUpdated() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.thumbnailsLastUpdatedKey);
  }

  ///Sets the date format locale
  void setDateFormatLocale(String locale) async {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.dateFormatLocaleKey, locale);
    box.put(Constants.dateFormatLocaleKey, locale);
  }

  ///Gets the date format locale from Hive (use in the app process). Use system locale if none is selected
  String getDateFormatLocaleHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    String defaultLocale = "en_US";
    if (!box.containsKey(Constants.dateFormatLocaleKey)) {
      defaultLocale = Platform.localeName;
    }
    return box.get(Constants.dateFormatLocaleKey, defaultValue: defaultLocale);
  }

  ///Gets the date format locale from preferences (use in background work). Use system locale if none is selected
  Future<String> getDateFormatLocalePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.dateFormatLocaleKey)) {
      return Platform.localeName;
    } else {
      return prefs.getString(Constants.dateFormatLocaleKey)!;
    }
  }

  ///Sets the number format locale
  void setNumberFormatLocale(String locale) async {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.numberFormatLocaleKey, locale);
    box.put(Constants.numberFormatLocaleKey, locale);
  }

  ///Gets the number format locale from preferences (use in background work). Use system locale if none is selected
  String getNumberFormatLocaleHive() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    String defaultLocale = "en_US";
    if (!box.containsKey(Constants.numberFormatLocaleKey)) {
      defaultLocale = Platform.localeName;
    }
    return box.get(Constants.numberFormatLocaleKey, defaultValue: defaultLocale);
  }

  ///Gets the number format locale from Hive (use in the app process). Use system locale if none is selected
  Future<String> getNumberFormatLocalePrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(Constants.numberFormatLocaleKey)) {
      return Platform.localeName;
    } else {
      return prefs.getString(Constants.numberFormatLocaleKey)!;
    }
  }

  ///Sets the private mode to enabled or disabled
  void setPrivateMode(bool enabled) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.privateModeKey, enabled);
  }

  ///Gets the status of the private mode
  bool getPrivateMode() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.privateModeKey, defaultValue: Constants.privateModeDefault);
  }

  ///Returns a stream that listens to changes of the privateMode value
  Stream<BoxEvent> getPrivateModeWatcher() {
    return Hive.box(Constants.SETTINGSBOXNAME).watch(key: Constants.privateModeKey);
  }

  ///Sets the password
  void setPassword(String password) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.passwordKey, password);
  }

  ///Gets the password
  String getPassword() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.passwordKey, defaultValue: Constants.passwordDefault);
  }

  ///Sets if biometric auth is allowed
  void setBiometricsAllowed(bool biometricsAllowed) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.biometricsAllowedKey, biometricsAllowed);
  }

  ///Gets if biometric auth is allowed
  bool getBiometricsAllowed() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.biometricsAllowedKey, defaultValue: Constants.biometricsAllowedDefault);
  }

  ///Sets if biometric hardware is available
  void setBiometricsAvailable(bool biometricsAvailable) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.biometricsAvailableKey, biometricsAvailable);
  }

  ///Gets if biometric hardware is available
  bool getBiometricsAvailable() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.biometricsAvailableKey, defaultValue: Constants.biometricsAvailableDefault);
  }

  ///Sets if the user is authenticated
  void setIsAuthenticated(bool isAuthenticated) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.isAuthenticatedKey, isAuthenticated);
  }

  ///Gets if the user is authenticated
  bool getIsAuthenticated() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.isAuthenticatedKey, defaultValue: Constants.isAuthenticatedDefault);
  }

  ///Returns a stream that listens to changes of the isAuthenticated value
  Stream<BoxEvent> getIsAuthenticatedWatcher() {
    return Hive.box(Constants.SETTINGSBOXNAME).watch(key: Constants.isAuthenticatedKey);
  }

  ///Sets if the user was authenticated when closing the app
  void setWasAuthenticated(bool wasAuthenticated) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.wasAuthenticatedKey, wasAuthenticated);
  }

  ///Gets if the user was authenticated when closing the app
  bool getWasAuthenticated() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.wasAuthenticatedKey, defaultValue: Constants.wasAuthenticatedDefault);
  }

  ///Sets if the password screen should be opened on app start
  void setOpenAuthOnAppStart(bool openOnStart) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.openAuthOnAppStartKey, openOnStart);
  }

  ///Gets if the password screen should be opened on app start
  bool getOpenAuthOnAppStart() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.openAuthOnAppStartKey, defaultValue: Constants.openAuthOnAppStartDefault);
  }

  ///Sets the time that the user should stay authenticated after closing the app
  void setStayAuthenticatedMinutes(int minutes) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.stayAuthenticatedMinutesKey, minutes);
  }

  ///Gets the time that the user should stay authenticated after closing the app
  int getStayAuthenticatedMinutes() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.stayAuthenticatedMinutesKey, defaultValue: Constants.stayAuthenticatedMinutesDefault);
  }
  ///Sets the time the app was closed
  void setAppClosedTime(DateTime time) {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.appClosedTimeKey, time);
  }

  ///Gets the time the app was closed
  DateTime getAppClosedTime() {
    var box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.appClosedTimeKey, defaultValue: DateTime.now());
  }

  ///Sets the path of the auto backup
  void setAutoBackupPath(String path) {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.autoBackupPathKey, path);
  }

  ///Gets the path of the auto backup
  String getAutoBackupPath() {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.autoBackupPathKey, defaultValue: Constants.autoBackupPathDefault);
  }

  ///Sets the date of the last auto backup
  void setAutoBackupDate(DateTime date) {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.autoBackupDateKey, date);
  }

  ///Gets the date of the last auto backup
  DateTime? getAutoBackupDate() {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.autoBackupDateKey);
  }

  ///Sets the name of the auto backup file
  void setAutoBackupFileName(String name) {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.autoBackupFileNameKey, name);
  }

  ///Gets the name of the auto backup file
  String getAutoBackupFileName() {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.autoBackupFileNameKey, defaultValue: Constants.BACKUPFILENAME);
  }

  ///Sets if the auto backup should overwrite the existing file
  void setAutoBackupOverwriteFile(bool overwrite) {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    box.put(Constants.autoBackupOverwriteFileKey, overwrite);
  }

  ///Gets if the auto backup should overwrite the existing file
  bool getAutoBackupOverwriteFile() {
    final box = Hive.box(Constants.SETTINGSBOXNAME);
    return box.get(Constants.autoBackupOverwriteFileKey, defaultValue: Constants.autoBackupOverwriteFileDefault);
  }
}