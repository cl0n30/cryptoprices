import 'dart:math';

import 'package:crypto_prices/constants.dart';
import 'package:crypto_prices/database/portfolio_dao.dart';
import 'package:crypto_prices/database/transaction_dao.dart';
import 'package:crypto_prices/models/favorite_coin.dart';
import 'package:crypto_prices/models/wallet.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../util/util.dart';
import 'favorites_dao.dart';
import 'settings_dao.dart';

///DAO of the WalletBox
class WalletDao {

  ///Returns one wallet by its id
  Wallet? getWallet(int id) {
    var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);
    return box.get(id);
  }

  ///Returns one wallet from a portfolio by its coinId
  //Wallet? getWalletByCoinId(String coinId, int portfolioId) {
  //  var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);
  //  for (int i = 0; i < box.length; i++) {
  //    if (box.getAt(i)!.coinId == coinId) {
  //      return box.getAt(i);
  //    }
  //  }
  //}

  ///Returns all wallets
  List<Wallet> getAllWallets() {
    var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);
    return box.values.toList();
  }

  ///Returns a ValueListenable that listens for changes to the WalletBox
  ValueListenable<Box<Wallet>> getAllWalletsWatcher() {
    return Hive.box<Wallet>(Constants.WALLETBOXNAME).listenable();
  }

  ///Returns multiple wallets with the given ids
  List<Wallet> getMultipleWallets(List<int> ids) {
    var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);
    List<Wallet> list = [];
    ids.forEach((id) {
      if (box.containsKey(id))
        list.add(box.get(id)!);
    });
    return list;
  }

  ///Inserts one wallet
  void insertWallet(Wallet wallet) {
    var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);

    if (wallet.id == 0) {
      wallet.id = Util.generateId();
    }
    box.put(wallet.id, wallet);

    PortfolioDao().getPortfolio(wallet.connectedPortfolioId)!.addWallet(wallet);

    if (SettingsDAO().getAddWalletToFavoritesHive()) {
      FavoritesDao favoritesDao = FavoritesDao();
      if (favoritesDao.getFavorite(wallet.coinId) == null)
        favoritesDao.insertFavorite(
          FavoriteCoin(
            wallet.coinId,
            name: wallet.coinName,
            symbol: wallet.coinSymbol,
            favoriteListPosition: favoritesDao.favoritesCount()
          )
        );
    }
  }

  ///Inserts multiple wallets
  void insertMultipleWallets(List<Wallet> wallets) {
    var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);
    final portfolioDao = PortfolioDao();

    wallets.forEach((wallet) {
      if (wallet.id == 0) {
        wallet.id = Util.generateId();
      }
      box.put(wallet.id, wallet);
      portfolioDao.getPortfolio(wallet.connectedPortfolioId)!.addWallet(wallet);
    });
  }

  ///Deletes one wallet and all corresponding transactions
  void deleteWallet(int id) {
    var box = Hive.box<Wallet>(Constants.WALLETBOXNAME);
    if (getWallet(id) == null)
      return;

    Wallet oldWallet = getWallet(id)!;
    TransactionDao().deleteMultipleTransactions(oldWallet.allTransactions.map((e) => e.id).toList());

    box.delete(id);
  }

  ///Deletes all wallets from a portfolio
  void deleteAllPortfolioWallets(int portfolioId) {
    final wallets = getAllWallets();
    final portfolioWallets = wallets.where((wallet) => wallet.connectedPortfolioId == portfolioId).toList();
    portfolioWallets.forEach((wallet) {
      deleteWallet(wallet.id);
    });
  }

  ///Sets if the currency for the wallets has been changed (in PortfolioWidget)
  void setCurrencyChanged(bool changed) {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    box.put(Constants.WALLETSCURRENCYCHANGEDKEY, changed);
  }

  ///Returns if the currency for the wallets has been changed (in PortfolioWidget)
  bool getCurrencyChanged() {
    var box = Hive.box(Constants.CURRENCYCHANGEDBOXNAME);
    return (box.get(Constants.WALLETSCURRENCYCHANGEDKEY)) == null ? true : box.get(Constants.WALLETSCURRENCYCHANGEDKEY);
  }
}